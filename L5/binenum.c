#include <stdio.h>
#include <stdlib.h>

int main(int argc,char** argv)
{
  int i,j;

  if(argc == 1)
    return 1;  

  for( i=0; i<atoi(argv[1]); i++){
      for(j=atoi(argv[1])/2; j>0; j >>= 1){
          printf("%d&",(i&j)/j);
      }
      for(j=0; j<13; j++)
          printf("X&");
      printf("X \\\\\n");
  }
  return 0;
}
