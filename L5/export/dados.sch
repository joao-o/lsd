VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3e"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL XLXN_1
        SIGNAL XLXN_2
        SIGNAL XLXN_3
        SIGNAL XLXN_4
        SIGNAL EX
        SIGNAL CLK_F
        SIGNAL Q(0)
        SIGNAL Q(1)
        SIGNAL Q(2)
        SIGNAL Q(3)
        SIGNAL Q(3:0)
        SIGNAL XLXN_27
        SIGNAL XLXN_49
        SIGNAL LD
        SIGNAL XLXN_52
        SIGNAL UP
        SIGNAL CE
        SIGNAL XLXN_18
        SIGNAL CT
        SIGNAL CLK_S
        PORT Output EX
        PORT Input CLK_F
        PORT Output Q(3:0)
        PORT Input LD
        PORT Input UP
        PORT Input CE
        PORT Input CT
        PORT Input CLK_S
        BEGIN BLOCKDEF cb4cled
            TIMESTAMP 2000 1 1 10 10 10
            RECTANGLE N 64 -704 320 -64 
            LINE N 0 -256 64 -256 
            LINE N 384 -192 320 -192 
            LINE N 384 -448 320 -448 
            LINE N 384 -512 320 -512 
            LINE N 384 -576 320 -576 
            LINE N 384 -640 320 -640 
            LINE N 0 -448 64 -448 
            LINE N 0 -32 64 -32 
            LINE N 0 -128 64 -128 
            LINE N 80 -128 64 -144 
            LINE N 64 -112 80 -128 
            LINE N 192 -32 64 -32 
            LINE N 192 -64 192 -32 
            LINE N 0 -512 64 -512 
            LINE N 0 -576 64 -576 
            LINE N 0 -640 64 -640 
            LINE N 0 -192 64 -192 
            LINE N 0 -320 64 -320 
            LINE N 384 -128 320 -128 
        END BLOCKDEF
        BEGIN BLOCKDEF and2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 144 -48 64 -48 
            LINE N 64 -144 144 -144 
            LINE N 64 -48 64 -144 
        END BLOCKDEF
        BEGIN BLOCKDEF pergunt2
            TIMESTAMP 2014 5 28 12 38 56
            RECTANGLE N 64 -192 320 0 
            LINE N 64 -160 0 -160 
            LINE N 64 -32 0 -32 
            LINE N 320 -96 384 -96 
        END BLOCKDEF
        BEGIN BLOCKDEF gnd
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -64 64 -96 
            LINE N 76 -48 52 -48 
            LINE N 68 -32 60 -32 
            LINE N 88 -64 40 -64 
            LINE N 64 -64 64 -80 
            LINE N 64 -128 64 -96 
        END BLOCKDEF
        BEGIN BLOCK XLXI_2 cb4cled
            PIN C CLK_F
            PIN CE XLXN_49
            PIN CLR XLXN_27
            PIN D0 XLXN_1
            PIN D1 XLXN_2
            PIN D2 XLXN_3
            PIN D3 XLXN_4
            PIN L LD
            PIN UP XLXN_27
            PIN CEO
            PIN Q0 Q(0)
            PIN Q1 Q(1)
            PIN Q2 Q(2)
            PIN Q3 Q(3)
            PIN TC EX
        END BLOCK
        BEGIN BLOCK XLXI_8 gnd
            PIN G XLXN_27
        END BLOCK
        BEGIN BLOCK XLXI_1 cb4cled
            PIN C CLK_F
            PIN CE CE
            PIN CLR XLXN_52
            PIN D0 XLXN_52
            PIN D1 XLXN_52
            PIN D2 XLXN_52
            PIN D3 XLXN_52
            PIN L XLXN_52
            PIN UP UP
            PIN CEO
            PIN Q0 XLXN_1
            PIN Q1 XLXN_2
            PIN Q2 XLXN_3
            PIN Q3 XLXN_4
            PIN TC
        END BLOCK
        BEGIN BLOCK XLXI_19 gnd
            PIN G XLXN_52
        END BLOCK
        BEGIN BLOCK XLXI_7 pergunt2
            PIN BUTTN CLK_S
            PIN CLK CLK_F
            PIN Px XLXN_18
        END BLOCK
        BEGIN BLOCK XLXI_3 and2
            PIN I0 CT
            PIN I1 XLXN_18
            PIN O XLXN_49
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 3520 2720
        BEGIN BRANCH XLXN_1
            WIRE 1520 880 1872 880
        END BRANCH
        BEGIN BRANCH XLXN_2
            WIRE 1520 944 1872 944
        END BRANCH
        BEGIN BRANCH XLXN_3
            WIRE 1520 1008 1872 1008
        END BRANCH
        BEGIN BRANCH XLXN_4
            WIRE 1520 1072 1872 1072
        END BRANCH
        BEGIN BRANCH EX
            WIRE 2256 1392 2352 1392
        END BRANCH
        BEGIN BRANCH CLK_F
            WIRE 1776 1392 1872 1392
        END BRANCH
        INSTANCE XLXI_2 1872 1520 R0
        IOMARKER 2352 1392 EX R0 28
        BEGIN BRANCH Q(0)
            WIRE 2256 880 2304 880
            WIRE 2304 880 2368 880
            BEGIN DISPLAY 2304 880 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 2256 944 2304 944
            WIRE 2304 944 2368 944
            BEGIN DISPLAY 2304 944 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 2256 1008 2304 1008
            WIRE 2304 1008 2368 1008
            BEGIN DISPLAY 2304 1008 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 2256 1072 2304 1072
            WIRE 2304 1072 2368 1072
            BEGIN DISPLAY 2304 1072 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3:0)
            WIRE 2464 880 2464 944
            WIRE 2464 944 2464 1008
            WIRE 2464 1008 2464 1072
            WIRE 2464 1072 2464 1104
            WIRE 2464 1104 2512 1104
        END BRANCH
        BUSTAP 2464 880 2368 880
        BUSTAP 2464 944 2368 944
        BUSTAP 2464 1008 2368 1008
        BUSTAP 2464 1072 2368 1072
        IOMARKER 1776 1392 CLK_F R180 28
        INSTANCE XLXI_8 1760 1648 R0
        BEGIN BRANCH XLXN_27
            WIRE 1824 1200 1872 1200
            WIRE 1824 1200 1824 1488
            WIRE 1824 1488 1872 1488
            WIRE 1824 1488 1824 1520
        END BRANCH
        BEGIN BRANCH XLXN_49
            WIRE 1488 624 1664 624
            WIRE 1664 624 1664 1328
            WIRE 1664 1328 1872 1328
        END BRANCH
        BEGIN BRANCH LD
            WIRE 1792 1264 1872 1264
        END BRANCH
        IOMARKER 1792 1264 LD R180 28
        IOMARKER 2512 1104 Q(3:0) R0 28
        BEGIN BRANCH CLK_F
            WIRE 1088 1392 1136 1392
        END BRANCH
        BEGIN BRANCH XLXN_52
            WIRE 1104 880 1136 880
            WIRE 1104 880 1104 944
            WIRE 1104 944 1136 944
            WIRE 1104 944 1104 1008
            WIRE 1104 1008 1136 1008
            WIRE 1104 1008 1104 1072
            WIRE 1104 1072 1136 1072
            WIRE 1104 1072 1104 1264
            WIRE 1104 1264 1104 1488
            WIRE 1104 1488 1136 1488
            WIRE 1104 1488 1104 1536
            WIRE 1104 1264 1136 1264
        END BRANCH
        BEGIN BRANCH UP
            WIRE 1056 1200 1136 1200
        END BRANCH
        BEGIN BRANCH CE
            WIRE 1056 1328 1136 1328
        END BRANCH
        INSTANCE XLXI_1 1136 1520 R0
        INSTANCE XLXI_19 1040 1664 R0
        IOMARKER 1088 1392 CLK_F R180 28
        IOMARKER 1056 1200 UP R180 28
        IOMARKER 1056 1328 CE R180 28
        BEGIN BRANCH XLXN_18
            WIRE 1120 592 1232 592
        END BRANCH
        BEGIN BRANCH CT
            WIRE 752 432 1184 432
            WIRE 1184 432 1184 656
            WIRE 1184 656 1232 656
        END BRANCH
        BEGIN BRANCH CLK_F
            WIRE 704 656 736 656
        END BRANCH
        BEGIN BRANCH CLK_S
            WIRE 704 528 736 528
        END BRANCH
        BEGIN INSTANCE XLXI_7 736 688 R0
        END INSTANCE
        INSTANCE XLXI_3 1232 720 R0
        IOMARKER 704 656 CLK_F R180 28
        IOMARKER 704 528 CLK_S R180 28
        IOMARKER 752 432 CT R180 28
    END SHEET
END SCHEMATIC
