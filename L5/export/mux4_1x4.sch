VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3e"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL O(0)
        SIGNAL D0(0)
        SIGNAL D1(0)
        SIGNAL D2(0)
        SIGNAL D3(0)
        SIGNAL S(0)
        SIGNAL S(1)
        SIGNAL XLXN_14
        SIGNAL O(1)
        SIGNAL D0(1)
        SIGNAL D1(1)
        SIGNAL D2(1)
        SIGNAL D3(1)
        SIGNAL XLXN_22
        SIGNAL O(3)
        SIGNAL D0(3)
        SIGNAL D1(3)
        SIGNAL D2(3)
        SIGNAL D3(3)
        SIGNAL XLXN_38
        SIGNAL XLXN_30
        SIGNAL O(2)
        SIGNAL D3(2)
        SIGNAL D2(2)
        SIGNAL D1(2)
        SIGNAL D0(2)
        SIGNAL O(3:0)
        SIGNAL D0(3:0)
        SIGNAL D1(3:0)
        SIGNAL D2(3:0)
        SIGNAL D3(3:0)
        SIGNAL S(1:0)
        PORT Output O(3:0)
        PORT Input D0(3:0)
        PORT Input D1(3:0)
        PORT Input D2(3:0)
        PORT Input D3(3:0)
        PORT Input S(1:0)
        BEGIN BLOCKDEF m4_1e
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -416 96 -416 
            LINE N 0 -352 96 -352 
            LINE N 0 -288 96 -288 
            LINE N 0 -224 96 -224 
            LINE N 0 -32 96 -32 
            LINE N 320 -320 256 -320 
            LINE N 0 -160 96 -160 
            LINE N 0 -96 96 -96 
            LINE N 176 -96 96 -96 
            LINE N 176 -208 176 -96 
            LINE N 224 -32 96 -32 
            LINE N 224 -216 224 -32 
            LINE N 256 -224 96 -192 
            LINE N 256 -416 256 -224 
            LINE N 96 -448 256 -416 
            LINE N 96 -192 96 -448 
            LINE N 128 -160 96 -160 
            LINE N 128 -200 128 -160 
        END BLOCKDEF
        BEGIN BLOCKDEF vcc
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -32 64 -64 
            LINE N 64 0 64 -32 
            LINE N 96 -64 32 -64 
        END BLOCKDEF
        BEGIN BLOCK XLXI_1 m4_1e
            PIN D0 D0(0)
            PIN D1 D1(0)
            PIN D2 D2(0)
            PIN D3 D3(0)
            PIN E XLXN_14
            PIN S0 S(0)
            PIN S1 S(1)
            PIN O O(0)
        END BLOCK
        BEGIN BLOCK XLXI_5 vcc
            PIN P XLXN_14
        END BLOCK
        BEGIN BLOCK XLXI_6 m4_1e
            PIN D0 D0(1)
            PIN D1 D1(1)
            PIN D2 D2(1)
            PIN D3 D3(1)
            PIN E XLXN_22
            PIN S0 S(0)
            PIN S1 S(1)
            PIN O O(1)
        END BLOCK
        BEGIN BLOCK XLXI_7 vcc
            PIN P XLXN_22
        END BLOCK
        BEGIN BLOCK XLXI_10 m4_1e
            PIN D0 D0(3)
            PIN D1 D1(3)
            PIN D2 D2(3)
            PIN D3 D3(3)
            PIN E XLXN_38
            PIN S0 S(0)
            PIN S1 S(1)
            PIN O O(3)
        END BLOCK
        BEGIN BLOCK XLXI_11 vcc
            PIN P XLXN_38
        END BLOCK
        BEGIN BLOCK XLXI_9 vcc
            PIN P XLXN_30
        END BLOCK
        BEGIN BLOCK XLXI_8 m4_1e
            PIN D0 D0(2)
            PIN D1 D1(2)
            PIN D2 D2(2)
            PIN D3 D3(2)
            PIN E XLXN_30
            PIN S0 S(0)
            PIN S1 S(1)
            PIN O O(2)
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 3520 2720
        INSTANCE XLXI_1 720 752 R0
        BEGIN BRANCH O(0)
            WIRE 1040 432 1072 432
            BEGIN DISPLAY 1072 432 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D0(0)
            WIRE 688 336 720 336
            BEGIN DISPLAY 688 336 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D1(0)
            WIRE 688 400 720 400
            BEGIN DISPLAY 688 400 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D2(0)
            WIRE 688 464 720 464
            BEGIN DISPLAY 688 464 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D3(0)
            WIRE 688 528 720 528
            BEGIN DISPLAY 688 528 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(0)
            WIRE 688 592 720 592
            BEGIN DISPLAY 688 592 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(1)
            WIRE 688 656 720 656
            BEGIN DISPLAY 688 656 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_14
            WIRE 688 720 720 720
        END BRANCH
        INSTANCE XLXI_5 752 720 R180
        INSTANCE XLXI_6 672 1440 R0
        BEGIN BRANCH O(1)
            WIRE 992 1120 1024 1120
            BEGIN DISPLAY 1024 1120 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D0(1)
            WIRE 640 1024 672 1024
            BEGIN DISPLAY 640 1024 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D1(1)
            WIRE 640 1088 672 1088
            BEGIN DISPLAY 640 1088 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D2(1)
            WIRE 640 1152 672 1152
            BEGIN DISPLAY 640 1152 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D3(1)
            WIRE 640 1216 672 1216
            BEGIN DISPLAY 640 1216 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(0)
            WIRE 640 1280 672 1280
            BEGIN DISPLAY 640 1280 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(1)
            WIRE 640 1344 672 1344
            BEGIN DISPLAY 640 1344 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_22
            WIRE 640 1408 672 1408
        END BRANCH
        INSTANCE XLXI_7 704 1408 R180
        INSTANCE XLXI_10 1232 1440 R0
        BEGIN BRANCH O(3)
            WIRE 1552 1120 1584 1120
            BEGIN DISPLAY 1584 1120 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D0(3)
            WIRE 1200 1024 1232 1024
            BEGIN DISPLAY 1200 1024 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D1(3)
            WIRE 1200 1088 1232 1088
            BEGIN DISPLAY 1200 1088 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D2(3)
            WIRE 1200 1152 1232 1152
            BEGIN DISPLAY 1200 1152 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D3(3)
            WIRE 1200 1216 1232 1216
            BEGIN DISPLAY 1200 1216 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(0)
            WIRE 1200 1280 1232 1280
            BEGIN DISPLAY 1200 1280 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(1)
            WIRE 1200 1344 1232 1344
            BEGIN DISPLAY 1200 1344 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_38
            WIRE 1200 1408 1232 1408
        END BRANCH
        INSTANCE XLXI_11 1264 1408 R180
        BEGIN BRANCH XLXN_30
            WIRE 1296 736 1312 736
            WIRE 1312 736 1328 736
        END BRANCH
        INSTANCE XLXI_9 1360 736 R180
        BEGIN BRANCH S(1)
            WIRE 1296 672 1312 672
            WIRE 1312 672 1328 672
            BEGIN DISPLAY 1296 672 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(0)
            WIRE 1296 608 1312 608
            WIRE 1312 608 1328 608
            BEGIN DISPLAY 1296 608 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_8 1328 768 R0
        BEGIN BRANCH O(2)
            WIRE 1648 448 1680 448
            BEGIN DISPLAY 1680 448 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D3(2)
            WIRE 1296 544 1328 544
            BEGIN DISPLAY 1296 544 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D2(2)
            WIRE 1296 480 1328 480
            BEGIN DISPLAY 1296 480 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D1(2)
            WIRE 1296 416 1328 416
            BEGIN DISPLAY 1296 416 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D0(2)
            WIRE 1296 352 1328 352
            BEGIN DISPLAY 1296 352 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH O(3:0)
            WIRE 1856 640 1968 640
            BEGIN DISPLAY 1856 640 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D0(3:0)
            WIRE 224 352 336 352
            BEGIN DISPLAY 336 352 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D1(3:0)
            WIRE 224 384 336 384
            BEGIN DISPLAY 336 384 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D2(3:0)
            WIRE 224 416 336 416
            BEGIN DISPLAY 336 416 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D3(3:0)
            WIRE 224 448 336 448
            BEGIN DISPLAY 336 448 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(1:0)
            WIRE 224 560 336 560
            BEGIN DISPLAY 336 560 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        IOMARKER 224 352 D0(3:0) R180 28
        IOMARKER 224 384 D1(3:0) R180 28
        IOMARKER 224 416 D2(3:0) R180 28
        IOMARKER 224 448 D3(3:0) R180 28
        IOMARKER 224 560 S(1:0) R180 28
        IOMARKER 1968 640 O(3:0) R0 28
    END SHEET
END SCHEMATIC
