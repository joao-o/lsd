VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3e"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL BTN
        SIGNAL XLXN_3
        SIGNAL XLXN_8
        SIGNAL CLK_F
        SIGNAL XLXN_10
        SIGNAL XLXN_12
        SIGNAL XLXN_16
        SIGNAL XLXN_17
        SIGNAL PRESS
        SIGNAL XLXN_19
        PORT Input BTN
        PORT Input CLK_F
        PORT Output PRESS
        BEGIN BLOCKDEF cb16ce
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 384 -192 320 -192 
            RECTANGLE N 320 -268 384 -244 
            LINE N 384 -256 320 -256 
            LINE N 0 -192 64 -192 
            LINE N 192 -32 64 -32 
            LINE N 192 -64 192 -32 
            LINE N 80 -128 64 -144 
            LINE N 64 -112 80 -128 
            LINE N 0 -128 64 -128 
            LINE N 0 -32 64 -32 
            LINE N 384 -128 320 -128 
            RECTANGLE N 64 -320 320 -64 
        END BLOCKDEF
        BEGIN BLOCKDEF fd
            TIMESTAMP 2000 1 1 10 10 10
            RECTANGLE N 64 -320 320 -64 
            LINE N 0 -128 64 -128 
            LINE N 0 -256 64 -256 
            LINE N 384 -256 320 -256 
            LINE N 80 -128 64 -144 
            LINE N 64 -112 80 -128 
        END BLOCKDEF
        BEGIN BLOCKDEF inv
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -32 64 -32 
            LINE N 224 -32 160 -32 
            LINE N 64 -64 128 -32 
            LINE N 128 -32 64 0 
            LINE N 64 0 64 -64 
            CIRCLE N 128 -48 160 -16 
        END BLOCKDEF
        BEGIN BLOCKDEF and2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 144 -48 64 -48 
            LINE N 64 -144 144 -144 
            LINE N 64 -48 64 -144 
        END BLOCKDEF
        BEGIN BLOCKDEF or2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 28 -224 204 -48 112 -48 192 -96 
            ARC N -40 -152 72 -40 48 -48 48 -144 
            LINE N 112 -144 48 -144 
            ARC N 28 -144 204 32 192 -96 112 -144 
            LINE N 112 -48 48 -48 
        END BLOCKDEF
        BEGIN BLOCK XLXI_1 cb16ce
            PIN C CLK_F
            PIN CE XLXN_19
            PIN CLR XLXN_3
            PIN CEO
            PIN Q(15:0)
            PIN TC XLXN_17
        END BLOCK
        BEGIN BLOCK XLXI_4 fd
            PIN C CLK_F
            PIN D XLXN_10
            PIN Q XLXN_8
        END BLOCK
        BEGIN BLOCK XLXI_5 fd
            PIN C CLK_F
            PIN D XLXN_8
            PIN Q XLXN_12
        END BLOCK
        BEGIN BLOCK XLXI_6 inv
            PIN I XLXN_17
            PIN O XLXN_10
        END BLOCK
        BEGIN BLOCK XLXI_8 or2
            PIN I0 XLXN_8
            PIN I1 XLXN_12
            PIN O XLXN_16
        END BLOCK
        BEGIN BLOCK XLXI_9 and2
            PIN I0 XLXN_17
            PIN I1 XLXN_16
            PIN O PRESS
        END BLOCK
        BEGIN BLOCK XLXI_10 inv
            PIN I BTN
            PIN O XLXN_3
        END BLOCK
        BEGIN BLOCK XLXI_11 and2
            PIN I0 BTN
            PIN I1 XLXN_10
            PIN O XLXN_19
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 3520 2720
        INSTANCE XLXI_1 624 544 R0
        BEGIN BRANCH XLXN_3
            WIRE 608 512 624 512
        END BRANCH
        BEGIN BRANCH CLK_F
            WIRE 1232 544 1248 544
            WIRE 1248 544 1248 672
            WIRE 1248 672 1696 672
            WIRE 1248 544 1280 544
            WIRE 1696 544 1696 672
            WIRE 1696 544 1712 544
        END BRANCH
        BEGIN BRANCH CLK_F
            WIRE 592 416 624 416
        END BRANCH
        INSTANCE XLXI_4 1280 672 R0
        INSTANCE XLXI_5 1712 672 R0
        IOMARKER 1232 544 CLK_F R180 28
        INSTANCE XLXI_8 2128 720 R0
        BEGIN BRANCH XLXN_12
            WIRE 2096 416 2112 416
            WIRE 2112 416 2112 592
            WIRE 2112 592 2128 592
        END BRANCH
        BEGIN BRANCH XLXN_8
            WIRE 1664 416 1680 416
            WIRE 1680 416 1712 416
            WIRE 1680 416 1680 656
            WIRE 1680 656 2128 656
        END BRANCH
        INSTANCE XLXI_9 2384 896 R0
        BEGIN BRANCH XLXN_16
            WIRE 2384 624 2384 768
        END BRANCH
        BEGIN BRANCH XLXN_17
            WIRE 1008 416 1024 416
            WIRE 1024 416 1024 832
            WIRE 1024 832 2384 832
        END BRANCH
        BEGIN BRANCH PRESS
            WIRE 2640 800 2656 800
        END BRANCH
        INSTANCE XLXI_10 384 544 R0
        BEGIN BRANCH BTN
            WIRE 144 384 208 384
            WIRE 208 384 208 512
            WIRE 208 512 384 512
            WIRE 208 384 256 384
        END BRANCH
        BEGIN BRANCH XLXN_19
            WIRE 512 352 624 352
        END BRANCH
        INSTANCE XLXI_6 1024 448 R0
        IOMARKER 2656 800 PRESS R0 28
        IOMARKER 144 384 BTN R180 28
        INSTANCE XLXI_11 256 448 R0
        BEGIN BRANCH XLXN_10
            WIRE 208 160 208 320
            WIRE 208 320 256 320
            WIRE 208 160 1264 160
            WIRE 1264 160 1264 416
            WIRE 1264 416 1280 416
            WIRE 1248 416 1264 416
        END BRANCH
        IOMARKER 592 416 CLK_F R180 28
    END SHEET
END SCHEMATIC
