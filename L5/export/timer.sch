VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3e"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL CT
        SIGNAL XLXN_2
        SIGNAL XLXN_3
        SIGNAL LOAD_TIME
        SIGNAL CLK_F
        SIGNAL MODE
        SIGNAL EXP
        SIGNAL Q(3:0)
        SIGNAL CLK_S
        SIGNAL M_S
        SIGNAL P
        SIGNAL INC
        SIGNAL DEC
        SIGNAL RST
        SIGNAL DISP(15:0)
        PORT Output CT
        PORT Output LOAD_TIME
        PORT Input CLK_F
        PORT Output MODE
        PORT Output EXP
        PORT Input CLK_S
        PORT Input M_S
        PORT Input P
        PORT Input INC
        PORT Input DEC
        PORT Input RST
        PORT Output DISP(15:0)
        BEGIN BLOCKDEF dados
            TIMESTAMP 2014 5 28 17 45 41
            LINE N 320 -352 384 -352 
            LINE N 64 -352 0 -352 
            LINE N 64 -304 0 -304 
            LINE N 64 -256 0 -256 
            LINE N 64 -208 0 -208 
            RECTANGLE N 320 -316 384 -292 
            LINE N 320 -304 384 -304 
            LINE N 64 -144 0 -144 
            LINE N 64 -96 0 -96 
            RECTANGLE N 64 -384 320 -64 
        END BLOCKDEF
        BEGIN BLOCKDEF control
            TIMESTAMP 2014 5 28 17 43 19
            RECTANGLE N 64 -448 320 -96 
            LINE N 64 -416 0 -416 
            LINE N 64 -368 0 -368 
            LINE N 64 -320 0 -320 
            LINE N 64 -272 0 -272 
            LINE N 64 -224 0 -224 
            LINE N 64 -176 0 -176 
            LINE N 64 -128 0 -128 
            LINE N 320 -368 384 -368 
            LINE N 320 -416 384 -416 
            LINE N 320 -320 384 -320 
            LINE N 320 -272 384 -272 
            LINE N 320 -224 384 -224 
        END BLOCKDEF
        BEGIN BLOCKDEF disp_rom
            TIMESTAMP 2014 5 28 18 35 34
            RECTANGLE N 64 -64 400 0 
            RECTANGLE N 0 -44 64 -20 
            LINE N 64 -32 0 -32 
            RECTANGLE N 400 -44 464 -20 
            LINE N 400 -32 464 -32 
        END BLOCKDEF
        BEGIN BLOCK XLXI_1 dados
            PIN EX EXP
            PIN CT CT
            PIN UP XLXN_2
            PIN CE XLXN_3
            PIN LD LOAD_TIME
            PIN Q(3:0) Q(3:0)
            PIN CLK_F CLK_F
            PIN CLK_S CLK_S
        END BLOCK
        BEGIN BLOCK XLXI_2 control
            PIN M_S M_S
            PIN P P
            PIN EX EXP
            PIN INC INC
            PIN DEC DEC
            PIN RST RST
            PIN CLK_F CLK_F
            PIN UP XLXN_2
            PIN CT CT
            PIN CE XLXN_3
            PIN LD LOAD_TIME
            PIN MODE MODE
        END BLOCK
        BEGIN BLOCK XLXI_3 disp_rom
            PIN address(3:0) Q(3:0)
            PIN data_out(15:0) DISP(15:0)
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 3520 2720
        BEGIN INSTANCE XLXI_2 960 1184 R0
        END INSTANCE
        BEGIN INSTANCE XLXI_1 1616 1120 R0
        END INSTANCE
        BEGIN BRANCH CT
            WIRE 1344 768 1456 768
            WIRE 1456 768 1616 768
            WIRE 1456 608 1456 768
            WIRE 1456 608 2144 608
        END BRANCH
        BEGIN BRANCH LOAD_TIME
            WIRE 1344 912 1520 912
            WIRE 1520 912 1616 912
            WIRE 1520 576 1520 912
            WIRE 1520 576 2144 576
        END BRANCH
        BEGIN BRANCH CLK_F
            WIRE 816 1056 944 1056
            WIRE 944 1056 960 1056
            WIRE 944 1056 944 1152
            WIRE 944 1152 1408 1152
            WIRE 1408 976 1408 1152
            WIRE 1408 976 1616 976
        END BRANCH
        BEGIN BRANCH MODE
            WIRE 1344 960 1440 960
            WIRE 1440 656 1440 960
            WIRE 1440 656 2144 656
        END BRANCH
        BEGIN BRANCH EXP
            WIRE 896 672 896 864
            WIRE 896 864 960 864
            WIRE 896 672 2064 672
            WIRE 2064 672 2064 704
            WIRE 2064 704 2064 768
            WIRE 2064 704 2144 704
            WIRE 2000 768 2064 768
        END BRANCH
        BEGIN BRANCH Q(3:0)
            WIRE 2000 816 2144 816
        END BRANCH
        BEGIN BRANCH CLK_S
            WIRE 816 1104 1600 1104
            WIRE 1600 1024 1616 1024
            WIRE 1600 1024 1600 1104
        END BRANCH
        BEGIN BRANCH M_S
            WIRE 816 768 944 768
            WIRE 944 768 960 768
        END BRANCH
        BEGIN BRANCH P
            WIRE 816 816 944 816
            WIRE 944 816 960 816
        END BRANCH
        BEGIN BRANCH INC
            WIRE 816 912 944 912
            WIRE 944 912 960 912
        END BRANCH
        BEGIN BRANCH DEC
            WIRE 816 960 944 960
            WIRE 944 960 960 960
        END BRANCH
        BEGIN BRANCH RST
            WIRE 816 1008 944 1008
            WIRE 944 1008 960 1008
        END BRANCH
        IOMARKER 2144 608 CT R0 28
        IOMARKER 2144 656 MODE R0 28
        BEGIN BRANCH XLXN_3
            WIRE 1344 864 1616 864
        END BRANCH
        BEGIN BRANCH XLXN_2
            WIRE 1344 816 1488 816
            WIRE 1488 816 1616 816
        END BRANCH
        IOMARKER 816 912 INC R180 28
        IOMARKER 816 768 M_S R180 28
        IOMARKER 816 816 P R180 28
        IOMARKER 816 960 DEC R180 28
        IOMARKER 816 1008 RST R180 28
        IOMARKER 816 1056 CLK_F R180 28
        IOMARKER 816 1104 CLK_S R180 28
        BEGIN INSTANCE XLXI_3 2144 848 R0
        END INSTANCE
        BEGIN BRANCH DISP(15:0)
            WIRE 2608 816 2656 816
        END BRANCH
        IOMARKER 2656 816 DISP(15:0) R0 28
        IOMARKER 2144 704 EXP R0 28
        IOMARKER 2144 576 LOAD_TIME R0 28
    END SHEET
END SCHEMATIC
