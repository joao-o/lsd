------------------------------------------------------------------------------------
-- Instruções para uso desta memória, caso opte por realizar a lógica combinatória
-- da máquina de estados por memória:
--   * Linhas 34-35: alterar a dimensão da memória correspondente a número de bits de 
--                   endereço, e dimensão de cada palavra de dados. Por exemplo,
--                   se a memória tiver 512 posições (9 bits de endereço), cada posição
--                   com uma palavra de 13 bits, deverá colocar na linha 34 e 35:
--                      address  : in  std_logic_vector(8 downto 0)
--                      data_out : out std_logic_vector(12 downto 0)
--
--   * Linha 39: indicar o número de posições na memória; para o exemplo anterior
--               (9 bits de endereço, correspondentes a 2^9=512 posições), esta linha seria:
--                    type ram_type is array (0 to 511) of 
--
--   * Linha 39: alterar a dimensão da palavra da memória; no caso do exemplo fornecido,
--               as palavras têm 13 bits. Assim esta linha é dada por
--                    std_logic_vector(12 downto 0);
--
--   * Linhas 41-46: preencher cada uma das posições de memória; para cada linha indicar
--                   <posição> => <dados>,
--                   sendo que <posicao> e um número décimal e <dados> é um numero
--                   em binário. No exemplo dado, o número 27 seria dado por (13 bits)
--                   "0000000011011". O bit mais significativo é o da esquerda, o menos
--                   significativo o da direita.
------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- EDITAR A DIMENSÃO DOS DADOS data_out
entity my_mem is
port (
		address	: in  std_logic_vector(3 downto 0);
		data_out : out std_logic_vector(14 downto 0)
		);
end my_mem;

architecture Behavioral of my_mem is

  type ram_type is array (0 to 15) of 
        		std_logic_vector(14 downto 0);
     constant InitValue: ram_type := (
		1 =>   X"878" & "010",
		2 =>   X"518" & "001",
		3 =>   X"441" & "110",
		4 =>   X"310" & "101",
		5 =>   X"480" & "100",
		7 =>   X"370" & "000",
		8 =>   X"BA1" & "011",
		9 =>   X"841" & "100",
		10 =>  X"883" & "100",
		11 =>  X"887" & "100",
	others => X"880" & "100"
	);       

signal Content_d_mem: ram_type:= InitValue;
 
begin

    read: process(address)
	begin
			data_out <= Content_d_mem(CONV_INTEGER(address));
	end process;

end Behavioral;

