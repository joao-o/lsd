VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3e"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL BUTTN
        SIGNAL CLK
        SIGNAL XLXN_28
        SIGNAL XLXN_29
        SIGNAL XLXN_17
        SIGNAL Px
        SIGNAL XLXN_30
        SIGNAL XLXN_31
        PORT Input BUTTN
        PORT Input CLK
        PORT Output Px
        BEGIN BLOCKDEF fdp
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -112 80 -128 
            LINE N 80 -128 64 -144 
            RECTANGLE N 64 -320 320 -64 
            LINE N 384 -256 320 -256 
            LINE N 192 -320 192 -352 
            LINE N 192 -352 64 -352 
            LINE N 0 -256 64 -256 
            LINE N 0 -352 64 -352 
            LINE N 0 -128 64 -128 
        END BLOCKDEF
        BEGIN BLOCKDEF inv
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -32 64 -32 
            LINE N 224 -32 160 -32 
            LINE N 64 -64 128 -32 
            LINE N 128 -32 64 0 
            LINE N 64 0 64 -64 
            CIRCLE N 128 -48 160 -16 
        END BLOCKDEF
        BEGIN BLOCKDEF and2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 144 -48 64 -48 
            LINE N 64 -144 144 -144 
            LINE N 64 -48 64 -144 
        END BLOCKDEF
        BEGIN BLOCKDEF gnd
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -64 64 -96 
            LINE N 76 -48 52 -48 
            LINE N 68 -32 60 -32 
            LINE N 88 -64 40 -64 
            LINE N 64 -64 64 -80 
            LINE N 64 -128 64 -96 
        END BLOCKDEF
        BEGIN BLOCK XLXI_1 fdp
            PIN C CLK
            PIN D BUTTN
            PIN PRE XLXN_28
            PIN Q XLXN_31
        END BLOCK
        BEGIN BLOCK XLXI_3 inv
            PIN I XLXN_31
            PIN O XLXN_17
        END BLOCK
        BEGIN BLOCK XLXI_5 gnd
            PIN G XLXN_28
        END BLOCK
        BEGIN BLOCK XLXI_6 gnd
            PIN G XLXN_29
        END BLOCK
        BEGIN BLOCK XLXI_2 fdp
            PIN C CLK
            PIN D XLXN_17
            PIN PRE XLXN_29
            PIN Q XLXN_30
        END BLOCK
        BEGIN BLOCK XLXI_4 and2
            PIN I0 XLXN_30
            PIN I1 XLXN_31
            PIN O Px
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 3520 2720
        INSTANCE XLXI_1 656 1040 R0
        BEGIN BRANCH BUTTN
            WIRE 496 784 656 784
        END BRANCH
        IOMARKER 464 912 CLK R180 28
        BEGIN BRANCH CLK
            WIRE 464 912 576 912
            WIRE 576 912 656 912
            WIRE 576 912 576 1232
            WIRE 576 1232 1376 1232
        END BRANCH
        BEGIN BRANCH XLXN_28
            WIRE 656 688 656 1056
        END BRANCH
        INSTANCE XLXI_5 592 1184 R0
        INSTANCE XLXI_3 1072 1136 R0
        INSTANCE XLXI_2 1376 1360 R0
        BEGIN BRANCH XLXN_17
            WIRE 1296 1104 1376 1104
        END BRANCH
        BEGIN BRANCH Px
            WIRE 2064 816 2080 816
        END BRANCH
        IOMARKER 2080 816 Px R0 28
        IOMARKER 496 784 BUTTN R180 28
        BEGIN BRANCH XLXN_29
            WIRE 1360 1008 1376 1008
            WIRE 1360 1008 1360 1312
        END BRANCH
        BEGIN BRANCH XLXN_31
            WIRE 1040 784 1072 784
            WIRE 1072 784 1072 1104
            WIRE 1072 784 1808 784
        END BRANCH
        INSTANCE XLXI_6 1296 1440 R0
        INSTANCE XLXI_4 1808 912 R0
        BEGIN BRANCH XLXN_30
            WIRE 1760 1104 1776 1104
            WIRE 1776 848 1808 848
            WIRE 1776 848 1776 1104
        END BRANCH
    END SHEET
END SCHEMATIC
