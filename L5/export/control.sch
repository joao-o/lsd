VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3e"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL O(1)
        SIGNAL O(0)
        SIGNAL O(1:0)
        SIGNAL CNT(3)
        SIGNAL CNT(2)
        SIGNAL CNT(1)
        SIGNAL CNT(0)
        SIGNAL CNT(3:0)
        SIGNAL CLK_F
        SIGNAL A(3:0)
        SIGNAL A(0)
        SIGNAL A(1)
        SIGNAL A(2)
        SIGNAL A(3)
        SIGNAL XLXN_56
        SIGNAL XLXN_97
        SIGNAL D(2:0)
        SIGNAL XLXN_100
        SIGNAL D(14:11)
        SIGNAL D(10:7)
        SIGNAL D(6)
        SIGNAL D(5)
        SIGNAL D(4)
        SIGNAL D(3)
        SIGNAL CT
        SIGNAL UP
        SIGNAL CE
        SIGNAL LD
        SIGNAL MODE
        SIGNAL P
        SIGNAL EX
        SIGNAL INC
        SIGNAL DEC
        SIGNAL M_S
        SIGNAL RST
        SIGNAL XLXN_185
        SIGNAL XLXN_187
        SIGNAL XLXN_189
        SIGNAL XLXN_190
        SIGNAL XLXN_191
        SIGNAL XLXN_192
        SIGNAL G
        SIGNAL XLXN_198
        SIGNAL GN(3:0)
        SIGNAL XLXN_200(3:0)
        SIGNAL GN(1)
        SIGNAL GN(0)
        SIGNAL GN(3)
        SIGNAL GN(2)
        BEGIN SIGNAL D(14:0)
        END SIGNAL
        PORT Input CLK_F
        PORT Output CT
        PORT Output UP
        PORT Output CE
        PORT Output LD
        PORT Output MODE
        PORT Input P
        PORT Input EX
        PORT Input INC
        PORT Input DEC
        PORT Input M_S
        PORT Input RST
        BEGIN BLOCKDEF my_mem
            TIMESTAMP 2014 5 28 0 40 52
            RECTANGLE N 64 -64 400 0 
            RECTANGLE N 0 -44 64 -20 
            LINE N 64 -32 0 -32 
            RECTANGLE N 400 -44 464 -20 
            LINE N 400 -32 464 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF cb4cle
            TIMESTAMP 2000 1 1 10 10 10
            RECTANGLE N 64 -640 320 -64 
            LINE N 0 -256 64 -256 
            LINE N 0 -192 64 -192 
            LINE N 0 -576 64 -576 
            LINE N 0 -512 64 -512 
            LINE N 0 -448 64 -448 
            LINE N 192 -32 64 -32 
            LINE N 192 -64 192 -32 
            LINE N 80 -128 64 -144 
            LINE N 64 -112 80 -128 
            LINE N 0 -128 64 -128 
            LINE N 0 -32 64 -32 
            LINE N 0 -384 64 -384 
            LINE N 384 -576 320 -576 
            LINE N 384 -512 320 -512 
            LINE N 384 -448 320 -448 
            LINE N 384 -384 320 -384 
            LINE N 384 -192 320 -192 
            LINE N 384 -128 320 -128 
        END BLOCKDEF
        BEGIN BLOCKDEF mux4_1x4
            TIMESTAMP 2014 5 28 1 27 2
            RECTANGLE N 64 -320 320 0 
            RECTANGLE N 0 -300 64 -276 
            LINE N 64 -288 0 -288 
            RECTANGLE N 0 -236 64 -212 
            LINE N 64 -224 0 -224 
            RECTANGLE N 0 -172 64 -148 
            LINE N 64 -160 0 -160 
            RECTANGLE N 0 -108 64 -84 
            LINE N 64 -96 0 -96 
            RECTANGLE N 0 -44 64 -20 
            LINE N 64 -32 0 -32 
            RECTANGLE N 320 -300 384 -276 
            LINE N 320 -288 384 -288 
        END BLOCKDEF
        BEGIN BLOCKDEF and2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 144 -48 64 -48 
            LINE N 64 -144 144 -144 
            LINE N 64 -48 64 -144 
        END BLOCKDEF
        BEGIN BLOCKDEF inv
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -32 64 -32 
            LINE N 224 -32 160 -32 
            LINE N 64 -64 128 -32 
            LINE N 128 -32 64 0 
            LINE N 64 0 64 -64 
            CIRCLE N 128 -48 160 -16 
        END BLOCKDEF
        BEGIN BLOCKDEF gnd
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -64 64 -96 
            LINE N 76 -48 52 -48 
            LINE N 68 -32 60 -32 
            LINE N 88 -64 40 -64 
            LINE N 64 -64 64 -80 
            LINE N 64 -128 64 -96 
        END BLOCKDEF
        BEGIN BLOCKDEF mux8_1x2
            TIMESTAMP 2014 5 28 12 4 23
            RECTANGLE N 320 -556 384 -532 
            LINE N 320 -544 384 -544 
            RECTANGLE N 64 -576 320 0 
            LINE N 64 -544 0 -544 
            LINE N 64 -512 0 -512 
            LINE N 64 -480 0 -480 
            LINE N 64 -416 0 -416 
            LINE N 64 -352 0 -352 
            LINE N 64 -288 0 -288 
            LINE N 64 -224 0 -224 
            LINE N 64 -160 0 -160 
            LINE N 64 -448 0 -448 
            LINE N 64 -384 0 -384 
            LINE N 64 -320 0 -320 
            LINE N 64 -256 0 -256 
            LINE N 64 -192 0 -192 
            LINE N 64 -128 0 -128 
            LINE N 64 -96 0 -96 
            RECTANGLE N 0 -44 64 -20 
            LINE N 64 -32 0 -32 
            LINE N 64 -64 0 -64 
        END BLOCKDEF
        BEGIN BLOCKDEF buf
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -32 64 -32 
            LINE N 224 -32 128 -32 
            LINE N 64 0 128 -32 
            LINE N 128 -32 64 -64 
            LINE N 64 -64 64 0 
        END BLOCKDEF
        BEGIN BLOCKDEF vcc
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -32 64 -64 
            LINE N 64 0 64 -32 
            LINE N 96 -64 32 -64 
        END BLOCKDEF
        BEGIN BLOCK XLXI_24 and2
            PIN I0 O(1)
            PIN I1 O(0)
            PIN O XLXN_97
        END BLOCK
        BEGIN BLOCK XLXI_23 inv
            PIN I XLXN_97
            PIN O XLXN_56
        END BLOCK
        BEGIN BLOCK XLXI_6 mux4_1x4
            PIN D0(3:0) D(14:11)
            PIN D1(3:0) D(10:7)
            PIN D2(3:0) D(14:11)
            PIN D3(3:0) GN(3:0)
            PIN S(1:0) O(1:0)
            PIN O(3:0) CNT(3:0)
        END BLOCK
        BEGIN BLOCK XLXI_3 cb4cle
            PIN C CLK_F
            PIN CE XLXN_97
            PIN CLR XLXN_100
            PIN D0 CNT(0)
            PIN D1 CNT(1)
            PIN D2 CNT(2)
            PIN D3 CNT(3)
            PIN L XLXN_56
            PIN CEO
            PIN Q0 A(0)
            PIN Q1 A(1)
            PIN Q2 A(2)
            PIN Q3 A(3)
            PIN TC
        END BLOCK
        BEGIN BLOCK XLXI_28 gnd
            PIN G XLXN_100
        END BLOCK
        BEGIN BLOCK XLXI_30 mux8_1x2
            PIN D0_0 XLXN_185
            PIN D1_0 XLXN_187
            PIN D2_0 XLXN_189
            PIN D3_0 XLXN_191
            PIN D4_0 M_S
            PIN D5_0 XLXN_185
            PIN D6_0 M_S
            PIN D0_1 M_S
            PIN D1_1 RST
            PIN D2_1 XLXN_190
            PIN D3_1 XLXN_192
            PIN D4_1 G
            PIN D5_1 P
            PIN D6_1 XLXN_198
            PIN D7_1 G
            PIN D7_0 G
            PIN S(2:0) D(2:0)
            PIN O(1:0) O(1:0)
        END BLOCK
        BEGIN BLOCK XLXI_32 buf
            PIN I D(6)
            PIN O CT
        END BLOCK
        BEGIN BLOCK XLXI_33 buf
            PIN I D(5)
            PIN O UP
        END BLOCK
        BEGIN BLOCK XLXI_36 buf
            PIN I D(4)
            PIN O CE
        END BLOCK
        BEGIN BLOCK XLXI_37 buf
            PIN I D(3)
            PIN O LD
        END BLOCK
        BEGIN BLOCK XLXI_39 inv
            PIN I A(3)
            PIN O MODE
        END BLOCK
        BEGIN BLOCK XLXI_55 gnd
            PIN G G
        END BLOCK
        BEGIN BLOCK XLXI_56 vcc
            PIN P XLXN_198
        END BLOCK
        BEGIN BLOCK XLXI_40 inv
            PIN I M_S
            PIN O XLXN_189
        END BLOCK
        BEGIN BLOCK XLXI_42 inv
            PIN I RST
            PIN O XLXN_185
        END BLOCK
        BEGIN BLOCK XLXI_43 inv
            PIN I P
            PIN O XLXN_187
        END BLOCK
        BEGIN BLOCK XLXI_44 inv
            PIN I EX
            PIN O XLXN_190
        END BLOCK
        BEGIN BLOCK XLXI_45 inv
            PIN I INC
            PIN O XLXN_191
        END BLOCK
        BEGIN BLOCK XLXI_46 inv
            PIN I DEC
            PIN O XLXN_192
        END BLOCK
        BEGIN BLOCK XLXI_66 gnd
            PIN G GN(2)
        END BLOCK
        BEGIN BLOCK XLXI_67 gnd
            PIN G GN(3)
        END BLOCK
        BEGIN BLOCK XLXI_63 gnd
            PIN G GN(0)
        END BLOCK
        BEGIN BLOCK XLXI_65 gnd
            PIN G GN(1)
        END BLOCK
        BEGIN BLOCK XLXI_1 my_mem
            PIN address(3:0) A(3:0)
            PIN data_out(14:0) D(14:0)
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 3520 2720
        BEGIN BRANCH O(1)
            WIRE 1440 848 1472 848
            WIRE 1472 848 1520 848
            BEGIN DISPLAY 1472 848 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH O(0)
            WIRE 1440 784 1472 784
            WIRE 1472 784 1520 784
            BEGIN DISPLAY 1472 784 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        IOMARKER 1952 784 CLK_F R180 28
        BEGIN BRANCH CNT(3)
            WIRE 1952 528 2000 528
            WIRE 2000 528 2096 528
            BEGIN DISPLAY 2000 528 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CNT(2)
            WIRE 1952 464 2000 464
            WIRE 2000 464 2096 464
            BEGIN DISPLAY 2000 464 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CNT(1)
            WIRE 1952 400 2000 400
            WIRE 2000 400 2096 400
            BEGIN DISPLAY 2000 400 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CNT(0)
            WIRE 1952 336 2000 336
            WIRE 2000 336 2096 336
            BEGIN DISPLAY 2000 336 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BUSTAP 2656 528 2560 528
        BUSTAP 2656 464 2560 464
        BUSTAP 2656 400 2560 400
        BUSTAP 2656 336 2560 336
        BEGIN BRANCH CLK_F
            WIRE 1952 784 2096 784
        END BRANCH
        BEGIN BRANCH A(0)
            WIRE 2480 336 2512 336
            WIRE 2512 336 2560 336
            BEGIN DISPLAY 2512 336 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH A(1)
            WIRE 2480 400 2512 400
            WIRE 2512 400 2560 400
            BEGIN DISPLAY 2512 400 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH A(2)
            WIRE 2480 464 2512 464
            WIRE 2512 464 2560 464
            BEGIN DISPLAY 2512 464 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH A(3)
            WIRE 2480 528 2512 528
            WIRE 2512 528 2560 528
            BEGIN DISPLAY 2512 528 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_56
            WIRE 2032 656 2096 656
        END BRANCH
        INSTANCE XLXI_3 2096 912 R0
        INSTANCE XLXI_23 1808 688 R0
        BUSTAP 1856 336 1952 336
        BUSTAP 1856 400 1952 400
        BUSTAP 1856 464 1952 464
        BUSTAP 1856 528 1952 528
        BEGIN BRANCH CNT(3:0)
            WIRE 1696 432 1760 432
            WIRE 1760 432 1856 432
            WIRE 1856 432 1856 464
            WIRE 1856 464 1856 528
            WIRE 1856 336 1856 400
            WIRE 1856 400 1856 432
            BEGIN DISPLAY 1760 432 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN INSTANCE XLXI_6 1312 720 R0
        END INSTANCE
        INSTANCE XLXI_24 1520 912 R0
        BUSTAP 1344 848 1440 848
        BUSTAP 1344 784 1440 784
        BEGIN BRANCH O(1:0)
            WIRE 1200 752 1232 752
            WIRE 1232 688 1232 752
            WIRE 1232 688 1280 688
            WIRE 1280 688 1312 688
            WIRE 1280 688 1280 784
            WIRE 1280 784 1344 784
            WIRE 1344 784 1344 848
            BEGIN DISPLAY 1280 784 ATTR Name
                ALIGNMENT SOFT-TCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_97
            WIRE 1776 816 1808 816
            WIRE 1808 656 1808 720
            WIRE 1808 720 1808 816
            WIRE 1808 720 2096 720
        END BRANCH
        BEGIN BRANCH D(2:0)
            WIRE 624 1264 816 1264
            BEGIN DISPLAY 624 1264 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_100
            WIRE 2096 880 2096 912
        END BRANCH
        INSTANCE XLXI_28 2032 1040 R0
        BEGIN BRANCH D(14:11)
            WIRE 1216 432 1264 432
            WIRE 1264 432 1312 432
            WIRE 1264 432 1264 560
            WIRE 1264 560 1312 560
            BEGIN DISPLAY 1216 432 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D(10:7)
            WIRE 1216 496 1312 496
            BEGIN DISPLAY 1216 496 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN INSTANCE XLXI_30 816 1296 R0
        END INSTANCE
        BEGIN BRANCH D(6)
            WIRE 1456 1184 1504 1184
            BEGIN DISPLAY 1456 1184 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D(5)
            WIRE 1456 1280 1504 1280
            BEGIN DISPLAY 1456 1280 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_32 1504 1216 R0
        INSTANCE XLXI_33 1504 1312 R0
        BEGIN BRANCH D(4)
            WIRE 1456 1376 1504 1376
            BEGIN DISPLAY 1456 1376 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_36 1504 1408 R0
        BEGIN BRANCH D(3)
            WIRE 1456 1472 1504 1472
            BEGIN DISPLAY 1456 1472 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_37 1504 1504 R0
        BEGIN BRANCH CT
            WIRE 1728 1184 1744 1184
        END BRANCH
        BEGIN BRANCH UP
            WIRE 1728 1280 1744 1280
        END BRANCH
        BEGIN BRANCH CE
            WIRE 1728 1376 1744 1376
        END BRANCH
        BEGIN BRANCH LD
            WIRE 1728 1472 1744 1472
        END BRANCH
        IOMARKER 1744 1184 CT R0 28
        IOMARKER 1744 1280 UP R0 28
        IOMARKER 1744 1376 CE R0 28
        IOMARKER 1744 1472 LD R0 28
        BEGIN BRANCH MODE
            WIRE 1728 1584 1744 1584
        END BRANCH
        IOMARKER 1744 1584 MODE R0 28
        INSTANCE XLXI_39 1504 1616 R0
        BEGIN BRANCH P
            WIRE 160 432 224 432
            WIRE 224 432 272 432
            WIRE 224 432 224 1104
            WIRE 224 1104 816 1104
        END BRANCH
        BEGIN BRANCH EX
            WIRE 144 512 272 512
        END BRANCH
        BEGIN BRANCH INC
            WIRE 144 592 272 592
        END BRANCH
        BEGIN BRANCH DEC
            WIRE 144 672 272 672
        END BRANCH
        BEGIN BRANCH M_S
            WIRE 144 272 256 272
            WIRE 256 272 272 272
            WIRE 256 272 256 784
            WIRE 256 784 816 784
            WIRE 256 784 256 1008
            WIRE 256 1008 816 1008
            WIRE 256 1008 256 1136
            WIRE 256 1136 816 1136
        END BRANCH
        BEGIN BRANCH RST
            WIRE 144 352 240 352
            WIRE 240 352 272 352
            WIRE 240 352 240 848
            WIRE 240 848 816 848
        END BRANCH
        INSTANCE XLXI_40 272 304 R0
        INSTANCE XLXI_42 272 384 R0
        INSTANCE XLXI_43 272 464 R0
        INSTANCE XLXI_44 272 544 R0
        INSTANCE XLXI_45 272 624 R0
        INSTANCE XLXI_46 272 704 R0
        IOMARKER 144 272 M_S R180 28
        IOMARKER 144 352 RST R180 28
        IOMARKER 160 432 P R180 28
        IOMARKER 144 512 EX R180 28
        IOMARKER 144 592 INC R180 28
        IOMARKER 144 672 DEC R180 28
        BEGIN BRANCH XLXN_185
            WIRE 496 352 656 352
            WIRE 656 352 656 752
            WIRE 656 752 816 752
            WIRE 656 752 656 1072
            WIRE 656 1072 816 1072
        END BRANCH
        BEGIN BRANCH XLXN_187
            WIRE 496 432 640 432
            WIRE 640 432 640 816
            WIRE 640 816 816 816
        END BRANCH
        BEGIN BRANCH XLXN_189
            WIRE 496 272 672 272
            WIRE 672 272 672 880
            WIRE 672 880 816 880
        END BRANCH
        BEGIN BRANCH XLXN_190
            WIRE 496 512 624 512
            WIRE 624 512 624 912
            WIRE 624 912 816 912
        END BRANCH
        BEGIN BRANCH XLXN_191
            WIRE 496 592 608 592
            WIRE 608 592 608 944
            WIRE 608 944 816 944
        END BRANCH
        BEGIN BRANCH XLXN_192
            WIRE 496 672 592 672
            WIRE 592 672 592 976
            WIRE 592 976 816 976
        END BRANCH
        INSTANCE XLXI_56 736 656 R0
        BEGIN BRANCH XLXN_198
            WIRE 800 656 800 1168
            WIRE 800 1168 816 1168
        END BRANCH
        BEGIN BRANCH GN(3:0)
            WIRE 1280 624 1312 624
            BEGIN DISPLAY 1280 624 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_55 720 1552 R0
        BEGIN BRANCH G
            WIRE 784 1040 816 1040
            WIRE 784 1040 784 1200
            WIRE 784 1200 816 1200
            WIRE 784 1200 784 1232
            WIRE 784 1232 816 1232
            WIRE 784 1232 784 1424
        END BRANCH
        BEGIN BRANCH A(3)
            WIRE 1456 1584 1504 1584
            BEGIN DISPLAY 1456 1584 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH GN(3:0)
            WIRE 800 432 832 432
            WIRE 832 432 832 464
            WIRE 832 320 832 368
            WIRE 832 368 832 416
            WIRE 832 416 832 432
            BEGIN DISPLAY 800 432 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH GN(0)
            WIRE 928 320 944 320
            WIRE 944 320 960 320
            BEGIN DISPLAY 944 320 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH GN(1)
            WIRE 928 368 944 368
            WIRE 944 368 960 368
            BEGIN DISPLAY 944 368 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH GN(2)
            WIRE 928 416 944 416
            WIRE 944 416 960 416
            BEGIN DISPLAY 944 416 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH GN(3)
            WIRE 928 464 944 464
            WIRE 944 464 960 464
            BEGIN DISPLAY 944 464 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_66 1088 480 R270
        INSTANCE XLXI_67 1088 528 R270
        INSTANCE XLXI_63 1088 384 R270
        BUSTAP 832 320 928 320
        BUSTAP 832 368 928 368
        BUSTAP 832 464 928 464
        INSTANCE XLXI_65 1088 432 R270
        BUSTAP 832 416 928 416
        BEGIN BRANCH A(3:0)
            WIRE 2656 336 2656 368
            WIRE 2656 368 2656 400
            WIRE 2656 400 2656 464
            WIRE 2656 464 2656 528
            BEGIN DISPLAY 2656 368 ATTR Name
                ALIGNMENT SOFT-TVCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D(14:0)
            WIRE 2544 1168 2576 1168
            BEGIN DISPLAY 2576 1168 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN INSTANCE XLXI_1 2080 1200 R0
        END INSTANCE
        BEGIN BRANCH A(3:0)
            WIRE 2048 1168 2080 1168
            BEGIN DISPLAY 2048 1168 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
    END SHEET
END SCHEMATIC
