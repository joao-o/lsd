VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3e"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL O(1)
        SIGNAL O(1:0)
        SIGNAL O(0)
        SIGNAL S(0)
        SIGNAL S(1)
        SIGNAL S(2)
        SIGNAL D0_0
        SIGNAL D1_0
        SIGNAL D2_0
        SIGNAL D3_0
        SIGNAL D4_0
        SIGNAL D5_0
        SIGNAL D6_0
        SIGNAL D0_1
        SIGNAL D1_1
        SIGNAL D2_1
        SIGNAL D3_1
        SIGNAL D4_1
        SIGNAL D5_1
        SIGNAL D6_1
        SIGNAL D7_1
        SIGNAL D7_0
        SIGNAL XLXN_1
        SIGNAL S(2:0)
        PORT Output O(1:0)
        PORT Input D0_0
        PORT Input D1_0
        PORT Input D2_0
        PORT Input D3_0
        PORT Input D4_0
        PORT Input D5_0
        PORT Input D6_0
        PORT Input D0_1
        PORT Input D1_1
        PORT Input D2_1
        PORT Input D3_1
        PORT Input D4_1
        PORT Input D5_1
        PORT Input D6_1
        PORT Input D7_1
        PORT Input D7_0
        PORT Input S(2:0)
        BEGIN BLOCKDEF m8_1e
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -224 96 -224 
            LINE N 0 -160 96 -160 
            LINE N 0 -96 96 -96 
            LINE N 0 -288 96 -288 
            LINE N 0 -352 96 -352 
            LINE N 0 -416 96 -416 
            LINE N 0 -544 96 -544 
            LINE N 0 -608 96 -608 
            LINE N 0 -672 96 -672 
            LINE N 0 -736 96 -736 
            LINE N 160 -160 96 -160 
            LINE N 160 -268 160 -160 
            LINE N 128 -224 96 -224 
            LINE N 128 -264 128 -224 
            LINE N 192 -96 96 -96 
            LINE N 192 -276 192 -96 
            LINE N 224 -32 96 -32 
            LINE N 224 -280 224 -32 
            LINE N 320 -512 256 -512 
            LINE N 96 -768 96 -256 
            LINE N 256 -704 96 -768 
            LINE N 256 -288 256 -704 
            LINE N 96 -256 256 -288 
            LINE N 0 -32 96 -32 
            LINE N 0 -480 96 -480 
        END BLOCKDEF
        BEGIN BLOCKDEF vcc
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -32 64 -64 
            LINE N 64 0 64 -32 
            LINE N 96 -64 32 -64 
        END BLOCKDEF
        BEGIN BLOCK XLXI_1 m8_1e
            PIN D0 D0_0
            PIN D1 D1_0
            PIN D2 D2_0
            PIN D3 D3_0
            PIN D4 D4_0
            PIN D5 D5_0
            PIN D6 D6_0
            PIN D7 D7_0
            PIN E XLXN_1
            PIN S0 S(0)
            PIN S1 S(1)
            PIN S2 S(2)
            PIN O O(0)
        END BLOCK
        BEGIN BLOCK XLXI_2 m8_1e
            PIN D0 D0_1
            PIN D1 D1_1
            PIN D2 D2_1
            PIN D3 D3_1
            PIN D4 D4_1
            PIN D5 D5_1
            PIN D6 D6_1
            PIN D7 D7_1
            PIN E XLXN_1
            PIN S0 S(0)
            PIN S1 S(1)
            PIN S2 S(2)
            PIN O O(1)
        END BLOCK
        BEGIN BLOCK XLXI_3 vcc
            PIN P XLXN_1
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 3520 2720
        INSTANCE XLXI_1 640 880 R0
        INSTANCE XLXI_2 656 1712 R0
        BEGIN BRANCH O(1)
            WIRE 976 1200 1072 1200
            WIRE 1072 1200 1120 1200
            BEGIN DISPLAY 1072 1200 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH O(1:0)
            WIRE 1216 368 1216 560
            WIRE 1216 560 1216 720
            WIRE 1216 720 1216 1200
            WIRE 1216 720 1248 720
            BEGIN DISPLAY 1216 560 ATTR Name
                ALIGNMENT SOFT-TVCENTER
            END DISPLAY
        END BRANCH
        BUSTAP 1216 368 1120 368
        BUSTAP 1216 1200 1120 1200
        BEGIN BRANCH O(0)
            WIRE 960 368 1056 368
            WIRE 1056 368 1120 368
            BEGIN DISPLAY 1056 368 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        IOMARKER 1248 720 O(1:0) R0 28
        BEGIN BRANCH D0_0
            WIRE 624 144 640 144
        END BRANCH
        BEGIN BRANCH D1_0
            WIRE 624 208 640 208
        END BRANCH
        BEGIN BRANCH D2_0
            WIRE 624 272 640 272
        END BRANCH
        BEGIN BRANCH D3_0
            WIRE 624 336 640 336
        END BRANCH
        BEGIN BRANCH D4_0
            WIRE 624 400 640 400
        END BRANCH
        BEGIN BRANCH D5_0
            WIRE 624 464 640 464
        END BRANCH
        BEGIN BRANCH D6_0
            WIRE 624 528 640 528
        END BRANCH
        BEGIN BRANCH D0_1
            WIRE 624 976 656 976
        END BRANCH
        BEGIN BRANCH D1_1
            WIRE 624 1040 656 1040
        END BRANCH
        BEGIN BRANCH D2_1
            WIRE 624 1104 656 1104
        END BRANCH
        BEGIN BRANCH D3_1
            WIRE 624 1168 656 1168
        END BRANCH
        BEGIN BRANCH D4_1
            WIRE 624 1232 656 1232
        END BRANCH
        BEGIN BRANCH D5_1
            WIRE 624 1296 656 1296
        END BRANCH
        BEGIN BRANCH D6_1
            WIRE 624 1360 656 1360
        END BRANCH
        BEGIN BRANCH D7_1
            WIRE 624 1424 656 1424
        END BRANCH
        BEGIN BRANCH S(0)
            WIRE 560 1488 640 1488
            WIRE 640 1488 656 1488
            BEGIN DISPLAY 640 1488 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(1)
            WIRE 560 1552 640 1552
            WIRE 640 1552 656 1552
            BEGIN DISPLAY 640 1552 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(2)
            WIRE 560 1616 640 1616
            WIRE 640 1616 656 1616
            BEGIN DISPLAY 640 1616 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D7_0
            WIRE 624 592 640 592
        END BRANCH
        INSTANCE XLXI_3 528 912 R0
        BEGIN BRANCH S(2)
            WIRE 560 784 624 784
            WIRE 624 784 640 784
            BEGIN DISPLAY 624 784 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(1)
            WIRE 560 720 624 720
            WIRE 624 720 640 720
            BEGIN DISPLAY 624 720 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(0)
            WIRE 560 656 624 656
            WIRE 624 656 640 656
            BEGIN DISPLAY 624 656 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(2:0)
            WIRE 400 848 464 848
            WIRE 464 848 464 1488
            WIRE 464 1488 464 1552
            WIRE 464 1552 464 1616
            WIRE 464 656 464 720
            WIRE 464 720 464 784
            WIRE 464 784 464 848
        END BRANCH
        BUSTAP 464 784 560 784
        BUSTAP 464 720 560 720
        BUSTAP 464 656 560 656
        BUSTAP 464 1488 560 1488
        BUSTAP 464 1552 560 1552
        BUSTAP 464 1616 560 1616
        IOMARKER 400 848 S(2:0) R180 28
        BEGIN BRANCH XLXN_1
            WIRE 592 912 592 944
            WIRE 592 944 640 944
            WIRE 640 944 640 1680
            WIRE 640 1680 656 1680
            WIRE 640 848 640 944
        END BRANCH
        IOMARKER 624 144 D0_0 R180 28
        IOMARKER 624 208 D1_0 R180 28
        IOMARKER 624 272 D2_0 R180 28
        IOMARKER 624 336 D3_0 R180 28
        IOMARKER 624 400 D4_0 R180 28
        IOMARKER 624 464 D5_0 R180 28
        IOMARKER 624 528 D6_0 R180 28
        IOMARKER 624 592 D7_0 R180 28
        IOMARKER 624 976 D0_1 R180 28
        IOMARKER 624 1040 D1_1 R180 28
        IOMARKER 624 1104 D2_1 R180 28
        IOMARKER 624 1168 D3_1 R180 28
        IOMARKER 624 1232 D4_1 R180 28
        IOMARKER 624 1296 D5_1 R180 28
        IOMARKER 624 1360 D6_1 R180 28
        IOMARKER 624 1424 D7_1 R180 28
    END SHEET
END SCHEMATIC
