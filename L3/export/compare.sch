VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3e"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL B(3:0)
        SIGNAL B(2)
        SIGNAL B(3)
        SIGNAL B(1)
        SIGNAL B(0)
        SIGNAL Q(3:0)
        SIGNAL XLXN_10
        SIGNAL XLXN_9
        SIGNAL XLXN_12
        SIGNAL XLXN_11
        SIGNAL XLXN_8
        SIGNAL INI
        SIGNAL Q(0)
        SIGNAL Q(1)
        SIGNAL Q(3)
        SIGNAL Q(2)
        SIGNAL eqs_0
        SIGNAL eqs_B
        PORT Input B(3:0)
        PORT Input Q(3:0)
        PORT Input INI
        PORT Output eqs_0
        PORT Output eqs_B
        BEGIN BLOCKDEF nor4
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 48 -64 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 0 -256 48 -256 
            LINE N 256 -160 216 -160 
            CIRCLE N 192 -172 216 -148 
            LINE N 112 -208 48 -208 
            ARC N 28 -208 204 -32 192 -160 112 -208 
            LINE N 112 -112 48 -112 
            LINE N 48 -256 48 -208 
            LINE N 48 -64 48 -112 
            ARC N -40 -216 72 -104 48 -112 48 -208 
            ARC N 28 -288 204 -112 112 -112 192 -160 
        END BLOCKDEF
        BEGIN BLOCKDEF xnor2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 60 -128 
            ARC N -40 -152 72 -40 48 -48 44 -144 
            ARC N -24 -152 88 -40 64 -48 64 -144 
            LINE N 128 -144 64 -144 
            LINE N 128 -48 64 -48 
            ARC N 44 -144 220 32 208 -96 128 -144 
            ARC N 44 -224 220 -48 128 -48 208 -96 
            CIRCLE N 212 -104 228 -88 
            LINE N 228 -96 256 -96 
            LINE N 60 -28 60 -28 
        END BLOCKDEF
        BEGIN BLOCKDEF and4
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 144 -112 64 -112 
            ARC N 96 -208 192 -112 144 -112 144 -208 
            LINE N 64 -208 144 -208 
            LINE N 64 -64 64 -256 
            LINE N 256 -160 192 -160 
            LINE N 0 -256 64 -256 
            LINE N 0 -192 64 -192 
            LINE N 0 -128 64 -128 
            LINE N 0 -64 64 -64 
        END BLOCKDEF
        BEGIN BLOCKDEF and2b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -48 64 -144 
            LINE N 64 -144 144 -144 
            LINE N 144 -48 64 -48 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 256 -96 192 -96 
            LINE N 0 -128 64 -128 
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
        END BLOCKDEF
        BEGIN BLOCK XLXI_5 xnor2
            PIN I0 Q(3)
            PIN I1 B(3)
            PIN O XLXN_11
        END BLOCK
        BEGIN BLOCK XLXI_6 and4
            PIN I0 XLXN_11
            PIN I1 XLXN_10
            PIN I2 XLXN_9
            PIN I3 XLXN_8
            PIN O XLXN_12
        END BLOCK
        BEGIN BLOCK XLXI_4 xnor2
            PIN I0 Q(2)
            PIN I1 B(2)
            PIN O XLXN_10
        END BLOCK
        BEGIN BLOCK XLXI_3 xnor2
            PIN I0 Q(1)
            PIN I1 B(1)
            PIN O XLXN_9
        END BLOCK
        BEGIN BLOCK XLXI_2 xnor2
            PIN I0 Q(0)
            PIN I1 B(0)
            PIN O XLXN_8
        END BLOCK
        BEGIN BLOCK XLXI_10 and2b1
            PIN I0 INI
            PIN I1 XLXN_12
            PIN O eqs_B
        END BLOCK
        BEGIN BLOCK XLXI_1 nor4
            PIN I0 Q(3)
            PIN I1 Q(2)
            PIN I2 Q(1)
            PIN I3 Q(0)
            PIN O eqs_0
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 3520 2720
        INSTANCE XLXI_5 704 1088 R0
        BEGIN BRANCH B(3:0)
            WIRE 176 400 448 400
            WIRE 448 400 448 464
            WIRE 448 464 448 608
            WIRE 448 608 448 720
            WIRE 448 720 448 784
            WIRE 448 784 448 960
            BEGIN DISPLAY 448 720 ATTR Name
                ALIGNMENT SOFT-TVCENTER
            END DISPLAY
        END BRANCH
        BUSTAP 448 784 544 784
        BUSTAP 448 960 544 960
        BUSTAP 448 608 544 608
        BUSTAP 448 464 544 464
        INSTANCE XLXI_6 1040 880 R0
        INSTANCE XLXI_3 704 736 R0
        INSTANCE XLXI_2 704 592 R0
        INSTANCE XLXI_10 1216 208 M180
        INSTANCE XLXI_1 496 400 R0
        BEGIN BRANCH B(3)
            WIRE 544 960 608 960
            WIRE 608 960 704 960
            BEGIN DISPLAY 608 960 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(1)
            WIRE 544 608 608 608
            WIRE 608 608 704 608
            BEGIN DISPLAY 608 608 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(0)
            WIRE 544 464 608 464
            WIRE 608 464 704 464
            BEGIN DISPLAY 608 464 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3:0)
            WIRE 176 96 304 96
            WIRE 304 96 304 144
            WIRE 304 144 304 208
            WIRE 304 208 304 272
            WIRE 304 272 304 336
            WIRE 304 336 304 528
            WIRE 304 528 304 672
            WIRE 304 672 304 768
            WIRE 304 768 304 848
            WIRE 304 848 304 1024
            BEGIN DISPLAY 304 768 ATTR Name
                ALIGNMENT SOFT-TVCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_10
            WIRE 976 752 1024 752
            WIRE 1024 752 1040 752
            WIRE 976 752 976 816
        END BRANCH
        BEGIN BRANCH XLXN_9
            WIRE 960 640 976 640
            WIRE 976 640 976 688
            WIRE 976 688 1040 688
        END BRANCH
        BEGIN BRANCH XLXN_12
            WIRE 1200 336 1216 336
            WIRE 1200 336 1200 400
            WIRE 1200 400 1360 400
            WIRE 1360 400 1360 720
            WIRE 1296 720 1360 720
        END BRANCH
        BEGIN BRANCH XLXN_11
            WIRE 960 992 1040 992
            WIRE 1040 816 1040 992
        END BRANCH
        BEGIN BRANCH XLXN_8
            WIRE 960 496 1040 496
            WIRE 1040 496 1040 624
        END BRANCH
        BEGIN BRANCH INI
            WIRE 912 112 1040 112
            WIRE 1040 112 1040 272
            WIRE 1040 272 1216 272
        END BRANCH
        IOMARKER 912 112 INI R180 28
        IOMARKER 176 96 Q(3:0) R180 28
        IOMARKER 176 400 B(3:0) R180 28
        BUSTAP 304 144 400 144
        BUSTAP 304 208 400 208
        BUSTAP 304 272 400 272
        BUSTAP 304 336 400 336
        BUSTAP 304 528 400 528
        BUSTAP 304 672 400 672
        BUSTAP 304 848 400 848
        BUSTAP 304 1024 400 1024
        BEGIN BRANCH Q(0)
            WIRE 400 144 416 144
            WIRE 416 144 496 144
            BEGIN DISPLAY 416 144 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 400 208 416 208
            WIRE 416 208 496 208
            BEGIN DISPLAY 416 208 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 400 272 416 272
            WIRE 416 272 496 272
            BEGIN DISPLAY 416 272 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 400 336 416 336
            WIRE 416 336 496 336
            BEGIN DISPLAY 416 336 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(0)
            WIRE 400 528 608 528
            WIRE 608 528 704 528
            BEGIN DISPLAY 608 528 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 400 672 608 672
            WIRE 608 672 704 672
            BEGIN DISPLAY 608 672 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 400 1024 608 1024
            WIRE 608 1024 704 1024
            BEGIN DISPLAY 608 1024 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH eqs_0
            WIRE 752 240 976 240
            WIRE 976 176 976 240
            WIRE 976 176 1216 176
            WIRE 1216 176 1472 176
            WIRE 1472 144 1504 144
            WIRE 1472 144 1472 176
        END BRANCH
        IOMARKER 1504 144 eqs_0 R0 28
        BEGIN BRANCH eqs_B
            WIRE 1472 304 1504 304
        END BRANCH
        IOMARKER 1504 304 eqs_B R0 28
        BEGIN BRANCH Q(2)
            WIRE 400 848 608 848
            WIRE 608 848 720 848
            BEGIN DISPLAY 608 848 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(2)
            WIRE 544 784 608 784
            WIRE 608 784 720 784
            BEGIN DISPLAY 608 784 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_4 720 912 R0
    END SHEET
END SCHEMATIC
