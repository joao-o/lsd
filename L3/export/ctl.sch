VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3e"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL B(3:0)
        SIGNAL Q(3:0)
        SIGNAL S0
        SIGNAL S1
        SIGNAL INI
        SIGNAL XLXN_71
        SIGNAL XLXN_72
        SIGNAL S2
        SIGNAL S3
        SIGNAL XLXN_85
        SIGNAL XLXN_96
        SIGNAL XLXN_83
        SIGNAL XLXN_103
        SIGNAL XLXN_104
        SIGNAL XLXN_105
        SIGNAL R2
        SIGNAL R1
        SIGNAL R3
        SIGNAL R0
        SIGNAL XLXN_125
        SIGNAL B(3)
        SIGNAL B(0)
        SIGNAL B(1)
        SIGNAL B(2)
        PORT Input B(3:0)
        PORT Input Q(3:0)
        PORT Output S0
        PORT Output S1
        PORT Input INI
        PORT Output S2
        PORT Output S3
        PORT Output R2
        PORT Output R1
        PORT Output R3
        PORT Output R0
        BEGIN BLOCKDEF xor2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 60 -128 
            LINE N 256 -96 208 -96 
            ARC N -40 -152 72 -40 48 -48 44 -144 
            ARC N -24 -152 88 -40 64 -48 64 -144 
            LINE N 128 -144 64 -144 
            LINE N 128 -48 64 -48 
            ARC N 44 -144 220 32 208 -96 128 -144 
            ARC N 44 -224 220 -48 128 -48 208 -96 
        END BLOCKDEF
        BEGIN BLOCKDEF compare
            TIMESTAMP 2014 4 28 20 2 37
            RECTANGLE N 64 -192 320 0 
            RECTANGLE N 0 -172 64 -148 
            LINE N 64 -160 0 -160 
            RECTANGLE N 0 -108 64 -84 
            LINE N 64 -96 0 -96 
            LINE N 64 -32 0 -32 
            LINE N 320 -160 384 -160 
            LINE N 320 -32 384 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF mux4x2_1
            TIMESTAMP 2014 4 28 20 51 23
            RECTANGLE N 64 -640 320 0 
            LINE N 64 -608 0 -608 
            LINE N 64 -544 0 -544 
            LINE N 64 -480 0 -480 
            LINE N 64 -416 0 -416 
            LINE N 64 -352 0 -352 
            LINE N 64 -288 0 -288 
            LINE N 64 -224 0 -224 
            LINE N 64 -160 0 -160 
            LINE N 64 -96 0 -96 
            LINE N 64 -32 0 -32 
            LINE N 320 -608 384 -608 
            LINE N 320 -416 384 -416 
            LINE N 320 -224 384 -224 
            LINE N 320 -32 384 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF or2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 28 -224 204 -48 112 -48 192 -96 
            ARC N -40 -152 72 -40 48 -48 48 -144 
            LINE N 112 -144 48 -144 
            ARC N 28 -144 204 32 192 -96 112 -144 
            LINE N 112 -48 48 -48 
        END BLOCKDEF
        BEGIN BLOCKDEF and2b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -48 64 -144 
            LINE N 64 -144 144 -144 
            LINE N 144 -48 64 -48 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 256 -96 192 -96 
            LINE N 0 -128 64 -128 
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
        END BLOCKDEF
        BEGIN BLOCK XLXI_11 compare
            PIN B(3:0) B(3:0)
            PIN Q(3:0) Q(3:0)
            PIN INI INI
            PIN eqs_0 XLXN_72
            PIN eqs_B XLXN_85
        END BLOCK
        BEGIN BLOCK XLXI_12 mux4x2_1
            PIN D1_1 B(1)
            PIN D0_1 B(2)
            PIN D1_2 B(2)
            PIN D0_2 B(3)
            PIN D0_0 B(1)
            PIN D1_0 B(0)
            PIN S0 INI
            PIN E XLXN_71
            PIN D0_3 XLXN_125
            PIN D1_3 B(3)
            PIN O_3 S3
            PIN O_2 S2
            PIN O_0 S0
            PIN O_1 S1
        END BLOCK
        BEGIN BLOCK XLXI_13 or2
            PIN I0 INI
            PIN I1 XLXN_72
            PIN O XLXN_71
        END BLOCK
        BEGIN BLOCK XLXI_14 and2b1
            PIN I0 INI
            PIN I1 XLXN_85
            PIN O XLXN_96
        END BLOCK
        BEGIN BLOCK XLXI_15 and2b1
            PIN I0 S0
            PIN I1 INI
            PIN O XLXN_83
        END BLOCK
        BEGIN BLOCK XLXI_16 or2
            PIN I0 XLXN_83
            PIN I1 XLXN_96
            PIN O R0
        END BLOCK
        BEGIN BLOCK XLXI_19 and2b1
            PIN I0 S1
            PIN I1 INI
            PIN O XLXN_105
        END BLOCK
        BEGIN BLOCK XLXI_20 or2
            PIN I0 XLXN_105
            PIN I1 XLXN_96
            PIN O R1
        END BLOCK
        BEGIN BLOCK XLXI_21 and2b1
            PIN I0 S2
            PIN I1 INI
            PIN O XLXN_103
        END BLOCK
        BEGIN BLOCK XLXI_22 or2
            PIN I0 XLXN_103
            PIN I1 XLXN_96
            PIN O R2
        END BLOCK
        BEGIN BLOCK XLXI_23 and2b1
            PIN I0 S3
            PIN I1 INI
            PIN O XLXN_104
        END BLOCK
        BEGIN BLOCK XLXI_24 or2
            PIN I0 XLXN_104
            PIN I1 XLXN_96
            PIN O R3
        END BLOCK
        BEGIN BLOCK XLXI_25 xor2
            PIN I0 B(1)
            PIN I1 B(0)
            PIN O XLXN_125
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 3520 2720
        BEGIN BRANCH B(3:0)
            WIRE 176 176 224 176
            WIRE 224 176 224 464
            WIRE 224 464 224 496
            WIRE 224 496 224 528
            WIRE 224 528 224 560
            WIRE 224 560 224 624
            WIRE 224 624 224 688
            WIRE 224 688 224 752
            WIRE 224 752 224 784
            WIRE 224 784 224 816
            WIRE 224 816 224 832
            WIRE 224 832 224 896
            WIRE 224 896 224 944
            WIRE 224 176 384 176
        END BRANCH
        BEGIN BRANCH Q(3:0)
            WIRE 176 240 384 240
        END BRANCH
        IOMARKER 176 240 Q(3:0) R180 28
        IOMARKER 176 176 B(3:0) R180 28
        IOMARKER 192 304 INI R180 28
        BEGIN INSTANCE XLXI_11 384 336 R0
        END INSTANCE
        BEGIN BRANCH S0
            WIRE 1536 496 1728 496
            WIRE 1728 496 2208 496
            WIRE 1728 448 1776 448
            WIRE 1728 448 1728 496
        END BRANCH
        BEGIN BRANCH S1
            WIRE 1536 688 1728 688
            WIRE 1728 688 2208 688
            WIRE 1728 640 1776 640
            WIRE 1728 640 1728 688
        END BRANCH
        BEGIN BRANCH INI
            WIRE 192 304 368 304
            WIRE 368 304 384 304
            WIRE 368 304 368 384
            WIRE 368 384 736 384
            WIRE 736 384 736 1008
            WIRE 736 1008 736 1104
            WIRE 736 1104 864 1104
            WIRE 736 1104 736 1232
            WIRE 736 1232 1216 1232
            WIRE 736 1008 1152 1008
            WIRE 736 384 1632 384
            WIRE 1632 384 1632 576
            WIRE 1632 576 1632 768
            WIRE 1632 768 1776 768
            WIRE 1632 768 1632 960
            WIRE 1632 960 1776 960
            WIRE 1632 576 1776 576
            WIRE 1632 384 1776 384
        END BRANCH
        BEGIN INSTANCE XLXI_12 1152 1104 R0
        END INSTANCE
        BUSTAP 224 944 320 944
        BEGIN BRANCH XLXN_71
            WIRE 1120 1072 1152 1072
        END BRANCH
        INSTANCE XLXI_13 864 1168 R0
        BEGIN BRANCH XLXN_72
            WIRE 768 176 816 176
            WIRE 816 176 816 1040
            WIRE 816 1040 864 1040
        END BRANCH
        BEGIN BRANCH S2
            WIRE 1536 880 1728 880
            WIRE 1728 880 2208 880
            WIRE 1728 832 1776 832
            WIRE 1728 832 1728 880
        END BRANCH
        BEGIN BRANCH S3
            WIRE 1536 1072 1728 1072
            WIRE 1728 1072 2208 1072
            WIRE 1728 1024 1776 1024
            WIRE 1728 1024 1728 1072
        END BRANCH
        INSTANCE XLXI_14 1216 1296 R0
        BEGIN BRANCH XLXN_85
            WIRE 768 304 784 304
            WIRE 784 304 784 1168
            WIRE 784 1168 1216 1168
        END BRANCH
        BEGIN BRANCH XLXN_96
            WIRE 1472 1200 1760 1200
            WIRE 1760 352 2064 352
            WIRE 1760 352 1760 544
            WIRE 1760 544 1760 736
            WIRE 1760 736 1760 928
            WIRE 1760 928 1760 1200
            WIRE 1760 928 2064 928
            WIRE 1760 736 2064 736
            WIRE 1760 544 2064 544
        END BRANCH
        BEGIN BRANCH XLXN_83
            WIRE 2032 416 2064 416
        END BRANCH
        INSTANCE XLXI_19 1776 704 R0
        INSTANCE XLXI_20 2064 672 R0
        INSTANCE XLXI_21 1776 896 R0
        INSTANCE XLXI_22 2064 864 R0
        INSTANCE XLXI_23 1776 1088 R0
        INSTANCE XLXI_24 2064 1056 R0
        BEGIN BRANCH XLXN_103
            WIRE 2032 800 2064 800
        END BRANCH
        BEGIN BRANCH XLXN_104
            WIRE 2032 992 2064 992
        END BRANCH
        BEGIN BRANCH XLXN_105
            WIRE 2032 608 2064 608
        END BRANCH
        BEGIN BRANCH R2
            WIRE 2320 768 2352 768
        END BRANCH
        BEGIN BRANCH R1
            WIRE 2320 576 2352 576
        END BRANCH
        BEGIN BRANCH R3
            WIRE 2320 960 2336 960
            WIRE 2336 960 2352 960
        END BRANCH
        BEGIN BRANCH R0
            WIRE 2320 384 2352 384
        END BRANCH
        IOMARKER 2208 496 S0 R0 28
        IOMARKER 2208 688 S1 R0 28
        IOMARKER 2208 1072 S3 R0 28
        IOMARKER 2208 880 S2 R0 28
        IOMARKER 2352 768 R2 R0 28
        IOMARKER 2352 576 R1 R0 28
        IOMARKER 2352 960 R3 R0 28
        IOMARKER 2352 384 R0 R0 28
        INSTANCE XLXI_16 2064 480 R0
        INSTANCE XLXI_15 1776 512 R0
        BEGIN BRANCH XLXN_125
            WIRE 672 864 1136 864
            WIRE 1136 864 1136 880
            WIRE 1136 880 1152 880
        END BRANCH
        INSTANCE XLXI_25 416 960 R0
        BEGIN BRANCH B(3)
            WIRE 320 944 336 944
            WIRE 336 944 1152 944
            BEGIN DISPLAY 336 944 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BUSTAP 224 832 320 832
        BUSTAP 224 896 320 896
        BEGIN BRANCH B(0)
            WIRE 320 832 336 832
            WIRE 336 832 416 832
            BEGIN DISPLAY 336 832 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(1)
            WIRE 320 896 336 896
            WIRE 336 896 416 896
            BEGIN DISPLAY 336 896 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BUSTAP 224 752 320 752
        BUSTAP 224 688 320 688
        BUSTAP 224 624 320 624
        BUSTAP 224 560 320 560
        BUSTAP 224 496 320 496
        BEGIN BRANCH B(0)
            WIRE 320 496 336 496
            WIRE 336 496 1152 496
            BEGIN DISPLAY 336 496 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(1)
            WIRE 320 560 336 560
            WIRE 336 560 1152 560
            BEGIN DISPLAY 336 560 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(1)
            WIRE 320 624 336 624
            WIRE 336 624 1152 624
            BEGIN DISPLAY 336 624 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(2)
            WIRE 320 688 336 688
            WIRE 336 688 1152 688
            BEGIN DISPLAY 336 688 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(2)
            WIRE 320 752 336 752
            WIRE 336 752 1152 752
            BEGIN DISPLAY 336 752 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BUSTAP 224 784 320 784
        BEGIN BRANCH B(3)
            WIRE 320 784 336 784
            WIRE 336 784 720 784
            WIRE 720 784 720 816
            WIRE 720 816 1152 816
            BEGIN DISPLAY 336 784 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
    END SHEET
END SCHEMATIC
