VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3e"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL Q(1)
        SIGNAL Q(0)
        SIGNAL Q(3)
        SIGNAL Q(2)
        SIGNAL S3
        SIGNAL R3
        SIGNAL CLK
        SIGNAL XLXN_5
        SIGNAL XLXN_6
        SIGNAL XLXN_8
        SIGNAL R2
        SIGNAL S2
        SIGNAL S1
        SIGNAL R1
        SIGNAL S0
        SIGNAL R0
        SIGNAL Q(3:0)
        SIGNAL B(3:0)
        SIGNAL INI
        PORT Output Q(1)
        PORT Output Q(0)
        PORT Output Q(3)
        PORT Output Q(2)
        PORT Input CLK
        PORT Input B(3:0)
        PORT Input INI
        BEGIN BLOCKDEF fdrs
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -128 64 -128 
            LINE N 0 -256 64 -256 
            LINE N 384 -256 320 -256 
            LINE N 0 -32 64 -32 
            LINE N 0 -352 64 -352 
            RECTANGLE N 64 -320 320 -64 
            LINE N 192 -64 192 -32 
            LINE N 192 -32 64 -32 
            LINE N 64 -112 80 -128 
            LINE N 80 -128 64 -144 
            LINE N 192 -320 192 -352 
            LINE N 192 -352 64 -352 
        END BLOCKDEF
        BEGIN BLOCKDEF xor2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 60 -128 
            LINE N 256 -96 208 -96 
            ARC N -40 -152 72 -40 48 -48 44 -144 
            ARC N -24 -152 88 -40 64 -48 64 -144 
            LINE N 128 -144 64 -144 
            LINE N 128 -48 64 -48 
            ARC N 44 -144 220 32 208 -96 128 -144 
            ARC N 44 -224 220 -48 128 -48 208 -96 
        END BLOCKDEF
        BEGIN BLOCKDEF ftrse
            TIMESTAMP 2000 1 1 10 10 10
            RECTANGLE N 64 -320 320 -64 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 0 -32 64 -32 
            LINE N 0 -256 64 -256 
            LINE N 0 -352 64 -352 
            LINE N 384 -256 320 -256 
            LINE N 192 -352 64 -352 
            LINE N 192 -320 192 -352 
            LINE N 192 -32 64 -32 
            LINE N 192 -64 192 -32 
            LINE N 80 -128 64 -144 
            LINE N 64 -112 80 -128 
        END BLOCKDEF
        BEGIN BLOCKDEF vcc
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -32 64 -64 
            LINE N 64 0 64 -32 
            LINE N 96 -64 32 -64 
        END BLOCKDEF
        BEGIN BLOCKDEF ctl
            TIMESTAMP 2014 4 28 19 52 41
            RECTANGLE N 64 -512 320 0 
            LINE N 64 -480 0 -480 
            RECTANGLE N 0 -268 64 -244 
            LINE N 64 -256 0 -256 
            RECTANGLE N 0 -44 64 -20 
            LINE N 64 -32 0 -32 
            LINE N 320 -480 384 -480 
            LINE N 320 -416 384 -416 
            LINE N 320 -352 384 -352 
            LINE N 320 -288 384 -288 
            LINE N 320 -224 384 -224 
            LINE N 320 -160 384 -160 
            LINE N 320 -96 384 -96 
            LINE N 320 -32 384 -32 
        END BLOCKDEF
        BEGIN BLOCK XLXI_80 xor2
            PIN I0 Q(1)
            PIN I1 Q(0)
            PIN O XLXN_6
        END BLOCK
        BEGIN BLOCK XLXI_86 xor2
            PIN I0 Q(3)
            PIN I1 XLXN_6
            PIN O XLXN_8
        END BLOCK
        BEGIN BLOCK XLXI_82 ftrse
            PIN C CLK
            PIN CE XLXN_5
            PIN R R3
            PIN S S3
            PIN T XLXN_8
            PIN Q Q(3)
        END BLOCK
        BEGIN BLOCK XLXI_85 vcc
            PIN P XLXN_5
        END BLOCK
        BEGIN BLOCK XLXI_48 fdrs
            PIN C CLK
            PIN D Q(3)
            PIN R R2
            PIN S S2
            PIN Q Q(2)
        END BLOCK
        BEGIN BLOCK XLXI_49 fdrs
            PIN C CLK
            PIN D Q(2)
            PIN R R1
            PIN S S1
            PIN Q Q(1)
        END BLOCK
        BEGIN BLOCK XLXI_50 fdrs
            PIN C CLK
            PIN D Q(1)
            PIN R R0
            PIN S S0
            PIN Q Q(0)
        END BLOCK
        BEGIN BLOCK XLXI_89 ctl
            PIN INI INI
            PIN B(3:0) B(3:0)
            PIN Q(3:0) Q(3:0)
            PIN S3 S3
            PIN R3 R3
            PIN S2 S2
            PIN R2 R2
            PIN S1 S1
            PIN R1 R1
            PIN S0 S0
            PIN R0 R0
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 5440 3520
        BEGIN BRANCH Q(0)
            WIRE 2448 1792 3488 1792
            WIRE 3488 1792 3488 2096
            WIRE 3488 2096 3488 2320
            WIRE 3488 2320 3504 2320
            WIRE 3408 2096 3488 2096
        END BRANCH
        IOMARKER 3504 2320 Q(0) R0 28
        IOMARKER 3504 2368 Q(1) R0 28
        IOMARKER 3504 2416 Q(2) R0 28
        IOMARKER 3504 2464 Q(3) R0 28
        BEGIN BRANCH Q(1)
            WIRE 2448 1856 2880 1856
            WIRE 2880 1856 2880 2096
            WIRE 2880 2096 2880 2368
            WIRE 2880 2368 3504 2368
            WIRE 2880 2096 3024 2096
            WIRE 2848 2096 2880 2096
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 2288 2096 2320 2096
            WIRE 2320 2096 2320 2416
            WIRE 2320 2416 3504 2416
            WIRE 2320 2096 2464 2096
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 1632 1888 1792 1888
            WIRE 1792 1888 1792 2096
            WIRE 1792 2096 1792 2464
            WIRE 1792 2464 3504 2464
            WIRE 1792 2096 1904 2096
            WIRE 1712 2096 1792 2096
        END BRANCH
        BEGIN BRANCH S3
            WIRE 1248 2000 1328 2000
            BEGIN DISPLAY 1248 2000 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R3
            WIRE 1248 2320 1328 2320
            BEGIN DISPLAY 1248 2320 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 1088 2224 1328 2224
        END BRANCH
        BEGIN BRANCH XLXN_5
            WIRE 1024 2128 1024 2160
            WIRE 1024 2160 1328 2160
        END BRANCH
        INSTANCE XLXI_82 1328 2352 R0
        INSTANCE XLXI_85 960 2128 R0
        BEGIN BRANCH R2
            WIRE 1888 2320 1904 2320
            BEGIN DISPLAY 1888 2320 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S2
            WIRE 1888 2000 1904 2000
            BEGIN DISPLAY 1888 2000 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 1888 2224 1904 2224
            BEGIN DISPLAY 1888 2224 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_48 1904 2352 R0
        INSTANCE XLXI_49 2464 2352 R0
        BEGIN BRANCH S1
            WIRE 2400 2000 2464 2000
            BEGIN DISPLAY 2400 2000 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R1
            WIRE 2400 2320 2464 2320
            BEGIN DISPLAY 2400 2320 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 2400 2224 2464 2224
            BEGIN DISPLAY 2400 2224 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_50 3024 2352 R0
        BEGIN BRANCH S0
            WIRE 2960 2000 2976 2000
            WIRE 2976 2000 3024 2000
            BEGIN DISPLAY 2960 2000 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R0
            WIRE 2960 2320 2976 2320
            WIRE 2976 2320 3024 2320
            BEGIN DISPLAY 2960 2320 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 2960 2224 2976 2224
            WIRE 2976 2224 3024 2224
            BEGIN DISPLAY 2960 2224 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        IOMARKER 1088 2224 CLK R180 28
        BEGIN BRANCH XLXN_6
            WIRE 1632 1824 2192 1824
        END BRANCH
        INSTANCE XLXI_86 1632 1952 M0
        BEGIN BRANCH XLXN_8
            WIRE 1312 1856 1376 1856
            WIRE 1312 1856 1312 2096
            WIRE 1312 2096 1328 2096
        END BRANCH
        INSTANCE XLXI_80 2448 1920 M0
        BEGIN BRANCH Q(3:0)
            WIRE 1488 1728 1504 1728
            WIRE 1504 1728 1584 1728
            BEGIN DISPLAY 1488 1728 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(3:0)
            WIRE 1504 1504 1520 1504
            WIRE 1520 1504 1584 1504
        END BRANCH
        BEGIN BRANCH INI
            WIRE 1456 1280 1472 1280
            WIRE 1472 1280 1584 1280
        END BRANCH
        BEGIN INSTANCE XLXI_89 1584 1760 R0
        END INSTANCE
        IOMARKER 1504 1504 B(3:0) R180 28
        IOMARKER 1456 1280 INI R180 28
        BEGIN BRANCH S3
            WIRE 1968 1280 2080 1280
            BEGIN DISPLAY 2080 1280 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R3
            WIRE 1968 1344 2080 1344
            BEGIN DISPLAY 2080 1344 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S2
            WIRE 1968 1408 2080 1408
            BEGIN DISPLAY 2080 1408 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R2
            WIRE 1968 1472 2080 1472
            BEGIN DISPLAY 2080 1472 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S1
            WIRE 1968 1536 2080 1536
            BEGIN DISPLAY 2080 1536 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R1
            WIRE 1968 1600 2080 1600
            BEGIN DISPLAY 2080 1600 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S0
            WIRE 1968 1664 2080 1664
            BEGIN DISPLAY 2080 1664 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R0
            WIRE 1968 1728 2080 1728
            BEGIN DISPLAY 2080 1728 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
    END SHEET
END SCHEMATIC
