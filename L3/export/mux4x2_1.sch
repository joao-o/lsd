VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3e"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL D1_1
        SIGNAL D0_1
        SIGNAL D1_2
        SIGNAL D0_2
        SIGNAL D0_0
        SIGNAL D1_0
        SIGNAL O_3
        SIGNAL O_2
        SIGNAL O_0
        SIGNAL S0
        SIGNAL E
        SIGNAL O_1
        SIGNAL D0_3
        SIGNAL D1_3
        PORT Input D1_1
        PORT Input D0_1
        PORT Input D1_2
        PORT Input D0_2
        PORT Input D0_0
        PORT Input D1_0
        PORT Output O_3
        PORT Output O_2
        PORT Output O_0
        PORT Input S0
        PORT Input E
        PORT Output O_1
        PORT Input D0_3
        PORT Input D1_3
        BEGIN BLOCKDEF m2_1e
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -96 96 -96 
            LINE N 0 -32 96 -32 
            LINE N 208 -32 92 -32 
            LINE N 208 -152 208 -32 
            LINE N 144 -96 96 -96 
            LINE N 144 -136 144 -96 
            LINE N 96 -128 96 -256 
            LINE N 256 -160 96 -128 
            LINE N 256 -224 256 -160 
            LINE N 96 -256 256 -224 
            LINE N 320 -192 256 -192 
            LINE N 0 -224 96 -224 
            LINE N 0 -160 96 -160 
        END BLOCKDEF
        BEGIN BLOCK XLXI_1 m2_1e
            PIN D0 D0_3
            PIN D1 D1_3
            PIN E E
            PIN S0 S0
            PIN O O_3
        END BLOCK
        BEGIN BLOCK XLXI_2 m2_1e
            PIN D0 D0_2
            PIN D1 D1_2
            PIN E E
            PIN S0 S0
            PIN O O_2
        END BLOCK
        BEGIN BLOCK XLXI_3 m2_1e
            PIN D0 D0_1
            PIN D1 D1_1
            PIN E E
            PIN S0 S0
            PIN O O_1
        END BLOCK
        BEGIN BLOCK XLXI_4 m2_1e
            PIN D0 D0_0
            PIN D1 D1_0
            PIN E E
            PIN S0 S0
            PIN O O_0
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 3520 2720
        INSTANCE XLXI_1 624 416 R0
        BEGIN BRANCH O_3
            WIRE 944 224 992 224
        END BRANCH
        BEGIN BRANCH O_2
            WIRE 944 512 992 512
        END BRANCH
        BEGIN BRANCH D1_2
            WIRE 256 544 624 544
        END BRANCH
        BEGIN BRANCH D0_2
            WIRE 256 480 624 480
        END BRANCH
        BEGIN BRANCH D1_1
            WIRE 256 832 624 832
        END BRANCH
        BEGIN BRANCH D0_1
            WIRE 256 768 624 768
        END BRANCH
        BEGIN BRANCH O_0
            WIRE 928 1088 944 1088
            WIRE 944 1088 976 1088
        END BRANCH
        BEGIN BRANCH D0_0
            WIRE 256 1056 608 1056
        END BRANCH
        BEGIN BRANCH D1_0
            WIRE 256 1120 608 1120
        END BRANCH
        BEGIN BRANCH S0
            WIRE 240 1184 592 1184
            WIRE 592 1184 608 1184
            WIRE 592 320 624 320
            WIRE 592 320 592 608
            WIRE 592 608 592 896
            WIRE 592 896 592 1184
            WIRE 592 896 624 896
            WIRE 592 608 624 608
        END BRANCH
        BEGIN BRANCH E
            WIRE 240 1248 576 1248
            WIRE 576 1248 608 1248
            WIRE 576 384 624 384
            WIRE 576 384 576 672
            WIRE 576 672 576 960
            WIRE 576 960 576 1248
            WIRE 576 960 624 960
            WIRE 576 672 624 672
        END BRANCH
        INSTANCE XLXI_4 608 1280 R0
        IOMARKER 256 1056 D0_0 R180 28
        IOMARKER 256 1120 D1_0 R180 28
        IOMARKER 240 1184 S0 R180 28
        IOMARKER 240 1248 E R180 28
        INSTANCE XLXI_3 624 992 R0
        IOMARKER 256 832 D1_1 R180 28
        IOMARKER 256 768 D0_1 R180 28
        INSTANCE XLXI_2 624 704 R0
        IOMARKER 256 544 D1_2 R180 28
        IOMARKER 256 480 D0_2 R180 28
        IOMARKER 992 512 O_2 R0 28
        IOMARKER 992 224 O_3 R0 28
        BEGIN BRANCH O_1
            WIRE 944 800 976 800
        END BRANCH
        IOMARKER 976 800 O_1 R0 28
        BEGIN BRANCH D0_3
            WIRE 256 192 608 192
            WIRE 608 192 624 192
        END BRANCH
        BEGIN BRANCH D1_3
            WIRE 256 256 608 256
            WIRE 608 256 624 256
        END BRANCH
        IOMARKER 256 256 D1_3 R180 28
        IOMARKER 256 192 D0_3 R180 28
        IOMARKER 976 1088 O_0 R0 28
    END SHEET
END SCHEMATIC
