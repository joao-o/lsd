VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3e"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL Q1
        SIGNAL Q0
        SIGNAL S3
        SIGNAL R3
        SIGNAL S2
        SIGNAL R2
        SIGNAL S1
        SIGNAL R1
        SIGNAL S0
        SIGNAL R0
        SIGNAL CLK
        SIGNAL Q3
        SIGNAL Q2
        SIGNAL B3
        SIGNAL B2
        SIGNAL B1
        SIGNAL INI
        SIGNAL B0
        SIGNAL XLXN_4
        SIGNAL XLXN_5
        SIGNAL XLXN_6
        SIGNAL XLXN_7
        SIGNAL XLXN_8
        SIGNAL XLXN_9
        SIGNAL XLXN_10
        SIGNAL XLXN_11
        SIGNAL XLXN_12
        SIGNAL XLXN_13
        PORT Output Q1
        PORT Output Q0
        PORT Input CLK
        PORT Output Q3
        PORT Output Q2
        PORT Input B3
        PORT Input B2
        PORT Input B1
        PORT Input INI
        PORT Input B0
        BEGIN BLOCKDEF fdrs
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -128 64 -128 
            LINE N 0 -256 64 -256 
            LINE N 384 -256 320 -256 
            LINE N 0 -32 64 -32 
            LINE N 0 -352 64 -352 
            RECTANGLE N 64 -320 320 -64 
            LINE N 192 -64 192 -32 
            LINE N 192 -32 64 -32 
            LINE N 64 -112 80 -128 
            LINE N 80 -128 64 -144 
            LINE N 192 -320 192 -352 
            LINE N 192 -352 64 -352 
        END BLOCKDEF
        BEGIN BLOCKDEF and2b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -48 64 -144 
            LINE N 64 -144 144 -144 
            LINE N 144 -48 64 -48 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 256 -96 192 -96 
            LINE N 0 -128 64 -128 
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
        END BLOCKDEF
        BEGIN BLOCKDEF and2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 144 -48 64 -48 
            LINE N 64 -144 144 -144 
            LINE N 64 -48 64 -144 
        END BLOCKDEF
        BEGIN BLOCKDEF xor2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 60 -128 
            LINE N 256 -96 208 -96 
            ARC N -40 -152 72 -40 48 -48 44 -144 
            ARC N -24 -152 88 -40 64 -48 64 -144 
            LINE N 128 -144 64 -144 
            LINE N 128 -48 64 -48 
            ARC N 44 -144 220 32 208 -96 128 -144 
            ARC N 44 -224 220 -48 128 -48 208 -96 
        END BLOCKDEF
        BEGIN BLOCKDEF ftrse
            TIMESTAMP 2000 1 1 10 10 10
            RECTANGLE N 64 -320 320 -64 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 0 -32 64 -32 
            LINE N 0 -256 64 -256 
            LINE N 0 -352 64 -352 
            LINE N 384 -256 320 -256 
            LINE N 192 -352 64 -352 
            LINE N 192 -320 192 -352 
            LINE N 192 -32 64 -32 
            LINE N 192 -64 192 -32 
            LINE N 80 -128 64 -144 
            LINE N 64 -112 80 -128 
        END BLOCKDEF
        BEGIN BLOCKDEF vcc
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -32 64 -64 
            LINE N 64 0 64 -32 
            LINE N 96 -64 32 -64 
        END BLOCKDEF
        BEGIN BLOCK XLXI_48 fdrs
            PIN C CLK
            PIN D Q3
            PIN R R2
            PIN S S2
            PIN Q Q2
        END BLOCK
        BEGIN BLOCK XLXI_49 fdrs
            PIN C CLK
            PIN D Q2
            PIN R R1
            PIN S S1
            PIN Q Q1
        END BLOCK
        BEGIN BLOCK XLXI_50 fdrs
            PIN C CLK
            PIN D Q1
            PIN R R0
            PIN S S0
            PIN Q Q0
        END BLOCK
        BEGIN BLOCK XLXI_80 xor2
            PIN I0 Q1
            PIN I1 Q0
            PIN O XLXN_6
        END BLOCK
        BEGIN BLOCK XLXI_68 and2b1
            PIN I0 B3
            PIN I1 INI
            PIN O R3
        END BLOCK
        BEGIN BLOCK XLXI_69 and2
            PIN I0 INI
            PIN I1 B3
            PIN O S3
        END BLOCK
        BEGIN BLOCK XLXI_70 and2
            PIN I0 INI
            PIN I1 B2
            PIN O S2
        END BLOCK
        BEGIN BLOCK XLXI_71 and2b1
            PIN I0 B2
            PIN I1 INI
            PIN O R2
        END BLOCK
        BEGIN BLOCK XLXI_72 and2b1
            PIN I0 B1
            PIN I1 INI
            PIN O R1
        END BLOCK
        BEGIN BLOCK XLXI_73 and2
            PIN I0 INI
            PIN I1 B1
            PIN O S1
        END BLOCK
        BEGIN BLOCK XLXI_76 and2
            PIN I0 INI
            PIN I1 B0
            PIN O S0
        END BLOCK
        BEGIN BLOCK XLXI_77 and2b1
            PIN I0 B0
            PIN I1 INI
            PIN O R0
        END BLOCK
        BEGIN BLOCK XLXI_82 ftrse
            PIN C CLK
            PIN CE XLXN_5
            PIN R R3
            PIN S S3
            PIN T XLXN_8
            PIN Q Q3
        END BLOCK
        BEGIN BLOCK XLXI_85 vcc
            PIN P XLXN_5
        END BLOCK
        BEGIN BLOCK XLXI_86 xor2
            PIN I0 Q3
            PIN I1 XLXN_6
            PIN O XLXN_8
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 5440 3520
        BEGIN BRANCH Q1
            WIRE 2432 1504 3440 1504
            WIRE 3440 1504 3440 1936
            WIRE 3440 1936 3680 1936
            WIRE 3440 1936 3440 2432
            WIRE 3440 2432 4496 2432
            WIRE 3232 1936 3440 1936
        END BRANCH
        INSTANCE XLXI_48 2064 2192 R0
        INSTANCE XLXI_49 2848 2192 R0
        INSTANCE XLXI_50 3680 2192 R0
        BEGIN BRANCH Q0
            WIRE 2432 1440 4128 1440
            WIRE 4128 1440 4128 1936
            WIRE 4128 1936 4128 2320
            WIRE 4128 2320 4496 2320
            WIRE 4064 1936 4128 1936
        END BRANCH
        BEGIN BRANCH S3
            WIRE 1248 1840 1328 1840
            BEGIN DISPLAY 1248 1840 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R3
            WIRE 1248 2160 1328 2160
            BEGIN DISPLAY 1248 2160 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R2
            WIRE 2000 2160 2064 2160
            BEGIN DISPLAY 2000 2160 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S2
            WIRE 2000 1840 2064 1840
            BEGIN DISPLAY 2000 1840 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S1
            WIRE 2784 1840 2848 1840
            BEGIN DISPLAY 2784 1840 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R1
            WIRE 2784 2160 2848 2160
            BEGIN DISPLAY 2784 2160 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S0
            WIRE 3616 1840 3680 1840
            BEGIN DISPLAY 3616 1840 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R0
            WIRE 3616 2160 3680 2160
            BEGIN DISPLAY 3616 2160 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 3616 2064 3680 2064
            BEGIN DISPLAY 3616 2064 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 2784 2064 2848 2064
            BEGIN DISPLAY 2784 2064 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 2000 2064 2064 2064
            BEGIN DISPLAY 2000 2064 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 864 2064 1328 2064
        END BRANCH
        BEGIN BRANCH Q3
            WIRE 1136 1536 1136 1568
            WIRE 1136 1536 1792 1536
            WIRE 1792 1536 1792 1936
            WIRE 1792 1936 2064 1936
            WIRE 1792 1936 1792 2688
            WIRE 1792 2688 4496 2688
            WIRE 1712 1936 1792 1936
        END BRANCH
        IOMARKER 864 2064 CLK R180 28
        IOMARKER 4496 2560 Q2 R0 28
        IOMARKER 4496 2432 Q1 R0 28
        IOMARKER 4496 2320 Q0 R0 28
        IOMARKER 4496 2688 Q3 R0 28
        BEGIN BRANCH Q2
            WIRE 2448 1936 2624 1936
            WIRE 2624 1936 2848 1936
            WIRE 2624 1936 2624 2560
            WIRE 2624 2560 4496 2560
        END BRANCH
        INSTANCE XLXI_68 1152 1008 R90
        INSTANCE XLXI_69 1024 1008 R90
        BEGIN BRANCH B3
            WIRE 1152 832 1152 976
            WIRE 1152 976 1152 1008
            WIRE 1152 976 1216 976
            WIRE 1216 976 1216 1008
        END BRANCH
        INSTANCE XLXI_70 1488 1008 R90
        INSTANCE XLXI_71 1616 1008 R90
        BEGIN BRANCH B2
            WIRE 1616 832 1616 976
            WIRE 1616 976 1616 1008
            WIRE 1616 976 1680 976
            WIRE 1680 976 1680 1008
        END BRANCH
        INSTANCE XLXI_72 2080 1008 R90
        INSTANCE XLXI_73 1952 1008 R90
        BEGIN BRANCH B1
            WIRE 2080 832 2080 976
            WIRE 2080 976 2080 1008
            WIRE 2080 976 2144 976
            WIRE 2144 976 2144 1008
        END BRANCH
        INSTANCE XLXI_76 2432 1008 R90
        INSTANCE XLXI_77 2560 1008 R90
        BEGIN BRANCH INI
            WIRE 832 928 1088 928
            WIRE 1088 928 1088 1008
            WIRE 1088 928 1280 928
            WIRE 1280 928 1280 1008
            WIRE 1280 928 1552 928
            WIRE 1552 928 1552 1008
            WIRE 1552 928 1744 928
            WIRE 1744 928 1744 1008
            WIRE 1744 928 2016 928
            WIRE 2016 928 2208 928
            WIRE 2208 928 2208 1008
            WIRE 2208 928 2496 928
            WIRE 2496 928 2496 1008
            WIRE 2496 928 2688 928
            WIRE 2688 928 2688 1008
            WIRE 2016 928 2016 1008
        END BRANCH
        BEGIN BRANCH B0
            WIRE 2560 832 2560 976
            WIRE 2560 976 2560 1008
            WIRE 2560 976 2624 976
            WIRE 2624 976 2624 1008
        END BRANCH
        BEGIN BRANCH S3
            WIRE 1120 1264 1120 1360
            BEGIN DISPLAY 1120 1360 ATTR Name
                ALIGNMENT SOFT-VRIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R3
            WIRE 1248 1264 1248 1360
            BEGIN DISPLAY 1248 1360 ATTR Name
                ALIGNMENT SOFT-VRIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S2
            WIRE 1584 1264 1584 1360
            BEGIN DISPLAY 1584 1360 ATTR Name
                ALIGNMENT SOFT-VRIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R2
            WIRE 1712 1264 1712 1360
            BEGIN DISPLAY 1712 1360 ATTR Name
                ALIGNMENT SOFT-VRIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S1
            WIRE 2048 1264 2048 1360
            BEGIN DISPLAY 2048 1360 ATTR Name
                ALIGNMENT SOFT-VRIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R1
            WIRE 2176 1264 2176 1360
            BEGIN DISPLAY 2176 1360 ATTR Name
                ALIGNMENT SOFT-VRIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S0
            WIRE 2528 1264 2528 1360
            BEGIN DISPLAY 2528 1360 ATTR Name
                ALIGNMENT SOFT-VRIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R0
            WIRE 2656 1264 2656 1360
            BEGIN DISPLAY 2656 1360 ATTR Name
                ALIGNMENT SOFT-VRIGHT
            END DISPLAY
        END BRANCH
        IOMARKER 832 928 INI R180 28
        IOMARKER 2560 832 B0 R270 28
        IOMARKER 2080 832 B1 R270 28
        IOMARKER 1616 832 B2 R270 28
        IOMARKER 1152 832 B3 R270 28
        INSTANCE XLXI_80 2432 1568 M0
        INSTANCE XLXI_85 736 1968 R0
        BEGIN BRANCH XLXN_5
            WIRE 800 1968 800 2000
            WIRE 800 2000 1328 2000
        END BRANCH
        INSTANCE XLXI_86 1200 1568 M90
        BEGIN BRANCH XLXN_6
            WIRE 1072 1472 2176 1472
            WIRE 1072 1472 1072 1568
        END BRANCH
        BEGIN BRANCH XLXN_8
            WIRE 1104 1824 1104 1936
            WIRE 1104 1936 1328 1936
        END BRANCH
        INSTANCE XLXI_82 1328 2192 R0
    END SHEET
END SCHEMATIC
