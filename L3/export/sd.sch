VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3e"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL clk1
        SIGNAL sw1
        SIGNAL sw2
        SIGNAL sw3
        SIGNAL sw4
        SIGNAL sw5
        SIGNAL sw6
        SIGNAL sw7
        SIGNAL sw0
        SIGNAL btn3
        SIGNAL btn2
        SIGNAL btn1
        SIGNAL btn0
        SIGNAL pressure0
        SIGNAL pressure1
        SIGNAL pressure2
        SIGNAL pressure3
        SIGNAL standard_clock
        SIGNAL slow_clock
        SIGNAL display_clock
        SIGNAL B(0)
        SIGNAL B(1)
        SIGNAL B(2)
        SIGNAL B(3)
        SIGNAL Switch4
        SIGNAL Switch5
        SIGNAL Switch6
        SIGNAL Switch7
        SIGNAL B(3:0)
        SIGNAL Q(0)
        SIGNAL XLXN_53
        SIGNAL Q(1)
        SIGNAL Q(2)
        SIGNAL Q(3)
        SIGNAL XLXN_252
        SIGNAL XLXN_59
        SIGNAL led0
        SIGNAL led7
        SIGNAL led6
        SIGNAL led5
        SIGNAL led4
        SIGNAL led3
        SIGNAL led2
        SIGNAL led1
        SIGNAL dp
        SIGNAL seg6
        SIGNAL seg5
        SIGNAL seg4
        SIGNAL seg3
        SIGNAL seg2
        SIGNAL seg1
        SIGNAL seg0
        SIGNAL an3
        SIGNAL an2
        SIGNAL an1
        SIGNAL an0
        SIGNAL Q(3:0)
        PORT Input clk1
        PORT Input sw1
        PORT Input sw2
        PORT Input sw3
        PORT Input sw4
        PORT Input sw5
        PORT Input sw6
        PORT Input sw7
        PORT Input sw0
        PORT Input btn3
        PORT Input btn2
        PORT Input btn1
        PORT Input btn0
        PORT Output led0
        PORT Output led7
        PORT Output led6
        PORT Output led5
        PORT Output led4
        PORT Output led3
        PORT Output led2
        PORT Output led1
        PORT Output dp
        PORT Output seg6
        PORT Output seg5
        PORT Output seg4
        PORT Output seg3
        PORT Output seg2
        PORT Output seg1
        PORT Output seg0
        PORT Output an3
        PORT Output an2
        PORT Output an1
        PORT Output an0
        BEGIN BLOCKDEF clkdiv
            TIMESTAMP 2009 11 12 15 8 48
            RECTANGLE N 64 -192 320 0 
            LINE N 64 -160 0 -160 
            LINE N 320 -160 384 -160 
            LINE N 320 -96 384 -96 
            LINE N 320 -32 384 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF buf
            TIMESTAMP 2001 2 2 12 51 12
            LINE N 0 -32 64 -32 
            LINE N 224 -32 128 -32 
            LINE N 64 0 128 -32 
            LINE N 128 -32 64 -64 
            LINE N 64 -64 64 0 
        END BLOCKDEF
        BEGIN BLOCKDEF disp7
            TIMESTAMP 2012 10 31 12 17 2
            LINE N 64 -288 0 -288 
            RECTANGLE N 64 -1088 480 -80 
            LINE N 64 -928 0 -928 
            LINE N 64 -960 0 -960 
            LINE N 64 -896 0 -896 
            LINE N 64 -864 0 -864 
            LINE N 64 -1008 0 -1008 
            BEGIN LINE N 192 -848 192 -1020 
                LINECOLOR 0 0 128
            END LINE
            LINE N 64 -736 0 -736 
            LINE N 64 -704 0 -704 
            LINE N 64 -672 0 -672 
            LINE N 64 -640 0 -640 
            LINE N 64 -784 0 -784 
            BEGIN LINE N 192 -796 192 -624 
                LINECOLOR 0 0 128
            END LINE
            LINE N 64 -512 0 -512 
            LINE N 64 -480 0 -480 
            LINE N 64 -448 0 -448 
            LINE N 64 -416 0 -416 
            LINE N 64 -560 0 -560 
            BEGIN LINE N 192 -572 192 -400 
                LINECOLOR 0 0 128
            END LINE
            LINE N 64 -256 0 -256 
            LINE N 64 -224 0 -224 
            LINE N 64 -192 0 -192 
            LINE N 64 -336 0 -336 
            BEGIN LINE N 192 -348 192 -176 
                LINECOLOR 0 0 128
            END LINE
            LINE N 64 -128 0 -128 
            LINE N 480 -1056 544 -1056 
            LINE N 480 -736 544 -736 
            LINE N 480 -1008 544 -1008 
            LINE N 480 -960 544 -960 
            LINE N 480 -912 544 -912 
            LINE N 480 -688 544 -688 
            LINE N 480 -640 544 -640 
            LINE N 480 -592 544 -592 
            LINE N 480 -544 544 -544 
            LINE N 480 -496 544 -496 
            LINE N 480 -448 544 -448 
            LINE N 480 -304 544 -304 
        END BLOCKDEF
        BEGIN BLOCKDEF seq2
            TIMESTAMP 2014 4 28 19 49 44
            RECTANGLE N 64 -192 320 0 
            LINE N 64 -160 0 -160 
            LINE N 64 -96 0 -96 
            RECTANGLE N 0 -44 64 -20 
            LINE N 64 -32 0 -32 
            RECTANGLE N 320 -172 384 -148 
            LINE N 320 -160 384 -160 
        END BLOCKDEF
        BEGIN BLOCKDEF vcc
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -32 64 -64 
            LINE N 64 0 64 -32 
            LINE N 96 -64 32 -64 
        END BLOCKDEF
        BEGIN BLOCKDEF gnd
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -64 64 -96 
            LINE N 76 -48 52 -48 
            LINE N 68 -32 60 -32 
            LINE N 88 -64 40 -64 
            LINE N 64 -64 64 -80 
            LINE N 64 -128 64 -96 
        END BLOCKDEF
        BEGIN BLOCK XLXI_1 clkdiv
            PIN clk clk1
            PIN clk_fast standard_clock
            PIN clk_slow slow_clock
            PIN clk_disp display_clock
        END BLOCK
        BEGIN BLOCK XLXI_20 buf
            PIN I sw1
            PIN O B(1)
        END BLOCK
        BEGIN BLOCK XLXI_21 buf
            PIN I sw2
            PIN O B(2)
        END BLOCK
        BEGIN BLOCK XLXI_22 buf
            PIN I sw3
            PIN O B(3)
        END BLOCK
        BEGIN BLOCK XLXI_23 buf
            PIN I sw4
            PIN O Switch4
        END BLOCK
        BEGIN BLOCK XLXI_24 buf
            PIN I sw5
            PIN O Switch5
        END BLOCK
        BEGIN BLOCK XLXI_25 buf
            PIN I sw6
            PIN O Switch6
        END BLOCK
        BEGIN BLOCK XLXI_26 buf
            PIN I sw7
            PIN O Switch7
        END BLOCK
        BEGIN BLOCK XLXI_27 buf
            PIN I sw0
            PIN O B(0)
        END BLOCK
        BEGIN BLOCK XLXI_29 buf
            PIN I btn0
            PIN O pressure0
        END BLOCK
        BEGIN BLOCK XLXI_30 buf
            PIN I btn1
            PIN O pressure1
        END BLOCK
        BEGIN BLOCK XLXI_31 buf
            PIN I btn2
            PIN O pressure2
        END BLOCK
        BEGIN BLOCK XLXI_33 buf
            PIN I btn3
            PIN O pressure3
        END BLOCK
        BEGIN BLOCK XLXI_35 seq2
            PIN CLK slow_clock
            PIN B(3:0) B(3:0)
            PIN INI pressure0
            PIN Q(3:0) Q(3:0)
        END BLOCK
        BEGIN BLOCK XLXI_38 gnd
            PIN G XLXN_53
        END BLOCK
        BEGIN BLOCK XLXI_37 gnd
            PIN G XLXN_252
        END BLOCK
        BEGIN BLOCK XLXI_36 vcc
            PIN P XLXN_59
        END BLOCK
        BEGIN BLOCK XLXI_9 buf
            PIN I Q(0)
            PIN O led0
        END BLOCK
        BEGIN BLOCK XLXI_8 buf
            PIN I XLXN_252
            PIN O led7
        END BLOCK
        BEGIN BLOCK XLXI_7 buf
            PIN I XLXN_252
            PIN O led6
        END BLOCK
        BEGIN BLOCK XLXI_6 buf
            PIN I XLXN_252
            PIN O led5
        END BLOCK
        BEGIN BLOCK XLXI_5 buf
            PIN I XLXN_252
            PIN O led4
        END BLOCK
        BEGIN BLOCK XLXI_4 buf
            PIN I Q(3)
            PIN O led3
        END BLOCK
        BEGIN BLOCK XLXI_3 buf
            PIN I Q(2)
            PIN O led2
        END BLOCK
        BEGIN BLOCK XLXI_2 buf
            PIN I Q(1)
            PIN O led1
        END BLOCK
        BEGIN BLOCK XLXI_34 disp7
            PIN disp4_0 XLXN_53
            PIN disp1_1 Q(1)
            PIN disp1_0 Q(0)
            PIN disp1_2 Q(2)
            PIN disp1_3 Q(3)
            PIN aceso1 XLXN_59
            PIN disp2_0 XLXN_53
            PIN disp2_1 XLXN_53
            PIN disp2_2 XLXN_53
            PIN disp2_3 XLXN_53
            PIN aceso2 XLXN_53
            PIN disp3_0 XLXN_53
            PIN disp3_1 XLXN_53
            PIN disp3_2 XLXN_53
            PIN disp3_3 XLXN_53
            PIN aceso3 XLXN_53
            PIN disp4_1 XLXN_53
            PIN disp4_2 XLXN_53
            PIN disp4_3 XLXN_53
            PIN aceso4 XLXN_53
            PIN clk display_clock
            PIN en1 an0
            PIN segm1 seg0
            PIN en2 an1
            PIN en3 an2
            PIN en4 an3
            PIN segm2 seg1
            PIN segm3 seg2
            PIN segm4 seg3
            PIN segm5 seg4
            PIN segm6 seg5
            PIN segm7 seg6
            PIN dp dp
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 7040 5440
        BEGIN INSTANCE XLXI_1 336 1456 R0
        END INSTANCE
        BEGIN BRANCH clk1
            WIRE 320 1296 336 1296
        END BRANCH
        BEGIN DISPLAY 324 1608 TEXT "Clock slow - 0,8 Hz"
            FONT 64 "Arial"
        END DISPLAY
        BEGIN DISPLAY 332 1536 TEXT "Clock fast - 55 MHz"
            FONT 64 "Arial"
        END DISPLAY
        BEGIN DISPLAY 308 748 TEXT "Botoes de pressao"
            FONT 64 "Arial"
        END DISPLAY
        BEGIN BRANCH sw1
            WIRE 304 1984 464 1984
        END BRANCH
        BEGIN BRANCH sw2
            WIRE 304 2064 464 2064
        END BRANCH
        BEGIN BRANCH sw3
            WIRE 304 2144 464 2144
        END BRANCH
        BEGIN BRANCH sw4
            WIRE 304 2224 464 2224
        END BRANCH
        BEGIN BRANCH sw5
            WIRE 304 2304 464 2304
        END BRANCH
        BEGIN BRANCH sw6
            WIRE 304 2384 464 2384
        END BRANCH
        BEGIN BRANCH sw7
            WIRE 304 2464 464 2464
        END BRANCH
        INSTANCE XLXI_20 464 2016 R0
        INSTANCE XLXI_21 464 2096 R0
        INSTANCE XLXI_22 464 2176 R0
        INSTANCE XLXI_23 464 2256 R0
        INSTANCE XLXI_24 464 2336 R0
        INSTANCE XLXI_25 464 2416 R0
        INSTANCE XLXI_26 464 2496 R0
        BEGIN BRANCH sw0
            WIRE 304 1904 464 1904
        END BRANCH
        INSTANCE XLXI_27 464 1936 R0
        BEGIN DISPLAY 256 1816 TEXT "Botoes on/off"
            FONT 64 "Arial"
        END DISPLAY
        BEGIN BRANCH btn3
            WIRE 320 1104 480 1104
        END BRANCH
        BEGIN BRANCH btn2
            WIRE 320 1040 480 1040
        END BRANCH
        BEGIN BRANCH btn1
            WIRE 320 976 480 976
        END BRANCH
        BEGIN BRANCH btn0
            WIRE 320 912 480 912
        END BRANCH
        INSTANCE XLXI_29 480 944 R0
        INSTANCE XLXI_30 480 1008 R0
        INSTANCE XLXI_31 480 1072 R0
        INSTANCE XLXI_33 480 1136 R0
        BEGIN BRANCH pressure0
            WIRE 704 912 816 912
            WIRE 816 912 1168 912
            WIRE 1168 912 1168 1424
            WIRE 1168 1424 1536 1424
            BEGIN DISPLAY 816 912 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH pressure1
            WIRE 704 976 816 976
            WIRE 816 976 1200 976
            BEGIN DISPLAY 816 976 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH pressure2
            WIRE 704 1040 816 1040
            WIRE 816 1040 1200 1040
            BEGIN DISPLAY 816 1040 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH pressure3
            WIRE 704 1104 816 1104
            WIRE 816 1104 1200 1104
            BEGIN DISPLAY 816 1104 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH standard_clock
            WIRE 720 1296 832 1296
            WIRE 832 1296 1200 1296
            BEGIN DISPLAY 832 1296 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH slow_clock
            WIRE 720 1360 832 1360
            WIRE 832 1360 1536 1360
            BEGIN DISPLAY 832 1360 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(0)
            WIRE 688 1904 816 1904
            WIRE 816 1904 1424 1904
            BEGIN DISPLAY 816 1904 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(1)
            WIRE 688 1984 816 1984
            WIRE 816 1984 1424 1984
            BEGIN DISPLAY 816 1984 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(2)
            WIRE 688 2064 816 2064
            WIRE 816 2064 1424 2064
            BEGIN DISPLAY 816 2064 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(3)
            WIRE 688 2144 816 2144
            WIRE 816 2144 1424 2144
            BEGIN DISPLAY 816 2144 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Switch4
            WIRE 688 2224 816 2224
            WIRE 816 2224 1200 2224
            BEGIN DISPLAY 816 2224 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Switch5
            WIRE 688 2304 816 2304
            WIRE 816 2304 1200 2304
            BEGIN DISPLAY 816 2304 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Switch6
            WIRE 688 2384 816 2384
            WIRE 816 2384 1200 2384
            BEGIN DISPLAY 816 2384 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Switch7
            WIRE 688 2464 816 2464
            WIRE 816 2464 1200 2464
            BEGIN DISPLAY 816 2464 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN RECTANGLE W 104 412 1024 2592 
            LINECOLOR 128 0 0
            LINESTYLE Dash
        END RECTANGLE
        BEGIN DISPLAY 372 500 TEXT "NAO ALTERAR"
            FONT 60 "Arial"
            TEXTCOLOR 128 0 0
        END DISPLAY
        BEGIN DISPLAY 104 348 TEXT ENTRADAS
            FONT 60 "Arial"
            TEXTCOLOR 128 0 0
        END DISPLAY
        IOMARKER 304 1984 sw1 R180 28
        IOMARKER 304 2064 sw2 R180 28
        IOMARKER 304 2144 sw3 R180 28
        IOMARKER 304 2224 sw4 R180 28
        IOMARKER 304 2304 sw5 R180 28
        IOMARKER 304 2384 sw6 R180 28
        IOMARKER 304 2464 sw7 R180 28
        IOMARKER 304 1904 sw0 R180 28
        IOMARKER 320 1296 clk1 R180 28
        IOMARKER 320 912 btn0 R180 28
        IOMARKER 320 976 btn1 R180 28
        IOMARKER 320 1040 btn2 R180 28
        IOMARKER 320 1104 btn3 R180 28
        BEGIN BRANCH B(3:0)
            WIRE 1520 1488 1536 1488
            WIRE 1520 1488 1520 1584
            WIRE 1520 1584 1520 1904
            WIRE 1520 1904 1520 1984
            WIRE 1520 1984 1520 2064
            WIRE 1520 2064 1520 2144
            BEGIN DISPLAY 1520 1584 ATTR Name
                ALIGNMENT SOFT-TVCENTER
            END DISPLAY
        END BRANCH
        BUSTAP 1520 1904 1424 1904
        BUSTAP 1520 1984 1424 1984
        BUSTAP 1520 2064 1424 2064
        BUSTAP 1520 2144 1424 2144
        BEGIN BRANCH display_clock
            WIRE 720 1424 832 1424
            WIRE 832 1424 832 1712
            WIRE 832 1712 2496 1712
            BEGIN DISPLAY 832 1424 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN INSTANCE XLXI_35 1536 1520 R0
        END INSTANCE
        BEGIN BRANCH Q(0)
            WIRE 2032 2144 2112 2144
            WIRE 2112 2144 2736 2144
            BEGIN DISPLAY 2112 2144 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_38 2160 1904 R0
        INSTANCE XLXI_37 2160 2864 R0
        INSTANCE XLXI_36 2160 832 R0
        BEGIN DISPLAY 3148 308 TEXT SAIDAS
            FONT 60 "Arial"
            TEXTCOLOR 128 0 0
        END DISPLAY
        BEGIN DISPLAY 2644 452 TEXT "NAO ALTERAR"
            FONT 60 "Arial"
            TEXTCOLOR 128 0 0
        END DISPLAY
        BEGIN RECTANGLE W 2372 368 3376 2816 
            LINECOLOR 128 0 0
            LINESTYLE Dash
        END RECTANGLE
        BEGIN DISPLAY 2752 2044 TEXT Leds
            FONT 64 "Arial"
        END DISPLAY
        BEGIN DISPLAY 2468 628 TEXT "Display 7 segmentos"
            FONT 64 "Arial"
        END DISPLAY
        INSTANCE XLXI_9 2736 2176 R0
        INSTANCE XLXI_8 2736 2736 R0
        INSTANCE XLXI_7 2736 2656 R0
        INSTANCE XLXI_6 2736 2576 R0
        INSTANCE XLXI_5 2736 2496 R0
        INSTANCE XLXI_4 2736 2416 R0
        INSTANCE XLXI_3 2736 2336 R0
        INSTANCE XLXI_2 2736 2256 R0
        BEGIN INSTANCE XLXI_34 2496 1840 R0
        END INSTANCE
        BEGIN BRANCH XLXN_53
            WIRE 2224 1056 2496 1056
            WIRE 2224 1056 2224 1104
            WIRE 2224 1104 2496 1104
            WIRE 2224 1104 2224 1136
            WIRE 2224 1136 2496 1136
            WIRE 2224 1136 2224 1168
            WIRE 2224 1168 2496 1168
            WIRE 2224 1168 2224 1200
            WIRE 2224 1200 2496 1200
            WIRE 2224 1200 2224 1280
            WIRE 2224 1280 2496 1280
            WIRE 2224 1280 2224 1328
            WIRE 2224 1328 2496 1328
            WIRE 2224 1328 2224 1360
            WIRE 2224 1360 2496 1360
            WIRE 2224 1360 2224 1392
            WIRE 2224 1392 2496 1392
            WIRE 2224 1392 2224 1424
            WIRE 2224 1424 2496 1424
            WIRE 2224 1424 2224 1504
            WIRE 2224 1504 2496 1504
            WIRE 2224 1504 2224 1552
            WIRE 2224 1552 2496 1552
            WIRE 2224 1552 2224 1584
            WIRE 2224 1584 2496 1584
            WIRE 2224 1584 2224 1616
            WIRE 2224 1616 2496 1616
            WIRE 2224 1616 2224 1648
            WIRE 2224 1648 2496 1648
            WIRE 2224 1648 2224 1776
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 2032 2224 2112 2224
            WIRE 2112 2224 2736 2224
            BEGIN DISPLAY 2112 2224 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 2032 2304 2112 2304
            WIRE 2112 2304 2736 2304
            BEGIN DISPLAY 2112 2304 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 2032 2384 2112 2384
            WIRE 2112 2384 2736 2384
            BEGIN DISPLAY 2112 2384 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_252
            WIRE 2224 2464 2224 2544
            WIRE 2224 2544 2736 2544
            WIRE 2224 2544 2224 2624
            WIRE 2224 2624 2736 2624
            WIRE 2224 2624 2224 2704
            WIRE 2224 2704 2736 2704
            WIRE 2224 2704 2224 2736
            WIRE 2224 2464 2736 2464
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 2032 976 2144 976
            WIRE 2144 976 2496 976
            BEGIN DISPLAY 2144 976 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 2032 944 2144 944
            WIRE 2144 944 2496 944
            BEGIN DISPLAY 2144 944 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 2032 912 2144 912
            WIRE 2144 912 2496 912
            BEGIN DISPLAY 2144 912 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(0)
            WIRE 2032 880 2144 880
            WIRE 2144 880 2496 880
            BEGIN DISPLAY 2144 880 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_59
            WIRE 2224 832 2496 832
        END BRANCH
        BEGIN BRANCH led0
            WIRE 2960 2144 3152 2144
        END BRANCH
        BEGIN BRANCH led7
            WIRE 2960 2704 3152 2704
        END BRANCH
        BEGIN BRANCH led6
            WIRE 2960 2624 3152 2624
        END BRANCH
        BEGIN BRANCH led5
            WIRE 2960 2544 3152 2544
        END BRANCH
        BEGIN BRANCH led4
            WIRE 2960 2464 3152 2464
        END BRANCH
        BEGIN BRANCH led3
            WIRE 2960 2384 3152 2384
        END BRANCH
        BEGIN BRANCH led2
            WIRE 2960 2304 3152 2304
        END BRANCH
        BEGIN BRANCH led1
            WIRE 2960 2224 3152 2224
        END BRANCH
        BEGIN BRANCH dp
            WIRE 3040 1536 3152 1536
        END BRANCH
        BEGIN BRANCH seg6
            WIRE 3040 1392 3152 1392
        END BRANCH
        BEGIN BRANCH seg5
            WIRE 3040 1344 3152 1344
        END BRANCH
        BEGIN BRANCH seg4
            WIRE 3040 1296 3152 1296
        END BRANCH
        BEGIN BRANCH seg3
            WIRE 3040 1248 3152 1248
        END BRANCH
        BEGIN BRANCH seg2
            WIRE 3040 1200 3152 1200
        END BRANCH
        BEGIN BRANCH seg1
            WIRE 3040 1152 3152 1152
        END BRANCH
        BEGIN BRANCH seg0
            WIRE 3040 1104 3152 1104
        END BRANCH
        BEGIN BRANCH an3
            WIRE 3040 928 3152 928
        END BRANCH
        BEGIN BRANCH an2
            WIRE 3040 880 3152 880
        END BRANCH
        BEGIN BRANCH an1
            WIRE 3040 832 3152 832
        END BRANCH
        BEGIN BRANCH an0
            WIRE 3040 784 3152 784
        END BRANCH
        IOMARKER 3152 2144 led0 R0 28
        IOMARKER 3152 2704 led7 R0 28
        IOMARKER 3152 2624 led6 R0 28
        IOMARKER 3152 2544 led5 R0 28
        IOMARKER 3152 2464 led4 R0 28
        IOMARKER 3152 2384 led3 R0 28
        IOMARKER 3152 2304 led2 R0 28
        IOMARKER 3152 2224 led1 R0 28
        IOMARKER 3152 1536 dp R0 28
        IOMARKER 3152 1392 seg6 R0 28
        IOMARKER 3152 1344 seg5 R0 28
        IOMARKER 3152 1296 seg4 R0 28
        IOMARKER 3152 1248 seg3 R0 28
        IOMARKER 3152 1200 seg2 R0 28
        IOMARKER 3152 1152 seg1 R0 28
        IOMARKER 3152 1104 seg0 R0 28
        IOMARKER 3152 928 an3 R0 28
        IOMARKER 3152 880 an2 R0 28
        IOMARKER 3152 832 an1 R0 28
        IOMARKER 3152 784 an0 R0 28
        BEGIN BRANCH Q(3:0)
            WIRE 1920 1360 1936 1360
            WIRE 1936 1360 1936 1616
            WIRE 1936 1616 1936 2144
            WIRE 1936 2144 1936 2224
            WIRE 1936 2224 1936 2304
            WIRE 1936 2304 1936 2384
            WIRE 1936 880 1936 912
            WIRE 1936 912 1936 944
            WIRE 1936 944 1936 976
            WIRE 1936 976 1936 1360
            BEGIN DISPLAY 1936 1616 ATTR Name
                ALIGNMENT SOFT-TVCENTER
            END DISPLAY
        END BRANCH
        BUSTAP 1936 880 2032 880
        BUSTAP 1936 912 2032 912
        BUSTAP 1936 944 2032 944
        BUSTAP 1936 976 2032 976
        BUSTAP 1936 2144 2032 2144
        BUSTAP 1936 2224 2032 2224
        BUSTAP 1936 2304 2032 2304
        BUSTAP 1936 2384 2032 2384
    END SHEET
END SCHEMATIC
