VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3e"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL XLXN_23
        SIGNAL A(3:0)
        SIGNAL A(0)
        SIGNAL A(1)
        SIGNAL A(2)
        SIGNAL XLXN_38
        SIGNAL XLXN_39
        SIGNAL XLXN_40
        SIGNAL XLXN_41
        SIGNAL B(3:0)
        SIGNAL B(0)
        SIGNAL B(1)
        SIGNAL B(2)
        SIGNAL B(3)
        SIGNAL A(3)
        SIGNAL S(3:0)
        SIGNAL S(0)
        SIGNAL S(1)
        SIGNAL S(2)
        SIGNAL S(3)
        SIGNAL CO
        PORT Input A(3:0)
        PORT Input B(3:0)
        PORT Output S(3:0)
        PORT Output CO
        BEGIN BLOCKDEF add4
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 112 -832 112 -804 
            LINE N 64 -832 112 -832 
            LINE N 0 -832 64 -832 
            LINE N 0 -192 64 -192 
            LINE N 448 -352 384 -352 
            LINE N 448 -416 384 -416 
            LINE N 448 -480 384 -480 
            LINE N 448 -544 384 -544 
            LINE N 0 -256 64 -256 
            LINE N 0 -320 64 -320 
            LINE N 0 -384 64 -384 
            LINE N 0 -512 64 -512 
            LINE N 0 -576 64 -576 
            LINE N 0 -640 64 -640 
            LINE N 0 -704 64 -704 
            LINE N 240 -64 384 -64 
            LINE N 240 -124 240 -64 
            LINE N 336 -128 336 -148 
            LINE N 384 -128 336 -128 
            LINE N 384 -736 64 -816 
            LINE N 384 -160 384 -736 
            LINE N 64 -80 384 -160 
            LINE N 64 -416 64 -80 
            LINE N 144 -448 64 -416 
            LINE N 64 -480 144 -448 
            LINE N 64 -816 64 -480 
            LINE N 448 -64 384 -64 
            LINE N 448 -128 384 -128 
        END BLOCKDEF
        BEGIN BLOCKDEF vcc
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -32 64 -64 
            LINE N 64 0 64 -32 
            LINE N 96 -64 32 -64 
        END BLOCKDEF
        BEGIN BLOCKDEF inv
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -32 64 -32 
            LINE N 224 -32 160 -32 
            LINE N 64 -64 128 -32 
            LINE N 128 -32 64 0 
            LINE N 64 0 64 -64 
            CIRCLE N 128 -48 160 -16 
        END BLOCKDEF
        BEGIN BLOCK XLXI_2 add4
            PIN A0 A(0)
            PIN A1 A(1)
            PIN A2 A(2)
            PIN A3 A(3)
            PIN B0 XLXN_38
            PIN B1 XLXN_39
            PIN B2 XLXN_40
            PIN B3 XLXN_41
            PIN CI XLXN_23
            PIN CO CO
            PIN OFL
            PIN S0 S(0)
            PIN S1 S(1)
            PIN S2 S(2)
            PIN S3 S(3)
        END BLOCK
        BEGIN BLOCK XLXI_3 vcc
            PIN P XLXN_23
        END BLOCK
        BEGIN BLOCK XLXI_4 inv
            PIN I B(0)
            PIN O XLXN_38
        END BLOCK
        BEGIN BLOCK XLXI_5 inv
            PIN I B(1)
            PIN O XLXN_39
        END BLOCK
        BEGIN BLOCK XLXI_6 inv
            PIN I B(2)
            PIN O XLXN_40
        END BLOCK
        BEGIN BLOCK XLXI_7 inv
            PIN I B(3)
            PIN O XLXN_41
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 3520 2720
        INSTANCE XLXI_2 928 1392 R0
        BEGIN BRANCH XLXN_23
            WIRE 928 528 928 544
            WIRE 928 544 928 560
        END BRANCH
        INSTANCE XLXI_3 864 528 R0
        BEGIN BRANCH S(0)
            WIRE 1376 848 1456 848
            WIRE 1456 848 1472 848
            WIRE 1472 848 1504 848
            BEGIN DISPLAY 1456 848 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(1)
            WIRE 1376 912 1456 912
            WIRE 1456 912 1472 912
            WIRE 1472 912 1504 912
            BEGIN DISPLAY 1456 912 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(2)
            WIRE 1376 976 1456 976
            WIRE 1456 976 1472 976
            WIRE 1472 976 1504 976
            BEGIN DISPLAY 1456 976 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(3)
            WIRE 1376 1040 1472 1040
            WIRE 1472 1040 1504 1040
            BEGIN DISPLAY 1472 1040 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH A(3:0)
            WIRE 496 880 576 880
            WIRE 576 880 688 880
            WIRE 688 688 688 752
            WIRE 688 752 688 816
            WIRE 688 816 688 880
            BEGIN DISPLAY 576 880 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BUSTAP 688 688 784 688
        BUSTAP 688 752 784 752
        BUSTAP 688 816 784 816
        BUSTAP 688 880 784 880
        IOMARKER 496 880 A(3:0) R180 28
        BEGIN BRANCH A(0)
            WIRE 784 688 800 688
            WIRE 800 688 816 688
            WIRE 816 688 928 688
            BEGIN DISPLAY 816 688 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH A(1)
            WIRE 784 752 800 752
            WIRE 800 752 928 752
            BEGIN DISPLAY 800 752 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH A(2)
            WIRE 784 816 800 816
            WIRE 800 816 928 816
            BEGIN DISPLAY 800 816 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_38
            WIRE 896 1008 912 1008
            WIRE 912 1008 928 1008
        END BRANCH
        INSTANCE XLXI_4 672 1040 R0
        BEGIN BRANCH XLXN_39
            WIRE 896 1072 912 1072
            WIRE 912 1072 928 1072
        END BRANCH
        INSTANCE XLXI_5 672 1104 R0
        BEGIN BRANCH XLXN_40
            WIRE 896 1136 912 1136
            WIRE 912 1136 928 1136
        END BRANCH
        INSTANCE XLXI_6 672 1168 R0
        BEGIN BRANCH XLXN_41
            WIRE 896 1200 912 1200
            WIRE 912 1200 928 1200
        END BRANCH
        INSTANCE XLXI_7 672 1232 R0
        BEGIN BRANCH B(3:0)
            WIRE 352 976 416 976
            WIRE 416 976 528 976
            WIRE 528 976 528 1008
            WIRE 528 1008 528 1072
            WIRE 528 1072 528 1136
            WIRE 528 1136 528 1200
            BEGIN DISPLAY 416 976 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BUSTAP 528 1008 624 1008
        BUSTAP 528 1072 624 1072
        BUSTAP 528 1136 624 1136
        BUSTAP 528 1200 624 1200
        IOMARKER 352 976 B(3:0) R180 28
        BEGIN BRANCH B(0)
            WIRE 624 1008 640 1008
            WIRE 640 1008 672 1008
            BEGIN DISPLAY 640 1008 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(1)
            WIRE 624 1072 640 1072
            WIRE 640 1072 672 1072
            BEGIN DISPLAY 640 1072 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(2)
            WIRE 624 1136 640 1136
            WIRE 640 1136 672 1136
            BEGIN DISPLAY 640 1136 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(3)
            WIRE 624 1200 640 1200
            WIRE 640 1200 672 1200
            BEGIN DISPLAY 640 1200 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH A(3)
            WIRE 784 880 800 880
            WIRE 800 880 928 880
            BEGIN DISPLAY 800 880 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH S(3:0)
            WIRE 1600 704 1600 752
            WIRE 1600 752 1600 848
            WIRE 1600 848 1600 912
            WIRE 1600 912 1600 976
            WIRE 1600 976 1600 1040
            WIRE 1600 704 1664 704
            BEGIN DISPLAY 1600 752 ATTR Name
                ALIGNMENT SOFT-TVCENTER
            END DISPLAY
        END BRANCH
        BUSTAP 1600 848 1504 848
        BUSTAP 1600 912 1504 912
        BUSTAP 1600 976 1504 976
        BUSTAP 1600 1040 1504 1040
        IOMARKER 1664 704 S(3:0) R0 28
        BEGIN BRANCH CO
            WIRE 1376 1328 1408 1328
            WIRE 1408 1328 1424 1328
        END BRANCH
        IOMARKER 1424 1328 CO R0 28
    END SHEET
END SCHEMATIC
