VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3e"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL clk1
        SIGNAL sw1
        SIGNAL sw2
        SIGNAL sw3
        SIGNAL sw4
        SIGNAL sw5
        SIGNAL sw6
        SIGNAL sw7
        SIGNAL sw0
        SIGNAL btn3
        SIGNAL btn2
        SIGNAL btn1
        SIGNAL btn0
        SIGNAL pressure0
        SIGNAL pressure1
        SIGNAL pressure2
        SIGNAL pressure3
        SIGNAL standard_clock
        SIGNAL slow_clock
        SIGNAL display_clock
        SIGNAL A(0)
        SIGNAL A(1)
        SIGNAL A(2)
        SIGNAL A(3)
        SIGNAL B(0)
        SIGNAL B(1)
        SIGNAL B(2)
        SIGNAL B(3)
        SIGNAL A(3:0)
        SIGNAL B(3:0)
        SIGNAL an0
        SIGNAL an1
        SIGNAL an2
        SIGNAL an3
        SIGNAL seg0
        SIGNAL seg1
        SIGNAL seg2
        SIGNAL seg3
        SIGNAL seg4
        SIGNAL seg5
        SIGNAL seg6
        SIGNAL dp
        SIGNAL led1
        SIGNAL led2
        SIGNAL led3
        SIGNAL led4
        SIGNAL led5
        SIGNAL led6
        SIGNAL led7
        SIGNAL led0
        SIGNAL R(0)
        SIGNAL R(1)
        SIGNAL R(2)
        SIGNAL R(3)
        SIGNAL Q(0)
        SIGNAL Q(1)
        SIGNAL Q(2)
        SIGNAL Q(3)
        SIGNAL XLXN_48
        SIGNAL LED(3)
        SIGNAL LED(2)
        SIGNAL LED(1)
        SIGNAL LED(0)
        SIGNAL XLXN_356
        SIGNAL R(3:0)
        SIGNAL Q(3:0)
        SIGNAL LED(3:0)
        SIGNAL XLXN_363
        PORT Input clk1
        PORT Input sw1
        PORT Input sw2
        PORT Input sw3
        PORT Input sw4
        PORT Input sw5
        PORT Input sw6
        PORT Input sw7
        PORT Input sw0
        PORT Input btn3
        PORT Input btn2
        PORT Input btn1
        PORT Input btn0
        PORT Output an0
        PORT Output an1
        PORT Output an2
        PORT Output an3
        PORT Output seg0
        PORT Output seg1
        PORT Output seg2
        PORT Output seg3
        PORT Output seg4
        PORT Output seg5
        PORT Output seg6
        PORT Output dp
        PORT Output led1
        PORT Output led2
        PORT Output led3
        PORT Output led4
        PORT Output led5
        PORT Output led6
        PORT Output led7
        PORT Output led0
        BEGIN BLOCKDEF clkdiv
            TIMESTAMP 2009 11 12 15 8 48
            RECTANGLE N 64 -192 320 0 
            LINE N 64 -160 0 -160 
            LINE N 320 -160 384 -160 
            LINE N 320 -96 384 -96 
            LINE N 320 -32 384 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF buf
            TIMESTAMP 2001 2 2 12 51 12
            LINE N 0 -32 64 -32 
            LINE N 224 -32 128 -32 
            LINE N 64 0 128 -32 
            LINE N 128 -32 64 -64 
            LINE N 64 -64 64 0 
        END BLOCKDEF
        BEGIN BLOCKDEF disp7
            TIMESTAMP 2012 10 31 12 17 2
            LINE N 64 -288 0 -288 
            RECTANGLE N 64 -1088 480 -80 
            LINE N 64 -928 0 -928 
            LINE N 64 -960 0 -960 
            LINE N 64 -896 0 -896 
            LINE N 64 -864 0 -864 
            LINE N 64 -1008 0 -1008 
            BEGIN LINE N 192 -848 192 -1020 
                LINECOLOR 0 0 128
            END LINE
            LINE N 64 -736 0 -736 
            LINE N 64 -704 0 -704 
            LINE N 64 -672 0 -672 
            LINE N 64 -640 0 -640 
            LINE N 64 -784 0 -784 
            BEGIN LINE N 192 -796 192 -624 
                LINECOLOR 0 0 128
            END LINE
            LINE N 64 -512 0 -512 
            LINE N 64 -480 0 -480 
            LINE N 64 -448 0 -448 
            LINE N 64 -416 0 -416 
            LINE N 64 -560 0 -560 
            BEGIN LINE N 192 -572 192 -400 
                LINECOLOR 0 0 128
            END LINE
            LINE N 64 -256 0 -256 
            LINE N 64 -224 0 -224 
            LINE N 64 -192 0 -192 
            LINE N 64 -336 0 -336 
            BEGIN LINE N 192 -348 192 -176 
                LINECOLOR 0 0 128
            END LINE
            LINE N 64 -128 0 -128 
            LINE N 480 -1056 544 -1056 
            LINE N 480 -736 544 -736 
            LINE N 480 -1008 544 -1008 
            LINE N 480 -960 544 -960 
            LINE N 480 -912 544 -912 
            LINE N 480 -688 544 -688 
            LINE N 480 -640 544 -640 
            LINE N 480 -592 544 -592 
            LINE N 480 -544 544 -544 
            LINE N 480 -496 544 -496 
            LINE N 480 -448 544 -448 
            LINE N 480 -304 544 -304 
        END BLOCKDEF
        BEGIN BLOCKDEF div
            TIMESTAMP 2014 5 14 0 40 25
            RECTANGLE N 64 -384 320 0 
            LINE N 64 -352 0 -352 
            RECTANGLE N 0 -268 64 -244 
            LINE N 64 -256 0 -256 
            LINE N 64 -160 0 -160 
            RECTANGLE N 0 -76 64 -52 
            LINE N 64 -64 0 -64 
            RECTANGLE N 320 -364 384 -340 
            LINE N 320 -352 384 -352 
            RECTANGLE N 320 -300 384 -276 
            LINE N 320 -288 384 -288 
            LINE N 320 -224 384 -224 
            RECTANGLE N 320 -108 384 -84 
            LINE N 320 -96 384 -96 
            LINE N 320 -32 384 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF gnd
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -64 64 -96 
            LINE N 76 -48 52 -48 
            LINE N 68 -32 60 -32 
            LINE N 88 -64 40 -64 
            LINE N 64 -64 64 -80 
            LINE N 64 -128 64 -96 
        END BLOCKDEF
        BEGIN BLOCK XLXI_1 clkdiv
            PIN clk clk1
            PIN clk_fast standard_clock
            PIN clk_slow slow_clock
            PIN clk_disp display_clock
        END BLOCK
        BEGIN BLOCK XLXI_20 buf
            PIN I sw1
            PIN O A(1)
        END BLOCK
        BEGIN BLOCK XLXI_21 buf
            PIN I sw2
            PIN O A(2)
        END BLOCK
        BEGIN BLOCK XLXI_22 buf
            PIN I sw3
            PIN O A(3)
        END BLOCK
        BEGIN BLOCK XLXI_23 buf
            PIN I sw4
            PIN O B(0)
        END BLOCK
        BEGIN BLOCK XLXI_24 buf
            PIN I sw5
            PIN O B(1)
        END BLOCK
        BEGIN BLOCK XLXI_25 buf
            PIN I sw6
            PIN O B(2)
        END BLOCK
        BEGIN BLOCK XLXI_26 buf
            PIN I sw7
            PIN O B(3)
        END BLOCK
        BEGIN BLOCK XLXI_27 buf
            PIN I sw0
            PIN O A(0)
        END BLOCK
        BEGIN BLOCK XLXI_29 buf
            PIN I btn0
            PIN O pressure0
        END BLOCK
        BEGIN BLOCK XLXI_30 buf
            PIN I btn1
            PIN O pressure1
        END BLOCK
        BEGIN BLOCK XLXI_31 buf
            PIN I btn2
            PIN O pressure2
        END BLOCK
        BEGIN BLOCK XLXI_33 buf
            PIN I btn3
            PIN O pressure3
        END BLOCK
        BEGIN BLOCK XLXI_34 disp7
            PIN disp4_0 XLXN_48
            PIN disp1_1 R(1)
            PIN disp1_0 R(0)
            PIN disp1_2 R(2)
            PIN disp1_3 R(3)
            PIN aceso1 XLXN_363
            PIN disp2_0 Q(0)
            PIN disp2_1 Q(1)
            PIN disp2_2 Q(2)
            PIN disp2_3 Q(3)
            PIN aceso2 XLXN_363
            PIN disp3_0 XLXN_48
            PIN disp3_1 XLXN_48
            PIN disp3_2 XLXN_48
            PIN disp3_3 XLXN_48
            PIN aceso3 XLXN_48
            PIN disp4_1 XLXN_48
            PIN disp4_2 XLXN_48
            PIN disp4_3 XLXN_48
            PIN aceso4 XLXN_48
            PIN clk display_clock
            PIN en1 an0
            PIN segm1 seg0
            PIN en2 an1
            PIN en3 an2
            PIN en4 an3
            PIN segm2 seg1
            PIN segm3 seg2
            PIN segm4 seg3
            PIN segm5 seg4
            PIN segm6 seg5
            PIN segm7 seg6
            PIN dp dp
        END BLOCK
        BEGIN BLOCK XLXI_2 buf
            PIN I LED(1)
            PIN O led1
        END BLOCK
        BEGIN BLOCK XLXI_3 buf
            PIN I LED(2)
            PIN O led2
        END BLOCK
        BEGIN BLOCK XLXI_4 buf
            PIN I LED(3)
            PIN O led3
        END BLOCK
        BEGIN BLOCK XLXI_5 buf
            PIN I XLXN_356
            PIN O led4
        END BLOCK
        BEGIN BLOCK XLXI_6 buf
            PIN I XLXN_356
            PIN O led5
        END BLOCK
        BEGIN BLOCK XLXI_7 buf
            PIN I XLXN_356
            PIN O led6
        END BLOCK
        BEGIN BLOCK XLXI_8 buf
            PIN I XLXN_356
            PIN O led7
        END BLOCK
        BEGIN BLOCK XLXI_9 buf
            PIN I LED(0)
            PIN O led0
        END BLOCK
        BEGIN BLOCK XLXI_35 div
            PIN CLK slow_clock
            PIN A(3:0) A(3:0)
            PIN BUTN pressure0
            PIN B(3:0) B(3:0)
            PIN R(3:0) R(3:0)
            PIN Q(3:0) Q(3:0)
            PIN OK XLXN_363
            PIN LED(3:0) LED(3:0)
            PIN DIV_0 XLXN_356
        END BLOCK
        BEGIN BLOCK XLXI_36 gnd
            PIN G XLXN_48
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 7040 5440
        BEGIN INSTANCE XLXI_1 336 1456 R0
        END INSTANCE
        BEGIN BRANCH clk1
            WIRE 320 1296 336 1296
        END BRANCH
        BEGIN DISPLAY 324 1608 TEXT "Clock slow - 0,8 Hz"
            FONT 64 "Arial"
        END DISPLAY
        BEGIN DISPLAY 332 1536 TEXT "Clock fast - 55 MHz"
            FONT 64 "Arial"
        END DISPLAY
        BEGIN DISPLAY 308 748 TEXT "Botoes de pressao"
            FONT 64 "Arial"
        END DISPLAY
        BEGIN BRANCH sw1
            WIRE 304 1984 464 1984
        END BRANCH
        BEGIN BRANCH sw2
            WIRE 304 2064 464 2064
        END BRANCH
        BEGIN BRANCH sw3
            WIRE 304 2144 464 2144
        END BRANCH
        BEGIN BRANCH sw4
            WIRE 304 2224 464 2224
        END BRANCH
        BEGIN BRANCH sw5
            WIRE 304 2304 464 2304
        END BRANCH
        BEGIN BRANCH sw6
            WIRE 304 2384 464 2384
        END BRANCH
        BEGIN BRANCH sw7
            WIRE 304 2464 464 2464
        END BRANCH
        INSTANCE XLXI_20 464 2016 R0
        INSTANCE XLXI_21 464 2096 R0
        INSTANCE XLXI_22 464 2176 R0
        INSTANCE XLXI_23 464 2256 R0
        INSTANCE XLXI_24 464 2336 R0
        INSTANCE XLXI_25 464 2416 R0
        INSTANCE XLXI_26 464 2496 R0
        BEGIN BRANCH sw0
            WIRE 304 1904 464 1904
        END BRANCH
        INSTANCE XLXI_27 464 1936 R0
        BEGIN DISPLAY 256 1816 TEXT "Botoes on/off"
            FONT 64 "Arial"
        END DISPLAY
        BEGIN BRANCH btn3
            WIRE 320 1104 480 1104
        END BRANCH
        BEGIN BRANCH btn2
            WIRE 320 1040 480 1040
        END BRANCH
        BEGIN BRANCH btn1
            WIRE 320 976 480 976
        END BRANCH
        BEGIN BRANCH btn0
            WIRE 320 912 480 912
        END BRANCH
        INSTANCE XLXI_29 480 944 R0
        INSTANCE XLXI_30 480 1008 R0
        INSTANCE XLXI_31 480 1072 R0
        INSTANCE XLXI_33 480 1136 R0
        BEGIN BRANCH pressure0
            WIRE 704 912 816 912
            WIRE 816 912 1216 912
            WIRE 1216 912 1216 1552
            WIRE 1216 1552 1504 1552
            BEGIN DISPLAY 816 912 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH pressure1
            WIRE 704 976 816 976
            WIRE 816 976 1200 976
            BEGIN DISPLAY 816 976 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH pressure2
            WIRE 704 1040 816 1040
            WIRE 816 1040 1200 1040
            BEGIN DISPLAY 816 1040 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH pressure3
            WIRE 704 1104 816 1104
            WIRE 816 1104 1200 1104
            BEGIN DISPLAY 816 1104 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH standard_clock
            WIRE 720 1296 832 1296
            WIRE 832 1296 1200 1296
            BEGIN DISPLAY 832 1296 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH slow_clock
            WIRE 720 1360 832 1360
            WIRE 832 1360 1504 1360
            BEGIN DISPLAY 832 1360 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH display_clock
            WIRE 720 1424 832 1424
            WIRE 832 1424 832 1792
            WIRE 832 1792 2304 1792
            WIRE 2304 1632 2304 1792
            WIRE 2304 1632 2496 1632
            BEGIN DISPLAY 832 1424 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH A(0)
            WIRE 688 1904 816 1904
            WIRE 816 1904 1200 1904
            WIRE 1200 1904 1264 1904
            BEGIN DISPLAY 816 1904 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH A(1)
            WIRE 688 1984 816 1984
            WIRE 816 1984 1200 1984
            WIRE 1200 1984 1264 1984
            BEGIN DISPLAY 816 1984 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH A(2)
            WIRE 688 2064 816 2064
            WIRE 816 2064 1200 2064
            WIRE 1200 2064 1264 2064
            BEGIN DISPLAY 816 2064 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH A(3)
            WIRE 688 2144 816 2144
            WIRE 816 2144 1200 2144
            WIRE 1200 2144 1264 2144
            BEGIN DISPLAY 816 2144 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(0)
            WIRE 688 2224 816 2224
            WIRE 816 2224 1200 2224
            WIRE 1200 2224 1328 2224
            BEGIN DISPLAY 816 2224 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(1)
            WIRE 688 2304 816 2304
            WIRE 816 2304 1200 2304
            WIRE 1200 2304 1328 2304
            BEGIN DISPLAY 816 2304 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(2)
            WIRE 688 2384 816 2384
            WIRE 816 2384 1200 2384
            WIRE 1200 2384 1328 2384
            BEGIN DISPLAY 816 2384 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(3)
            WIRE 688 2464 816 2464
            WIRE 816 2464 1200 2464
            WIRE 1200 2464 1328 2464
            BEGIN DISPLAY 816 2464 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN RECTANGLE W 104 412 1024 2592 
            LINECOLOR 128 0 0
            LINESTYLE Dash
        END RECTANGLE
        BEGIN DISPLAY 372 500 TEXT "NAO ALTERAR"
            FONT 60 "Arial"
            TEXTCOLOR 128 0 0
        END DISPLAY
        BEGIN DISPLAY 104 348 TEXT ENTRADAS
            FONT 60 "Arial"
            TEXTCOLOR 128 0 0
        END DISPLAY
        IOMARKER 304 1984 sw1 R180 28
        IOMARKER 304 2064 sw2 R180 28
        IOMARKER 304 2144 sw3 R180 28
        IOMARKER 304 2224 sw4 R180 28
        IOMARKER 304 2304 sw5 R180 28
        IOMARKER 304 2384 sw6 R180 28
        IOMARKER 304 2464 sw7 R180 28
        IOMARKER 304 1904 sw0 R180 28
        IOMARKER 320 1296 clk1 R180 28
        IOMARKER 320 912 btn0 R180 28
        IOMARKER 320 976 btn1 R180 28
        IOMARKER 320 1040 btn2 R180 28
        IOMARKER 320 1104 btn3 R180 28
        BEGIN DISPLAY 2924 228 TEXT SAIDAS
            FONT 60 "Arial"
            TEXTCOLOR 128 0 0
        END DISPLAY
        BEGIN BRANCH A(3:0)
            WIRE 1360 1456 1360 1904
            WIRE 1360 1904 1360 1984
            WIRE 1360 1984 1360 2064
            WIRE 1360 2064 1360 2144
            WIRE 1360 1456 1488 1456
            WIRE 1488 1456 1504 1456
            BEGIN DISPLAY 1488 1456 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(3:0)
            WIRE 1424 1648 1424 2224
            WIRE 1424 2224 1424 2304
            WIRE 1424 2304 1424 2384
            WIRE 1424 2384 1424 2464
            WIRE 1424 1648 1488 1648
            WIRE 1488 1648 1504 1648
            BEGIN DISPLAY 1488 1648 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BUSTAP 1360 1904 1264 1904
        BUSTAP 1360 1984 1264 1984
        BUSTAP 1360 2064 1264 2064
        BUSTAP 1360 2144 1264 2144
        BUSTAP 1424 2224 1328 2224
        BUSTAP 1424 2304 1328 2304
        BUSTAP 1424 2384 1328 2384
        BUSTAP 1424 2464 1328 2464
        BEGIN INSTANCE XLXI_34 2496 1760 R0
        END INSTANCE
        BEGIN BRANCH an0
            WIRE 3040 704 3152 704
        END BRANCH
        BEGIN BRANCH an1
            WIRE 3040 752 3152 752
        END BRANCH
        BEGIN BRANCH an2
            WIRE 3040 800 3152 800
        END BRANCH
        BEGIN BRANCH an3
            WIRE 3040 848 3152 848
        END BRANCH
        BEGIN BRANCH seg0
            WIRE 3040 1024 3152 1024
        END BRANCH
        BEGIN BRANCH seg1
            WIRE 3040 1072 3152 1072
        END BRANCH
        BEGIN BRANCH seg2
            WIRE 3040 1120 3152 1120
        END BRANCH
        BEGIN BRANCH seg3
            WIRE 3040 1168 3152 1168
        END BRANCH
        BEGIN BRANCH seg4
            WIRE 3040 1216 3152 1216
        END BRANCH
        BEGIN BRANCH seg5
            WIRE 3040 1264 3152 1264
        END BRANCH
        BEGIN BRANCH seg6
            WIRE 3040 1312 3152 1312
        END BRANCH
        BEGIN BRANCH dp
            WIRE 3040 1456 3152 1456
        END BRANCH
        BEGIN BRANCH led1
            WIRE 2960 2144 3152 2144
        END BRANCH
        BEGIN BRANCH led2
            WIRE 2960 2224 3152 2224
        END BRANCH
        BEGIN BRANCH led3
            WIRE 2960 2304 3152 2304
        END BRANCH
        BEGIN BRANCH led4
            WIRE 2960 2384 3152 2384
        END BRANCH
        BEGIN BRANCH led5
            WIRE 2960 2464 3152 2464
        END BRANCH
        BEGIN BRANCH led6
            WIRE 2960 2544 3152 2544
        END BRANCH
        BEGIN BRANCH led7
            WIRE 2960 2624 3152 2624
        END BRANCH
        BEGIN BRANCH led0
            WIRE 2960 2064 3152 2064
        END BRANCH
        INSTANCE XLXI_2 2736 2176 R0
        INSTANCE XLXI_3 2736 2256 R0
        INSTANCE XLXI_4 2736 2336 R0
        INSTANCE XLXI_5 2736 2416 R0
        INSTANCE XLXI_6 2736 2496 R0
        INSTANCE XLXI_7 2736 2576 R0
        INSTANCE XLXI_8 2736 2656 R0
        INSTANCE XLXI_9 2736 2096 R0
        BEGIN DISPLAY 2468 548 TEXT "Display 7 segmentos"
            FONT 64 "Arial"
        END DISPLAY
        BEGIN DISPLAY 2752 1964 TEXT Leds
            FONT 64 "Arial"
        END DISPLAY
        BEGIN BRANCH R(0)
            WIRE 2064 800 2160 800
            WIRE 2160 800 2496 800
            BEGIN DISPLAY 2160 800 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R(1)
            WIRE 2064 832 2144 832
            WIRE 2144 832 2496 832
            BEGIN DISPLAY 2144 832 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R(2)
            WIRE 2064 864 2160 864
            WIRE 2160 864 2496 864
            BEGIN DISPLAY 2160 864 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R(3)
            WIRE 2064 896 2144 896
            WIRE 2144 896 2496 896
            BEGIN DISPLAY 2144 896 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(0)
            WIRE 2144 1024 2176 1024
            WIRE 2176 1024 2496 1024
            BEGIN DISPLAY 2176 1024 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(1)
            WIRE 2144 1056 2176 1056
            WIRE 2176 1056 2496 1056
            BEGIN DISPLAY 2176 1056 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 2144 1088 2160 1088
            WIRE 2160 1088 2496 1088
            BEGIN DISPLAY 2160 1088 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 2144 1120 2176 1120
            WIRE 2176 1120 2496 1120
            BEGIN DISPLAY 2176 1120 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_48
            WIRE 2224 1200 2496 1200
            WIRE 2224 1200 2224 1248
            WIRE 2224 1248 2496 1248
            WIRE 2224 1248 2224 1280
            WIRE 2224 1280 2496 1280
            WIRE 2224 1280 2224 1312
            WIRE 2224 1312 2496 1312
            WIRE 2224 1312 2224 1344
            WIRE 2224 1344 2496 1344
            WIRE 2224 1344 2224 1424
            WIRE 2224 1424 2496 1424
            WIRE 2224 1424 2224 1472
            WIRE 2224 1472 2496 1472
            WIRE 2224 1472 2224 1504
            WIRE 2224 1504 2496 1504
            WIRE 2224 1504 2224 1536
            WIRE 2224 1536 2496 1536
            WIRE 2224 1536 2224 1568
            WIRE 2224 1568 2496 1568
            WIRE 2224 1568 2224 1600
        END BRANCH
        BEGIN RECTANGLE W 2372 288 3376 2736 
            LINECOLOR 128 0 0
            LINESTYLE Dash
        END RECTANGLE
        BEGIN DISPLAY 2644 372 TEXT "NAO ALTERAR"
            FONT 60 "Arial"
            TEXTCOLOR 128 0 0
        END DISPLAY
        BEGIN BRANCH LED(3)
            WIRE 2208 2304 2224 2304
            WIRE 2224 2304 2736 2304
            BEGIN DISPLAY 2224 2304 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH LED(2)
            WIRE 2208 2224 2224 2224
            WIRE 2224 2224 2240 2224
            WIRE 2240 2224 2736 2224
            BEGIN DISPLAY 2240 2224 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH LED(1)
            WIRE 2208 2144 2224 2144
            WIRE 2224 2144 2240 2144
            WIRE 2240 2144 2736 2144
            BEGIN DISPLAY 2240 2144 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH LED(0)
            WIRE 2208 2064 2224 2064
            WIRE 2224 2064 2256 2064
            WIRE 2256 2064 2736 2064
            BEGIN DISPLAY 2256 2064 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_356
            WIRE 1888 1680 2032 1680
            WIRE 2032 1680 2032 2384
            WIRE 2032 2384 2032 2464
            WIRE 2032 2464 2032 2544
            WIRE 2032 2544 2032 2624
            WIRE 2032 2624 2736 2624
            WIRE 2032 2544 2736 2544
            WIRE 2032 2464 2736 2464
            WIRE 2032 2384 2736 2384
        END BRANCH
        BEGIN BRANCH R(3:0)
            WIRE 1888 1360 1968 1360
            WIRE 1968 800 1968 832
            WIRE 1968 832 1968 864
            WIRE 1968 864 1968 896
            WIRE 1968 896 1968 1216
            WIRE 1968 1216 1968 1360
            BEGIN DISPLAY 1968 1216 ATTR Name
                ALIGNMENT SOFT-TVCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3:0)
            WIRE 1888 1424 1952 1424
            WIRE 1952 1424 2048 1424
            WIRE 2048 1024 2048 1056
            WIRE 2048 1056 2048 1088
            WIRE 2048 1088 2048 1120
            WIRE 2048 1120 2048 1424
            BEGIN DISPLAY 1952 1424 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BUSTAP 1968 800 2064 800
        BUSTAP 1968 832 2064 832
        BUSTAP 1968 864 2064 864
        BUSTAP 1968 896 2064 896
        BUSTAP 2048 1024 2144 1024
        BUSTAP 2048 1056 2144 1056
        BUSTAP 2048 1088 2144 1088
        BUSTAP 2048 1120 2144 1120
        BEGIN INSTANCE XLXI_35 1504 1712 R0
        END INSTANCE
        BEGIN BRANCH LED(3:0)
            WIRE 1888 1616 2112 1616
            WIRE 2112 1616 2112 1856
            WIRE 2112 1856 2112 2064
            WIRE 2112 2064 2112 2144
            WIRE 2112 2144 2112 2224
            WIRE 2112 2224 2112 2304
            BEGIN DISPLAY 2112 1856 ATTR Name
                ALIGNMENT SOFT-TVCENTER
            END DISPLAY
        END BRANCH
        BUSTAP 2112 2064 2208 2064
        BUSTAP 2112 2144 2208 2144
        BUSTAP 2112 2224 2208 2224
        BUSTAP 2112 2304 2208 2304
        BEGIN BRANCH XLXN_363
            WIRE 1888 1488 2032 1488
            WIRE 2032 752 2032 976
            WIRE 2032 976 2032 1488
            WIRE 2032 976 2496 976
            WIRE 2032 752 2496 752
        END BRANCH
        INSTANCE XLXI_36 2160 1728 R0
        IOMARKER 3152 704 an0 R0 28
        IOMARKER 3152 752 an1 R0 28
        IOMARKER 3152 800 an2 R0 28
        IOMARKER 3152 848 an3 R0 28
        IOMARKER 3152 1024 seg0 R0 28
        IOMARKER 3152 1072 seg1 R0 28
        IOMARKER 3152 1120 seg2 R0 28
        IOMARKER 3152 1168 seg3 R0 28
        IOMARKER 3152 1216 seg4 R0 28
        IOMARKER 3152 1264 seg5 R0 28
        IOMARKER 3152 1312 seg6 R0 28
        IOMARKER 3152 1456 dp R0 28
        IOMARKER 3152 2144 led1 R0 28
        IOMARKER 3152 2224 led2 R0 28
        IOMARKER 3152 2304 led3 R0 28
        IOMARKER 3152 2384 led4 R0 28
        IOMARKER 3152 2464 led5 R0 28
        IOMARKER 3152 2544 led6 R0 28
        IOMARKER 3152 2624 led7 R0 28
        IOMARKER 3152 2064 led0 R0 28
    END SHEET
END SCHEMATIC
