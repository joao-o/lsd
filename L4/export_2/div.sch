VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3e"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL CLK
        SIGNAL D(3:0)
        SIGNAL R(3)
        SIGNAL R(2)
        SIGNAL R(1)
        SIGNAL R(0)
        SIGNAL R(3:0)
        SIGNAL SUB(0)
        SIGNAL SUB(1)
        SIGNAL SUB(2)
        SIGNAL SUB(3)
        SIGNAL SUB(3:0)
        SIGNAL A(3:0)
        SIGNAL A(0)
        SIGNAL A(1)
        SIGNAL A(2)
        SIGNAL A(3)
        SIGNAL BUTN
        SIGNAL XLXN_382
        SIGNAL CE
        SIGNAL Q(3:0)
        SIGNAL Q(0)
        SIGNAL Q(1)
        SIGNAL Q(2)
        SIGNAL Q(3)
        SIGNAL CO
        SIGNAL OK
        SIGNAL CT_0
        SIGNAL CT_1
        SIGNAL XLXN_384
        SIGNAL XLXN_390
        SIGNAL XLXN_391
        SIGNAL XLXN_392
        SIGNAL XLXN_393
        SIGNAL XLXN_414
        SIGNAL LED(0)
        SIGNAL LED(1)
        SIGNAL LED(2)
        SIGNAL LED(3)
        SIGNAL LED(3:0)
        SIGNAL B(0)
        SIGNAL B(1)
        SIGNAL B(2)
        SIGNAL B(3)
        SIGNAL D(0)
        SIGNAL D(1)
        SIGNAL D(2)
        SIGNAL D(3)
        SIGNAL XLXN_342
        SIGNAL B(3:0)
        SIGNAL XLXN_569
        SIGNAL DIV_0
        SIGNAL CT_2
        PORT Input CLK
        PORT Output R(3:0)
        PORT Input A(3:0)
        PORT Input BUTN
        PORT Output Q(3:0)
        PORT Output OK
        PORT Output LED(3:0)
        PORT Input B(3:0)
        PORT Output DIV_0
        BEGIN BLOCKDEF pergunt2
            TIMESTAMP 2014 5 11 17 35 49
            LINE N 64 32 0 32 
            LINE N 64 -32 0 -32 
            LINE N 320 -96 384 -96 
            RECTANGLE N 64 -128 320 64 
        END BLOCKDEF
        BEGIN BLOCKDEF sr4rled
            TIMESTAMP 2000 1 1 10 10 10
            RECTANGLE N 64 -768 320 -64 
            LINE N 0 -384 64 -384 
            LINE N 0 -320 64 -320 
            LINE N 384 -448 320 -448 
            LINE N 384 -512 320 -512 
            LINE N 384 -576 320 -576 
            LINE N 384 -640 320 -640 
            LINE N 0 -448 64 -448 
            LINE N 0 -512 64 -512 
            LINE N 0 -576 64 -576 
            LINE N 0 -640 64 -640 
            LINE N 0 -704 64 -704 
            LINE N 0 -32 64 -32 
            LINE N 192 -32 64 -32 
            LINE N 192 -64 192 -32 
            LINE N 0 -128 64 -128 
            LINE N 80 -128 64 -144 
            LINE N 64 -112 80 -128 
            LINE N 0 -192 64 -192 
            LINE N 0 -256 64 -256 
        END BLOCKDEF
        BEGIN BLOCKDEF mux4x2_1
            TIMESTAMP 2014 5 12 22 21 58
            RECTANGLE N 64 -1228 320 -824 
            LINE N 64 -1136 0 -1136 
            LINE N 64 -1104 0 -1104 
            LINE N 64 -1072 0 -1072 
            LINE N 64 -1040 0 -1040 
            LINE N 64 -800 0 -800 
            LINE N 144 -800 64 -800 
            LINE N 144 -800 144 -824 
            LINE N 64 -976 0 -976 
            LINE N 64 -944 0 -944 
            LINE N 64 -912 0 -912 
            LINE N 64 -880 0 -880 
            LINE N 320 -1056 384 -1056 
            LINE N 320 -1024 384 -1024 
            LINE N 320 -992 384 -992 
            LINE N 320 -960 384 -960 
        END BLOCKDEF
        BEGIN BLOCKDEF gnd
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -64 64 -96 
            LINE N 76 -48 52 -48 
            LINE N 68 -32 60 -32 
            LINE N 88 -64 40 -64 
            LINE N 64 -64 64 -80 
            LINE N 64 -128 64 -96 
        END BLOCKDEF
        BEGIN BLOCKDEF vcc
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -32 64 -64 
            LINE N 64 0 64 -32 
            LINE N 96 -64 32 -64 
        END BLOCKDEF
        BEGIN BLOCKDEF cb4ce
            TIMESTAMP 2000 1 1 10 10 10
            RECTANGLE N 64 -512 320 -64 
            LINE N 0 -32 64 -32 
            LINE N 0 -128 64 -128 
            LINE N 384 -256 320 -256 
            LINE N 384 -320 320 -320 
            LINE N 384 -384 320 -384 
            LINE N 384 -448 320 -448 
            LINE N 80 -128 64 -144 
            LINE N 64 -112 80 -128 
            LINE N 384 -128 320 -128 
            LINE N 192 -32 64 -32 
            LINE N 192 -64 192 -32 
            LINE N 0 -192 64 -192 
            LINE N 384 -192 320 -192 
        END BLOCKDEF
        BEGIN BLOCKDEF or2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 192 -96 
            ARC N 28 -224 204 -48 112 -48 192 -96 
            ARC N -40 -152 72 -40 48 -48 48 -144 
            LINE N 112 -144 48 -144 
            ARC N 28 -144 204 32 192 -96 112 -144 
            LINE N 112 -48 48 -48 
        END BLOCKDEF
        BEGIN BLOCKDEF and2b1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 64 -48 64 -144 
            LINE N 64 -144 144 -144 
            LINE N 144 -48 64 -48 
            ARC N 96 -144 192 -48 144 -48 144 -144 
            LINE N 256 -96 192 -96 
            LINE N 0 -128 64 -128 
            LINE N 0 -64 40 -64 
            CIRCLE N 40 -76 64 -52 
        END BLOCKDEF
        BEGIN BLOCKDEF nor2
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 64 -64 
            LINE N 0 -128 64 -128 
            LINE N 256 -96 216 -96 
            CIRCLE N 192 -108 216 -84 
            ARC N 28 -224 204 -48 112 -48 192 -96 
            ARC N 28 -144 204 32 192 -96 112 -144 
            ARC N -40 -152 72 -40 48 -48 48 -144 
            LINE N 112 -48 48 -48 
            LINE N 112 -144 48 -144 
        END BLOCKDEF
        BEGIN BLOCKDEF subtest
            TIMESTAMP 2014 5 12 19 0 50
            RECTANGLE N 64 -128 320 0 
            RECTANGLE N 0 -108 64 -84 
            LINE N 64 -96 0 -96 
            RECTANGLE N 0 -44 64 -20 
            LINE N 64 -32 0 -32 
            RECTANGLE N 320 -108 384 -84 
            LINE N 320 -96 384 -96 
            LINE N 320 -32 384 -32 
        END BLOCKDEF
        BEGIN BLOCKDEF d2_4e
            TIMESTAMP 2000 1 1 10 10 10
            RECTANGLE N 64 -384 320 -64 
            LINE N 0 -128 64 -128 
            LINE N 0 -256 64 -256 
            LINE N 0 -320 64 -320 
            LINE N 384 -128 320 -128 
            LINE N 384 -192 320 -192 
            LINE N 384 -256 320 -256 
            LINE N 384 -320 320 -320 
        END BLOCKDEF
        BEGIN BLOCKDEF nor4
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 0 -64 48 -64 
            LINE N 0 -128 64 -128 
            LINE N 0 -192 64 -192 
            LINE N 0 -256 48 -256 
            LINE N 256 -160 216 -160 
            CIRCLE N 192 -172 216 -148 
            LINE N 112 -208 48 -208 
            ARC N 28 -208 204 -32 192 -160 112 -208 
            LINE N 112 -112 48 -112 
            LINE N 48 -256 48 -208 
            LINE N 48 -64 48 -112 
            ARC N -40 -216 72 -104 48 -112 48 -208 
            ARC N 28 -288 204 -112 112 -112 192 -160 
        END BLOCKDEF
        BEGIN BLOCK XLXI_2 sr4rled
            PIN C CLK
            PIN CE CE
            PIN D0 SUB(0)
            PIN D1 SUB(1)
            PIN D2 SUB(2)
            PIN D3 SUB(3)
            PIN L CO
            PIN LEFT XLXN_382
            PIN R XLXN_569
            PIN SLI Q(3)
            PIN SRI XLXN_342
            PIN Q0 R(0)
            PIN Q1 R(1)
            PIN Q2 R(2)
            PIN Q3 R(3)
        END BLOCK
        BEGIN BLOCK XLXI_1 pergunt2
            PIN CLK CLK
            PIN Px XLXN_569
            PIN BUTTN BUTN
        END BLOCK
        BEGIN BLOCK XLXI_16 mux4x2_1
            PIN C1 A(2)
            PIN C0 Q(2)
            PIN B1 A(1)
            PIN B0 Q(1)
            PIN D0 Q(3)
            PIN D1 A(3)
            PIN S0 XLXN_569
            PIN A0 XLXN_382
            PIN A1 A(0)
            PIN A_O XLXN_390
            PIN B_O XLXN_391
            PIN C_O XLXN_392
            PIN D_O XLXN_393
        END BLOCK
        BEGIN BLOCK XLXI_23 gnd
            PIN G XLXN_342
        END BLOCK
        BEGIN BLOCK XLXI_3 sr4rled
            PIN C CLK
            PIN CE CE
            PIN D0 XLXN_390
            PIN D1 XLXN_391
            PIN D2 XLXN_392
            PIN D3 XLXN_393
            PIN L XLXN_384
            PIN LEFT XLXN_382
            PIN R XLXN_342
            PIN SLI XLXN_342
            PIN SRI XLXN_342
            PIN Q0 Q(0)
            PIN Q1 Q(1)
            PIN Q2 Q(2)
            PIN Q3 Q(3)
        END BLOCK
        BEGIN BLOCK XLXI_29 cb4ce
            PIN C CLK
            PIN CE CE
            PIN CLR XLXN_569
            PIN CEO
            PIN Q0 CT_0
            PIN Q1 CT_1
            PIN Q2 CT_2
            PIN Q3
            PIN TC
        END BLOCK
        BEGIN BLOCK XLXI_55 or2
            PIN I0 CO
            PIN I1 XLXN_569
            PIN O XLXN_384
        END BLOCK
        BEGIN BLOCK XLXI_58 and2b1
            PIN I0 CO
            PIN I1 CT_2
            PIN O OK
        END BLOCK
        BEGIN BLOCK XLXI_59 nor2
            PIN I0 CT_2
            PIN I1 CO
            PIN O CE
        END BLOCK
        BEGIN BLOCK XLXI_24 vcc
            PIN P XLXN_382
        END BLOCK
        BEGIN BLOCK XLXI_61 subtest
            PIN A(3:0) R(3:0)
            PIN B(3:0) D(3:0)
            PIN S(3:0) SUB(3:0)
            PIN CO CO
        END BLOCK
        BEGIN BLOCK XLXI_62 d2_4e
            PIN A0 CT_0
            PIN A1 CT_1
            PIN E XLXN_414
            PIN D0 LED(0)
            PIN D1 LED(1)
            PIN D2 LED(2)
            PIN D3 LED(3)
        END BLOCK
        BEGIN BLOCK XLXI_65 vcc
            PIN P XLXN_414
        END BLOCK
        BEGIN BLOCK XLXI_5 sr4rled
            PIN C CLK
            PIN CE XLXN_342
            PIN D0 B(0)
            PIN D1 B(1)
            PIN D2 B(2)
            PIN D3 B(3)
            PIN L XLXN_569
            PIN LEFT XLXN_342
            PIN R XLXN_342
            PIN SLI XLXN_342
            PIN SRI XLXN_342
            PIN Q0 D(0)
            PIN Q1 D(1)
            PIN Q2 D(2)
            PIN Q3 D(3)
        END BLOCK
        BEGIN BLOCK XLXI_80 nor4
            PIN I0 D(3)
            PIN I1 D(2)
            PIN I2 D(1)
            PIN I3 D(0)
            PIN O DIV_0
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 3520 2720
        BEGIN BRANCH R(3:0)
            WIRE 1808 704 1840 704
            WIRE 1840 704 1856 704
            WIRE 1856 704 1872 704
            WIRE 1872 704 1904 704
            WIRE 1904 704 2048 704
            WIRE 2048 704 2752 704
            WIRE 2048 704 2048 928
            WIRE 2048 928 2144 928
            BEGIN DISPLAY 1856 704 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BUSTAP 1904 704 1904 800
        BUSTAP 1872 704 1872 800
        BUSTAP 1840 704 1840 800
        BUSTAP 1808 704 1808 800
        INSTANCE XLXI_2 1424 1552 R0
        BEGIN BRANCH SUB(0)
            WIRE 1392 912 1408 912
            WIRE 1408 912 1424 912
            BEGIN DISPLAY 1408 912 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH SUB(1)
            WIRE 1392 976 1408 976
            WIRE 1408 976 1424 976
            BEGIN DISPLAY 1408 976 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH SUB(2)
            WIRE 1392 1040 1408 1040
            WIRE 1408 1040 1424 1040
            BEGIN DISPLAY 1408 1040 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH SUB(3)
            WIRE 1392 1104 1408 1104
            WIRE 1408 1104 1424 1104
            BEGIN DISPLAY 1408 1104 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 1312 1424 1424 1424
        END BRANCH
        BUSTAP 1296 1104 1392 1104
        BUSTAP 1296 1040 1392 1040
        BUSTAP 1296 976 1392 976
        BUSTAP 1296 912 1392 912
        BEGIN BRANCH SUB(3:0)
            WIRE 1296 640 1296 912
            WIRE 1296 912 1296 976
            WIRE 1296 976 1296 1040
            WIRE 1296 1040 1296 1104
            WIRE 1296 640 2128 640
            WIRE 2128 640 2592 640
            WIRE 2592 640 2592 928
            WIRE 2528 928 2592 928
            BEGIN DISPLAY 2128 640 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 1360 2304 1424 2304
        END BRANCH
        BEGIN INSTANCE XLXI_16 816 2896 R0
        END INSTANCE
        IOMARKER 1312 1424 CLK R180 28
        IOMARKER 1360 2304 CLK R180 28
        BEGIN BRANCH A(3:0)
            WIRE 240 1776 640 1776
            WIRE 640 1776 688 1776
            WIRE 688 1776 688 1920
            WIRE 688 1920 688 1952
            WIRE 688 1952 688 1984
            WIRE 688 1984 688 2016
            BEGIN DISPLAY 640 1776 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BUSTAP 688 2016 784 2016
        BUSTAP 688 1984 784 1984
        BUSTAP 688 1952 784 1952
        BUSTAP 688 1920 784 1920
        IOMARKER 240 1776 A(3:0) R180 28
        BEGIN BRANCH A(0)
            WIRE 784 1920 800 1920
            WIRE 800 1920 816 1920
            BEGIN DISPLAY 800 1920 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH A(1)
            WIRE 784 1952 800 1952
            WIRE 800 1952 816 1952
            BEGIN DISPLAY 800 1952 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH A(2)
            WIRE 784 1984 800 1984
            WIRE 800 1984 816 1984
            BEGIN DISPLAY 800 1984 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH A(3)
            WIRE 784 2016 800 2016
            WIRE 800 2016 816 2016
            BEGIN DISPLAY 800 2016 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH BUTN
            WIRE 144 1664 192 1664
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 144 1600 192 1600
        END BRANCH
        BEGIN BRANCH CE
            WIRE 1360 2240 1424 2240
            BEGIN DISPLAY 1360 2240 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CE
            WIRE 1328 1360 1424 1360
            BEGIN DISPLAY 1328 1360 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CO
            WIRE 1312 1232 1424 1232
            BEGIN DISPLAY 1312 1232 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 2464 2096 2496 2096
        END BRANCH
        INSTANCE XLXI_29 2496 2224 R0
        IOMARKER 2464 2096 CLK R180 28
        BEGIN BRANCH CE
            WIRE 2368 1952 2432 1952
            WIRE 2432 1952 2432 2032
            WIRE 2432 2032 2496 2032
            WIRE 2432 1936 2432 1952
            BEGIN DISPLAY 2368 1952 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_55 976 2256 R0
        BEGIN BRANCH Q(0)
            WIRE 1808 1792 1824 1792
            WIRE 1824 1792 2000 1792
            BEGIN DISPLAY 1824 1792 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3:0)
            WIRE 2096 1792 2096 1856
            WIRE 2096 1856 2096 1920
            WIRE 2096 1920 2096 1984
            WIRE 2096 1984 2096 2320
            WIRE 2096 2320 2240 2320
        END BRANCH
        BEGIN BRANCH OK
            WIRE 3040 1520 3088 1520
        END BRANCH
        INSTANCE XLXI_59 2336 1680 R90
        BUSTAP 2096 1792 2000 1792
        BUSTAP 2096 1856 2000 1856
        BUSTAP 2096 1920 2000 1920
        BUSTAP 2096 1984 2000 1984
        BEGIN BRANCH Q(1)
            WIRE 752 1600 1856 1600
            WIRE 1856 1600 1856 1856
            WIRE 1856 1856 2000 1856
            WIRE 752 1600 752 1792
            WIRE 752 1792 816 1792
            WIRE 1808 1856 1824 1856
            WIRE 1824 1856 1856 1856
            BEGIN DISPLAY 1824 1856 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(2)
            WIRE 736 1584 1872 1584
            WIRE 1872 1584 1872 1920
            WIRE 1872 1920 2000 1920
            WIRE 736 1584 736 1824
            WIRE 736 1824 816 1824
            WIRE 1808 1920 1824 1920
            WIRE 1824 1920 1872 1920
            BEGIN DISPLAY 1824 1920 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH Q(3)
            WIRE 720 1568 1360 1568
            WIRE 1360 1568 1888 1568
            WIRE 1888 1568 1888 1984
            WIRE 1888 1984 2000 1984
            WIRE 720 1568 720 1856
            WIRE 720 1856 816 1856
            WIRE 1360 848 1424 848
            WIRE 1360 848 1360 1568
            WIRE 1808 1984 1824 1984
            WIRE 1824 1984 1888 1984
            BEGIN DISPLAY 1824 1984 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_3 1424 2432 R0
        BEGIN BRANCH R(3)
            WIRE 1808 1104 1904 1104
            WIRE 1904 800 1904 816
            WIRE 1904 816 1904 1104
            BEGIN DISPLAY 1904 816 ATTR Name
                ALIGNMENT SOFT-TVCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R(2)
            WIRE 1808 1040 1872 1040
            WIRE 1872 800 1872 816
            WIRE 1872 816 1872 1040
            BEGIN DISPLAY 1872 816 ATTR Name
                ALIGNMENT SOFT-TVCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R(1)
            WIRE 1808 976 1840 976
            WIRE 1840 800 1840 816
            WIRE 1840 816 1840 976
            BEGIN DISPLAY 1840 816 ATTR Name
                ALIGNMENT SOFT-TVCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH R(0)
            WIRE 1808 800 1808 816
            WIRE 1808 816 1808 912
            BEGIN DISPLAY 1808 816 ATTR Name
                ALIGNMENT SOFT-TVCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_384
            WIRE 1232 2160 1328 2160
            WIRE 1328 2112 1328 2160
            WIRE 1328 2112 1424 2112
        END BRANCH
        BEGIN BRANCH XLXN_390
            WIRE 1200 1840 1312 1840
            WIRE 1312 1792 1312 1840
            WIRE 1312 1792 1424 1792
        END BRANCH
        BEGIN BRANCH XLXN_391
            WIRE 1200 1872 1312 1872
            WIRE 1312 1856 1312 1872
            WIRE 1312 1856 1424 1856
        END BRANCH
        BEGIN BRANCH XLXN_392
            WIRE 1200 1904 1312 1904
            WIRE 1312 1904 1312 1920
            WIRE 1312 1920 1424 1920
        END BRANCH
        BEGIN BRANCH XLXN_393
            WIRE 1200 1936 1312 1936
            WIRE 1312 1936 1312 1984
            WIRE 1312 1984 1424 1984
        END BRANCH
        INSTANCE XLXI_58 2784 1616 R0
        IOMARKER 3088 1520 OK R0 28
        BEGIN INSTANCE XLXI_61 2144 1024 R0
        END INSTANCE
        IOMARKER 2752 704 R(3:0) R0 28
        INSTANCE XLXI_24 1280 800 R0
        BEGIN BRANCH XLXN_382
            WIRE 768 1616 1344 1616
            WIRE 1344 1616 1344 2176
            WIRE 1344 2176 1424 2176
            WIRE 768 1616 768 1760
            WIRE 768 1760 816 1760
            WIRE 1344 800 1344 1296
            WIRE 1344 1296 1424 1296
            WIRE 1344 1296 1344 1616
        END BRANCH
        BEGIN BRANCH CO
            WIRE 960 2192 976 2192
            WIRE 960 2192 960 2416
            WIRE 960 2416 2160 2416
            WIRE 2160 1648 2464 1648
            WIRE 2464 1648 2464 1680
            WIRE 2464 1648 2576 1648
            WIRE 2160 1648 2160 2416
            WIRE 2528 992 2576 992
            WIRE 2576 992 2576 1552
            WIRE 2576 1552 2576 1648
            WIRE 2576 1552 2784 1552
        END BRANCH
        INSTANCE XLXI_62 2144 1504 R0
        BEGIN BRANCH CT_0
            WIRE 2032 1184 2144 1184
            BEGIN DISPLAY 2032 1184 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CT_1
            WIRE 2032 1248 2144 1248
            BEGIN DISPLAY 2032 1248 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH LED(0)
            WIRE 2528 1184 2640 1184
            WIRE 2640 1184 2720 1184
            BEGIN DISPLAY 2640 1184 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH LED(1)
            WIRE 2528 1248 2640 1248
            WIRE 2640 1248 2720 1248
            BEGIN DISPLAY 2640 1248 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH LED(2)
            WIRE 2528 1312 2640 1312
            WIRE 2640 1312 2720 1312
            BEGIN DISPLAY 2640 1312 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH LED(3)
            WIRE 2528 1376 2640 1376
            WIRE 2640 1376 2720 1376
            BEGIN DISPLAY 2640 1376 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        IOMARKER 2976 1024 LED(3:0) R0 28
        BUSTAP 2816 1376 2720 1376
        BUSTAP 2816 1312 2720 1312
        BUSTAP 2816 1248 2720 1248
        BUSTAP 2816 1184 2720 1184
        BEGIN BRANCH LED(3:0)
            WIRE 2816 1024 2864 1024
            WIRE 2864 1024 2976 1024
            WIRE 2816 1024 2816 1184
            WIRE 2816 1184 2816 1248
            WIRE 2816 1248 2816 1312
            WIRE 2816 1312 2816 1376
            BEGIN DISPLAY 2864 1024 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        INSTANCE XLXI_23 1328 2560 R0
        BEGIN BRANCH D(3:0)
            WIRE 1200 608 1200 736
            WIRE 1200 736 1200 800
            WIRE 1200 800 1200 864
            WIRE 1200 864 1200 928
            WIRE 1200 608 1504 608
            WIRE 1504 608 1984 608
            WIRE 1984 608 1984 992
            WIRE 1984 992 2144 992
            BEGIN DISPLAY 1504 608 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN INSTANCE XLXI_1 192 1632 R0
        END INSTANCE
        IOMARKER 144 1664 BUTN R180 28
        IOMARKER 144 1600 CLK R180 28
        BUSTAP 1200 736 1104 736
        BUSTAP 1200 800 1104 800
        BUSTAP 1200 864 1104 864
        BUSTAP 1200 928 1104 928
        BEGIN BRANCH B(0)
            WIRE 592 736 656 736
            WIRE 656 736 672 736
            BEGIN DISPLAY 656 736 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(1)
            WIRE 592 800 656 800
            WIRE 656 800 672 800
            BEGIN DISPLAY 656 800 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(2)
            WIRE 592 864 656 864
            WIRE 656 864 672 864
            BEGIN DISPLAY 656 864 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH B(3)
            WIRE 592 928 656 928
            WIRE 656 928 672 928
            BEGIN DISPLAY 656 928 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D(0)
            WIRE 1056 736 1088 736
            WIRE 1088 736 1104 736
            BEGIN DISPLAY 1088 736 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D(1)
            WIRE 1056 800 1088 800
            WIRE 1088 800 1104 800
            BEGIN DISPLAY 1088 800 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D(2)
            WIRE 1056 864 1088 864
            WIRE 1088 864 1104 864
            BEGIN DISPLAY 1088 864 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D(3)
            WIRE 1056 928 1088 928
            WIRE 1088 928 1104 928
            BEGIN DISPLAY 1088 928 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CLK
            WIRE 592 1248 672 1248
        END BRANCH
        BEGIN BRANCH XLXN_342
            WIRE 624 672 672 672
            WIRE 624 672 624 992
            WIRE 624 992 672 992
            WIRE 624 992 624 1120
            WIRE 624 1120 672 1120
            WIRE 624 1120 624 1184
            WIRE 624 1184 672 1184
            WIRE 624 1184 624 1344
            WIRE 624 1344 672 1344
            WIRE 624 1344 624 1472
            WIRE 624 1472 1392 1472
            WIRE 1392 1472 1392 1728
            WIRE 1392 1728 1392 2048
            WIRE 1392 2048 1392 2400
            WIRE 1392 2400 1424 2400
            WIRE 1392 2400 1392 2432
            WIRE 1392 2048 1424 2048
            WIRE 1392 1728 1424 1728
            WIRE 1392 1168 1424 1168
            WIRE 1392 1168 1392 1472
        END BRANCH
        INSTANCE XLXI_5 672 1376 R0
        IOMARKER 592 1248 CLK R180 28
        BEGIN BRANCH B(3:0)
            WIRE 336 624 496 624
            WIRE 496 624 496 736
            WIRE 496 736 496 800
            WIRE 496 800 496 864
            WIRE 496 864 496 928
        END BRANCH
        IOMARKER 336 624 B(3:0) R180 28
        BUSTAP 496 736 592 736
        BUSTAP 496 800 592 800
        BUSTAP 496 864 592 864
        BUSTAP 496 928 592 928
        BEGIN BRANCH XLXN_569
            WIRE 576 1536 576 2128
            WIRE 576 2128 816 2128
            WIRE 816 2128 976 2128
            WIRE 576 1536 672 1536
            WIRE 672 1536 1264 1536
            WIRE 1264 1536 2208 1536
            WIRE 2208 1536 2208 2192
            WIRE 2208 2192 2496 2192
            WIRE 672 1056 672 1536
            WIRE 816 2096 816 2128
            WIRE 1264 1520 1424 1520
            WIRE 1264 1520 1264 1536
        END BRANCH
        IOMARKER 2240 2320 Q(3:0) R0 28
        INSTANCE XLXI_80 384 2416 R0
        BEGIN BRANCH DIV_0
            WIRE 640 2256 688 2256
        END BRANCH
        BEGIN BRANCH D(0)
            WIRE 320 2160 384 2160
            BEGIN DISPLAY 320 2160 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D(1)
            WIRE 320 2224 384 2224
            BEGIN DISPLAY 320 2224 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D(2)
            WIRE 320 2288 384 2288
            BEGIN DISPLAY 320 2288 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH D(3)
            WIRE 320 2352 384 2352
            BEGIN DISPLAY 320 2352 ATTR Name
                ALIGNMENT SOFT-RIGHT
            END DISPLAY
        END BRANCH
        IOMARKER 688 2256 DIV_0 R0 28
        BEGIN BRANCH CT_2
            WIRE 2400 1600 2400 1680
            WIRE 2400 1600 2608 1600
            WIRE 2608 1600 2912 1600
            WIRE 2912 1600 2912 1904
            WIRE 2608 1488 2608 1600
            WIRE 2608 1488 2672 1488
            WIRE 2672 1488 2784 1488
            WIRE 2880 1904 2912 1904
            BEGIN DISPLAY 2672 1488 ATTR Name
                ALIGNMENT SOFT-BCENTER
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CT_0
            WIRE 2880 1776 2896 1776
            WIRE 2896 1776 3024 1776
            BEGIN DISPLAY 3024 1776 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH CT_1
            WIRE 2880 1840 2896 1840
            WIRE 2896 1840 3024 1840
            BEGIN DISPLAY 3024 1840 ATTR Name
                ALIGNMENT SOFT-LEFT
            END DISPLAY
        END BRANCH
        BEGIN BRANCH XLXN_414
            WIRE 2048 1376 2144 1376
        END BRANCH
        INSTANCE XLXI_65 2048 1440 R270
    END SHEET
END SCHEMATIC
