VERSION 6
BEGIN SCHEMATIC
    BEGIN ATTR DeviceFamilyName "spartan3e"
        DELETE all:0
        EDITNAME all:0
        EDITTRAIT all:0
    END ATTR
    BEGIN NETLIST
        SIGNAL C1
        SIGNAL C0
        SIGNAL B1
        SIGNAL B0
        SIGNAL D0
        SIGNAL D1
        SIGNAL A_O
        SIGNAL B_O
        SIGNAL D_O
        SIGNAL S0
        SIGNAL C_O
        SIGNAL A0
        SIGNAL A1
        PORT Input C1
        PORT Input C0
        PORT Input B1
        PORT Input B0
        PORT Input D0
        PORT Input D1
        PORT Output A_O
        PORT Output B_O
        PORT Output D_O
        PORT Input S0
        PORT Output C_O
        PORT Input A0
        PORT Input A1
        BEGIN BLOCKDEF m2_1
            TIMESTAMP 2000 1 1 10 10 10
            LINE N 96 -64 96 -192 
            LINE N 256 -96 96 -64 
            LINE N 256 -160 256 -96 
            LINE N 96 -192 256 -160 
            LINE N 176 -32 96 -32 
            LINE N 176 -80 176 -32 
            LINE N 0 -32 96 -32 
            LINE N 320 -128 256 -128 
            LINE N 0 -96 96 -96 
            LINE N 0 -160 96 -160 
        END BLOCKDEF
        BEGIN BLOCK XLXI_5 m2_1
            PIN D0 A0
            PIN D1 A1
            PIN S0 S0
            PIN O A_O
        END BLOCK
        BEGIN BLOCK XLXI_6 m2_1
            PIN D0 B0
            PIN D1 B1
            PIN S0 S0
            PIN O B_O
        END BLOCK
        BEGIN BLOCK XLXI_7 m2_1
            PIN D0 C0
            PIN D1 C1
            PIN S0 S0
            PIN O C_O
        END BLOCK
        BEGIN BLOCK XLXI_8 m2_1
            PIN D0 D0
            PIN D1 D1
            PIN S0 S0
            PIN O D_O
        END BLOCK
    END NETLIST
    BEGIN SHEET 1 3520 2720
        BEGIN BRANCH A_O
            WIRE 944 224 992 224
        END BRANCH
        BEGIN BRANCH B_O
            WIRE 944 512 992 512
        END BRANCH
        BEGIN BRANCH B1
            WIRE 256 544 624 544
        END BRANCH
        BEGIN BRANCH B0
            WIRE 256 480 624 480
        END BRANCH
        BEGIN BRANCH C1
            WIRE 256 832 624 832
        END BRANCH
        BEGIN BRANCH C0
            WIRE 256 768 624 768
        END BRANCH
        BEGIN BRANCH D_O
            WIRE 928 1088 944 1088
            WIRE 944 1088 976 1088
        END BRANCH
        BEGIN BRANCH D0
            WIRE 256 1056 608 1056
        END BRANCH
        BEGIN BRANCH D1
            WIRE 256 1120 608 1120
        END BRANCH
        BEGIN BRANCH S0
            WIRE 256 1184 592 1184
            WIRE 592 1184 608 1184
            WIRE 592 320 624 320
            WIRE 592 320 592 608
            WIRE 592 608 592 896
            WIRE 592 896 592 1184
            WIRE 592 896 624 896
            WIRE 592 608 624 608
        END BRANCH
        IOMARKER 256 1056 D0 R180 28
        IOMARKER 256 1120 D1 R180 28
        IOMARKER 256 832 C1 R180 28
        IOMARKER 256 768 C0 R180 28
        IOMARKER 256 544 B1 R180 28
        IOMARKER 256 480 B0 R180 28
        IOMARKER 992 512 B_O R0 28
        IOMARKER 992 224 A_O R0 28
        BEGIN BRANCH C_O
            WIRE 944 800 976 800
        END BRANCH
        IOMARKER 976 800 C_O R0 28
        BEGIN BRANCH A0
            WIRE 256 192 624 192
        END BRANCH
        BEGIN BRANCH A1
            WIRE 256 256 624 256
        END BRANCH
        IOMARKER 256 256 A1 R180 28
        IOMARKER 256 192 A0 R180 28
        IOMARKER 976 1088 D_O R0 28
        INSTANCE XLXI_6 624 640 R0
        INSTANCE XLXI_7 624 928 R0
        INSTANCE XLXI_8 608 1216 R0
        INSTANCE XLXI_5 624 352 R0
        IOMARKER 256 1184 S0 R180 28
    END SHEET
END SCHEMATIC
